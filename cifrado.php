<?php

error_reporting('E_ALL');
ini_set('display_errors', 1);

if(!extension_loaded('OpenSSL'))
	die('No ta la SSL');
/*
$conf = array (
    'digest_alg'       => 'sha256',
    'private_key_type' => OPENSSL_KEYTYPE_RSA,
    'private_key_bits' => 2048,
    'encrypt_key'      => true,
);
*/
// Creo el keypair
$res=openssl_pkey_new();

// Exporto la Private Key
openssl_pkey_export($res, $privatekey);

// Exporto la Public key
$publickey=openssl_pkey_get_details($res);
$publickey=$publickey["key"];

echo "**".$privatekey."<br>";
echo "Private Key:<BR>$privatekey<br><br>";
echo "Public Key:<BR>$publickey<BR><BR>";

$mensajeOriginal = 'Hola Jaz, soy un texto cifrado! Uy, cuanta seguridad.';

echo "Mensaje :<br>$mensajeOriginal<BR><BR>";

openssl_public_encrypt($mensajeOriginal, $crypttext, $publickey);

echo "Mensaje Encriptado que viaja en la llamada a la API:<br>$crypttext<BR><BR>";

openssl_private_decrypt($crypttext, $decrypted, $privatekey);

echo "Cuando llega, el server lo desencripta:<BR>$decrypted<br><br>";
?>