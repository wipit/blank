<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('ENTORNO', 'local');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
// define('DB_NAME', 'c0_club_elnuevodia');
// define('DB_NAME', 'dinardip_clubendi');
define('DB_NAME', 'dinardip_cupon_test');

/** MySQL database username */
// define('DB_USER', 'prov_dinardi');
// define('DB_USER', 'dinardip_clubend');
define('DB_USER', 'dinardip');

/** MySQL database password */
// define('DB_PASSWORD', '9xFp4jFx!sox$P');
// define('DB_PASSWORD', ';p9CJopTj=@2');
define('DB_PASSWORD', '4ls1n4176o');

/** MySQL hostname */
// define('DB_HOST', 'localhost');
// define('DB_HOST', 'www.dinardiproject-lab.com.ar');
define('DB_HOST', 'ahostear.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


$dev = true;
if($dev) {
	define('SCRIPT_VUE', '//cdn.jsdelivr.net/npm/vue/dist/vue.js');
	define('GIGYA_KEY', '3_mK2cmEzLkzhqe4MUrtPncxbv4wKsYAhsew0iVwhVUls3c_Jx_HLX434jAVpq5M_W');
	define('AMAZON_URL', 'https://sqs.us-east-1.amazonaws.com/013106308891/ClubEndi-Atabex-QA');
	define('GFRM_URL', 'https://sam-qa.gfrmservices.com/v1/api/ActiveCustomerSubscription');
	define('SCREENSET_GIGYA', 'NewRaas4nov15-RegistrationLogin');
} else {
	define('SCRIPT_VUE', '//cdn.jsdelivr.net/npm/vue');
	define('GIGYA_KEY', '3_6lTIHFIzusJkm-TMB23rOsdQTUfjJ_kphOHGbWc-xQbbVhl76PTg8UdPlTz7ethy');
	define('AMAZON_URL', 'https://sqs.us-east-1.amazonaws.com/013106308891/ClubEndi-Atabex');
	define('GFRM_URL', 'https://sam.gfrmservices.com/v1/api/ActiveCustomerSubscription');
	define('SCREENSET_GIGYA', 'elnuevodia-web-login');
}
define('GTM_KEY', 'GTM-NZ48FVD');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'put your unique phrase here');
define('SECURE_AUTH_KEY',  'put your unique phrase here');
define('LOGGED_IN_KEY',    'put your unique phrase here');
define('NONCE_KEY',        'put your unique phrase here');
define('AUTH_SALT',        'put your unique phrase here');
define('SECURE_AUTH_SALT', 'put your unique phrase here');
define('LOGGED_IN_SALT',   'put your unique phrase here');
define('NONCE_SALT',       'put your unique phrase here');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
 // Enable WP_DEBUG mode
define( 'WP_DEBUG', false );

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', false );

// Disable display of errors and warnings 
define( 'WP_DEBUG_DISPLAY', false );
// @ini_set( 'display_errors', 1 );

// Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
define( 'SCRIPT_DEBUG', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
