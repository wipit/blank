<!doctype html>
	<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no" />
		<link rel="profile" href="//gmpg.org/xfn/11" />
		<?php wp_head(); ?>
		<script>
			var patear = false;
			var googletag = googletag || {};
			googletag.cmd = googletag.cmd || [];
		</script>
	</head>
	<body <?php body_class(); ?>>
		<!-- Google Tag Manager (noscript) --> 
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo GTM_KEY; ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> 
        <!-- End Google Tag Manager (noscript) --> 
		<div id="page-container">
			<?php
				if($_COOKIE['logged'] == 1 && $_COOKIE['nuevo'] == 1 ) { ?>
					<style type="text/css">
						.exclusivo {
							display: block;
						}
					</style>
			<?php
				}
				// $url = home_url($wp->request);
				// $partes = explode('/', $url);
				if(is_archive()) {
					$type = 'archive';
				} else if(is_search()) {
					$type = 'search';
				} else {
					$type = get_post_type() ? get_post_type() : 'NO DETERMINADO';
				}
				switch($type) {
					case 'post':
						$subseccion = 'Lista curada';
						$merchant = '';
						$nombre = $wp_query->post->post_title;
						break;
					case 'archivos':
						$subseccion = 'Archivo';
						$merchant = '';
						$nombre =$wp_query->post->post_title;
						break;
					case 'eventos':
						$term = get_the_category($post->ID);
						$subcategoria = get_cat_name($term[0]->term_id);
						$subseccion = $subcategoria;
						$merchant_field = get_field('merchant', $post->ID);
						$merchant = $merchant_field ? $merchant_field->post_title.' / ' : '';
						$nombre = $post->post_title;
						break;
					case 'beneficios':
						$term = get_the_category($post->ID);
						$subcategoria = get_cat_name($term[0]->term_id);
						$subseccion = $subcategoria;
						$merchant_field = get_field('merchant', $post->ID);
						$merchant = $merchant_field ? $merchant_field->post_title.' / ' : '';
						$nombre = $post->post_title;
						break;
					case 'page':
						$subseccion = 'Página';
						$merchant = '';
						$nombre = $wp_query->post->post_title ? ucfirst(str_replace('Destacados ', '', $wp_query->post->post_title)) : 'VER';
						break;
					default:
						$merchant = '';
						$nombre = '';
						if(is_archive()) {
							$subseccion = ltrim(wp_title('', false), ' ');
							$nombre = 'Página de filtros';
						} else if(is_search()) {
							$subseccion = 'Página de búsqueda';
							$nombre = get_search_query();
						} else {
							$subseccion = get_post_type() ? get_post_type() : 'NO DETERMINADO';
						}
						break;
				}
			?>
			<input type="hidden" id="gtm_subseccion" name="gtm_subseccion" value="<?php echo $subseccion; ?>" />
			<input type="hidden" id="gtm_merchant" name="gtm_merchant" value="<?php echo $merchant; ?>" />
			<input type="hidden" id="gtm_oferta" name="gtm_oferta" value="<?php echo $nombre; ?>" />
			<input type="hidden" id="gtm_tipo" name="gtm_tipo" value="<?php echo $type; ?>" />
			<?php if((!isset($_COOKIE['accept']) || !$_COOKIE['accept']) && 0) { ?>
				<div id="accept" class="alert alert-info text-center">
				  <strong>Importante</strong> <br/>Acepto las cookies del sitio. <br/><button class="btn btn-info" onclick="aceptarCookie();">Acepto</button>
				</div>
			<?php } ?>
			<?php if(!(get_post_type() == 'page' && get_the_ID() == '347')) { ?>
			<div id="header" v-cloak>
				<header class="container">
					<div class="d-flex justify-content-between align-items-center">
						<?php echo the_custom_logo(); ?>
					    <div id="buscador" class="flex-1 justify-content-center align-items-center d-none d-lg-flex">
					    	<form :action="base + '/'" id="search" onsubmit="enviarGTM()">
						    	<input type="text" name="s" placeholder="Busca la oferta ideal para ti" />
						    	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/lupa.png" />
					    	</form>
					    </div>
					    <div id="login" class="d-none d-lg-flex justify-content-end align-items-center" v-cloak>
					    	<div v-if="ready">
					    		<div class="con_back" v-if="!logueado">
					    			<a href="#" @click="desplegarMenu" class="btn btn-primary" style="margin-right: 3.5em;">Login</a>
					    			<a onclick="enviarSuscribete()" href="https://suscripciones.elnuevodia.com/?sub_source=clubendi" target="_blank">Suscríbete</a>
					    		</div>
					    		<div class="dropdown con_back" v-else>
									<button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>¡Hola,&nbsp;</span><span class="overflowed" v-text="perfil.firstName ? perfil.firstName : 'usuario'"></span><span>!</span><i class="fa fa-lg fa-chevron-down"></i></button>
									<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
										<a class="dropdown-item" v-if="suscripto" href="https://micuenta.elnuevodia.com/suscripcion" target="_blank">Mi cuenta</a>
										<a class="dropdown-item" v-if="!suscripto" href="https://micuenta.elnuevodia.com/perfil" target="_blank">Mi cuenta</a>
										<a class="dropdown-item" href="#" @click="onLogout">Cerrar sesión</a>
									</div>
									<div class="dropdown" v-if="suscripto">
										<a class="pl-3" :href="base + '/mis-eventos'" v-if="es_eventos">Tus eventos</a>
										<a class="pl-3" :href="base + '/cupones'" v-else>Tus cupones <span v-if="nuevas && nuevas != '0'" v-text="nuevas"></span></a>
										<button type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<i class="fa fa-lg fa-chevron-down"></i>
										</button>
										<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownCuponesButton" id="dropdownCuponesButton">
											<a class="dropdown-item" :href="base + '/mis-eventos'" v-if="!es_eventos"><img :src="base + '/wp-content/themes/dp/images/icon_event.png'" /> Tus eventos</a>
											<a class="dropdown-item" :href="base + '/cupones'" v-else><img :src="base + '/wp-content/themes/dp/images/icon_cupon.png'" /> Tus cupones</a>
										</div>
									</div>
									<a id="margined" onclick="javascript:enviarSuscribete()" class="btn btn-primary" href="https://suscripciones.elnuevodia.com/?sub_source=clubendi" target="_blank" v-else>Suscríbete!</a>
								</div>
					    	</div>
					    </div>
					    <button type="button" class="d-block d-lg-none btn btn-transparente" data-toggle="offcanvas">Menú</button>
					</div>
				</header>
				<?php require "nav.php"; ?>
			</div>
			<?php } ?>