<?php 
	if(get_field('exclusivo', $post->ID) == 'si') {
		if(!isset($_COOKIE['logged']) || ($_COOKIE['logged'] == 1 && $_COOKIE['nuevo'] == 1 )) {
		// if(isset($_COOKIE['logged']) && ($_COOKIE['logged'] == 1 && $_COOKIE['nuevo'] == 1 )) {
		} else {
			header("Location: http://".$_SERVER['HTTP_HOST']);
		    die();
		} 
	}
	get_header(); 
	if(get_field('exclusivo', $post->ID) == 'si') {
		echo '<script>patear = true;</script>';
	}
?>
	<div class="container-fluid" id="cuerpo">
		<div class="row">
			<div id="beneficio" class="col-12">
				<div class="row">
					<div id="top" class="col-12">
						<style type="text/css">
							#whatever {
								background-image: url('<?php echo get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID, 'encabezado') : get_field('thumbnail', $post->ID); ?>');
							}
							@media(max-width: 767px) {
								#whatever {
									background-image: url('<?php echo get_field('thumbnail_mobile', $post->ID) ? get_field('thumbnail_mobile', $post->ID, 'encabezado') : get_field('thumbnail', $post->ID); ?>');
								}
							}
						</style>
						<div id="whatever" class="slide d-block">
							<div class="dummy"></div>
							<a class="link" href="#">
							    <?php 
							    	$tipo = get_post_type($post->ID);
							    	$remember = $tipo;
							    	$term = get_the_category($post->ID);
							    	$subcategoria = get_cat_name($term[0]->term_id);
							    	$categoria = $term[0] && $term[0]->parent == '0' ? $term[0]->term_id : $term[0]->parent;
						    		$term = get_term($categoria, 'category');
		    						$color = get_field('color', $term) ? get_field('color', $term) : '#4F97C7';
						    		$beneficio = get_descuento($post->ID);
						    		$disponible = get_disponibles($post->ID);
						    		echo '<div class="only_mobile d-block d-lg-none"><h5 class="d-block d-lg-none">';
							    	echo get_cat_name($categoria) ? get_cat_name($categoria) : ucfirst($tipo);
							    	echo '</h5><h4>'.$post->post_title.'</h4></div>';
							    	echo '<div class="info">';
							    	echo '<span class="fondo" style="background: '.$color.';"></span>';
							    	echo '<div class="wrapper d-flex flex-column justify-content-center"><div class="titular"><h5 class="d-none d-lg-block"><span class="opaco">';
							    	echo get_cat_name($categoria) ? get_cat_name($categoria) : ucfirst($tipo);
							    	echo '</span>';
							    	echo '</h5>'.$beneficio.'</div>';
							    	echo get_detalle($post->ID);
							    	$merchant = get_field('merchant', $post->ID);
							    	// if($merchant)
							    	// 	echo '<p class="merchant">'.$merchant->post_title.'</p>';
							    	echo '</div></div>';
							    	$pie = get_post_meta($post->ID, 'pie_de_imagen_texto_pie', true);
							    	if($pie) {
							    		$color2 = get_post_meta($post->ID, 'pie_de_imagen_color_pie', true) ? get_post_meta($post->ID, 'pie_de_imagen_color_pie', true) : '#fff';
							    		echo '<p class="pie" style="color: '.$color2.';">'.$pie.'</p>';
									}
							    ?>
							</a>
							<style type="text/css">
								@media(max-width: 992px) {
									body #cuerpo #beneficio #top .slide .info #descuento, body #cuerpo #acerca #top .slide .info #descuento, body #cuerpo #archivo #top .slide .info #descuento, body #cuerpo #evento #top .slide .info #descuento {
									    background: <?php echo $color; ?>;
									}
									body #cuerpo #beneficio #top .slide .info .titular, body #cuerpo #acerca #top .slide .info .titular, body #cuerpo #archivo #top .slide .info .titular, body #cuerpo #evento #top .slide .info .titular {
										color: <?php echo $color; ?>;
									}
								}
							</style>
						</div>
					</div>
					<div id="informacion" class="col-12">
						<div class="container">
							<div class="row padded-fix">
								<div class="col-12 col-lg-4 order-12 order-lg-1 boton-beneficio">
									<div class="d-none d-lg-block" id="boton-beneficio" v-cloak>
										<?php $validez = get_field('validez', $post->ID); ?>
										<boton-beneficios :subcategoria="'<?php echo $subcategoria; ?>'" :merchant="'<?php echo $merchant ? str_replace("'", "", $merchant->post_title) : ''; ?>'" :titulo="'<?php echo str_replace("'", "", htmlentities($post->post_title)); ?>'" :post="<?php echo $post->ID; ?>" :validez="'<?php echo $validez; ?>'" :hoy="'<?php echo date('Y-m-d'); ?>'" :user="perfil" :id="id" :cupones="cupones" :ready="ready" :agotado="<?php echo $disponible < 1 ? 'true' : 'false'; ?>"  :logueado="logueado" :suscripto="suscripto" v-on:agregar="agregar" v-on:suscribir="suscribir" inline-template>
											<div v-if="ready" v-cloak>
												<button class='btn btn-primary' disabled='disabled' v-if="expirado|| agotado" @click="gtm('expirado')">Cupón expirado</button>
												<span v-else>
													<button class="btn btn-primary" @click="abrirLogin" v-if="!logueado">Añadir cupón</button>
													<span v-else>
														<button class='btn btn-primary' disabled='disabled' v-if="canjeado" @click="gtm('canjeado')">Ya añadiste este cupón</button>
														<span v-else>
															<button class="btn btn-primary" @click="canjearCupon" v-if="suscripto">Añadir cupón</button>
															<button class="btn btn-primary" @click="mostrarPopup" v-else>Añadir cupón</button>
														</span>
													</span>
												</span>
											</div>
										</boton-beneficios>
									</div>
									<?php
										echo '<div id="resumen">';
										$local = get_field('local');
										if($local['nombre']) {
									?>
										<p><span class="celeste"><i class="fa fa-lg fa-map-marker"></i></span> <?php echo $local['nombre']; ?></p>
									<?php
										}
										if($local['telefono']) {
									?>
										<p class="telefono"><span class="celeste"><a style="color: inherit;" href="tel:<?php echo preg_replace( '/[^0-9]/', '', $local['telefono']); ?>"><i class="fa fa-lg fa-volume-control-phone"></i></span> <?php echo $local['telefono']; ?></a></p>
									<?php
										}
										if($local['website']) {
									?>
										<p><a class="celeste" href="<?php echo $local['website']; ?>" target="_blank"><?php echo $local['website']; ?></a></p>
									<?php
										}
										echo '</div>';
									?>
									<?php 
										$contenido_adicional = get_field('archivo');
										if($contenido_adicional) {
									?>
										<p class="sans" id="descarga" v-cloak>
											<boton-archivo :url="'<?php echo $contenido_adicional; ?>'" :ready="ready" :suscripto="suscripto" :logueado="logueado" :titulo="'<?php echo str_replace("'", "", htmlentities($post->post_title)); ?>'" inline-template>
												<a :href="url" target="_blank" v-if="suscripto && logueado && 0">
													<i class="fa fa-lg fa-download celeste"></i> Descargar contenido adicional
												</a>
												<a href="javascript:;" @click="mostrarPopup(logueado)" v-else>
													<i class="fa fa-lg fa-download celeste"></i> Descargar contenido adicional
												</a>
											</boton-archivo>
										</p>
									<?php
										}
									?>
								</div>
								<div class="col-12 col-lg-8 order-1 order-lg-12">
									<div id="texto" v-cloak>
										<div class="d-block d-lg-none">
											<?php $validez = get_field('validez', $post->ID); ?>
											<boton-beneficios :subcategoria="'<?php echo $subcategoria; ?>'" :merchant="'<?php echo $merchant ? str_replace("'", "", $merchant->post_title) : ''; ?>'" :titulo="'<?php echo str_replace("'", "", htmlentities($post->post_title)); ?>'" :post="<?php echo $post->ID; ?>" :validez="'<?php echo $validez; ?>'" :hoy="'<?php echo date('Y-m-d'); ?>'" :user="perfil" :id="id" :cupones="cupones" :ready="ready" :agotado="<?php echo $disponible < 1 ? 'true' : 'false'; ?>"  :logueado="logueado" :suscripto="suscripto" v-on:agregar="agregar" v-on:suscribir="suscribir" inline-template>
											<div v-if="ready" v-cloak>
												<button class='btn btn-primary' disabled='disabled' v-if="expirado|| agotado" @click="gtm('expirado')">Cupón expirado</button>
												<span v-else>
													<button class="btn btn-primary" @click="abrirLogin" v-if="!logueado">Añadir cupón</button>
													<span v-else>
														<button class='btn btn-primary' disabled='disabled' v-if="canjeado" @click="gtm('canjeado')">Ya añadiste este cupón</button>
														<span v-else>
															<button class="btn btn-primary" @click="canjearCupon" v-if="suscripto">Añadir cupón</button>
															<button class="btn btn-primary" @click="mostrarPopup" v-else>Añadir cupón</button>
														</span>
													</span>
												</span>
											</div>
										</boton-beneficios>
										</div>
										<?php
											echo '<h4 class="d-none d-lg-block">'.$post->post_title.'</h4>';
											$contenido = apply_filters('the_content', get_post_field('post_content', $post->ID));
											if($contenido)
												echo $contenido;
											include_once('includes/share.php'); 
											include_once('includes/share_mobile.php');
											echo '<hr/>';
											$horarios = get_field('horarios', $post->ID);
											if($horarios)
												echo '<p><strong>Horario para redimir la oferta</strong></p>'.$horarios.'<hr/>';
											if($validez)
												echo '<p>Válido hasta el '.strtolower(formatearFecha($validez, 'single')).'</p>'.'<hr/>';
										?>
										<?php
											$bases = get_field('bases', $post->ID);
											if($bases)
												echo '<p><strong>Letra pequeña</strong></p>'.$bases;
										?>
									</div>
								</div>
							</div>
							<div class="row padded-fix map">
								<?php
									if($local['sucursales']) {
								?>
									<div class="col-12">
										<h3>Localidades para redimir</h3>
										<div id="mapa">
											<div id="map"></div>
											<div class="dummy"></div>
										</div>
									</div>
								<?php
									}
								?>
								<?php if(0) { ?>
								<div class="col-12 d-none d-lg-block" id="boton-beneficio2" v-cloak>
									<boton-beneficios :subcategoria="'<?php echo $subcategoria; ?>'" :merchant="'<?php echo $merchant ? str_replace("'", "", $merchant->post_title) : ''; ?>'" :titulo="'<?php echo str_replace("'", "", htmlentities($post->post_title)); ?>'" :post="<?php echo $post->ID; ?>" :validez="'<?php echo $validez; ?>'" :hoy="'<?php echo date('Y-m-d'); ?>'" :user="perfil" :id="id" :cupones="cupones" :ready="ready" :agotado="<?php echo $disponible < 1 ? 'true' : 'false'; ?>"  :logueado="logueado" :suscripto="suscripto" v-on:agregar="agregar" v-on:suscribir="suscribir" inline-template>
										<div v-if="ready" v-cloak>
											<button class='btn btn-primary' disabled='disabled' v-if="expirado|| agotado" @click="gtm('expirado')">Cupón expirado</button>
											<span v-else>
												<button class="btn btn-primary" @click="abrirLogin" v-if="!logueado">Añadir cupón</button>
												<span v-else>
													<button class='btn btn-primary' disabled='disabled' v-if="canjeado" @click="gtm('canjeado')">Ya añadiste este cupón</button>
													<span v-else>
														<button class="btn btn-primary" @click="canjearCupon" v-if="suscripto">Añadir cupón</button>
														<button class="btn btn-primary" @click="mostrarPopup" v-else>Añadir cupón</button>
													</span>
												</span>
											</span>
										</div>
									</boton-beneficios>
								</div>
								<?php } ?>
							</div>
							<div class="row">
								<div class="col-12">
									<?php
										require('includes/recomendados.php');
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
<?php include_once('includes/maps.php'); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/boton.js?jazminN"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/archivo.js?mayo"></script>
<script type="text/javascript">
	var id = <?php echo $post->ID; ?>;
	var tipo = '<?php echo $remember; ?>';
</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/visitas.js"></script>