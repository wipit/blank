<?php get_header(); ?>
	<div class="container-fluid" id="cuerpo">
		<div class="row">
			<div id="slider" class="col-12">
				<?php require_once('includes/slider.php'); ?>
			</div>
			<div id="suscripcion" class="col-12">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-12 col-lg-5 d-flex align-items-start justify-content-center justify-content-md-left" id="clarito">
								<?php $grupo = get_field('beneficios_de_ser_socio'); ?>
								<h6><?php echo $grupo['titulo']; ?></h6>
							</div>
							<div class="col-12 col-lg-7" id="spotlights">
								<div class="row h-100">
									<?php $iconos = $grupo['iconos']; ?>
									<?php foreach($iconos as $icono) { ?>
										<div class="spot col-4 d-flex align-items-center justify-content-center">
											<span class="text-center imagen">
												<img class="sprite" src="<?php echo $icono['imagen']; ?>" />
											</span>
											<p>
												<span class="animated left activate slideInLeft"><?php echo $icono['texto']; ?></span>
											</p>
										</div>
									<?php } ?>
									<?php if(0) { ?>
										<div class="spot col-4 d-flex align-items-center justify-content-center">
											<span class="text-center">
												<div class="sprite1"><div id="sprite1"></div></div>
												<p><span class="animated left">Descuentos </span><span class="animated right">atractivos</span></p>
											</span>
										</div>
										<div class="spot col-4 d-flex align-items-center justify-content-center">
											<span class="text-center">
												<div class="sprite2"><div id="sprite2"></div></div>
												<p><span class="animated left">Invitación a eventos</span><span class="animated right">exclusivos</span></p>
											</span>
										</div>
										<div class="spot col-4 d-flex align-items-center justify-content-center">
											<span class="text-center">
												<div class="sprite3"><div id="sprite3"></div></div>
												<p><span class="animated left">Sorteos de</span><span class="animated right">entradas a conciertos</span></p>
											</span>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="beneficios" class="col-12">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<h3 class="d-flex justify-content-between align-items-center">Ofertas destacadas</h3>
								<div class="grilla row">
									<?php
										$home = get_page_by_title('Home');
										$destacados = get_field('beneficios', $home);
										global $elemento;
										global $cantidad;
										global $x;
										$x = 0;
										$flat_beneficios = array_map("filtroVencidos", $destacados['destacados_beneficios']);
										$array_beneficios = array_filter($flat_beneficios, 'removerVencidos');
										$cantidad = count($array_beneficios);
										foreach($array_beneficios as $elemento) {
											get_template_part( 'parts/cuadricula' );
										}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php require_once('includes/proximos.php'); ?>
			<div id="banner" class="col-12 d-flex justify-content-center align-items-center">
				<!-- GPT AdSlot --> 
				<div id='adFooter'> 
				<script> googletag.cmd.push(function() { googletag.display('adFooter'); }); </script> 
				</div> 
				<!-- End AdSlot --> 
			</div>
			<?php
				$home = get_page_by_title('Home');
				$destacados = get_field('recomendados', $home);
				// var_dump($destacados);
				global $elemento;
				global $cantidad;
				global $counter;
				$cantidad = count($destacados['destacados_recomendados'][0]['destacado_recomendados']);
				$counter = 1;
				if($cantidad > 0) {
			?>
				<div id="recomendados" class="col-12">
					<div class="row">	
						<div class="container">
							<div class="row">
								<div class="col-12">
									<h3>Te recomendamos</h3>
									<div class="tira row d-flex justify-content-between">
										<?php
											// $args = array(
											// 	'post_type'              => array( 'post' ),
											// 	'post_status'            => array( 'publish' ),
											// 	'posts_per_page'         => '3',
									  		//  'order'    => 'DESC',
										 	//  'orderby'  => 'date',
											// );
											// $query = new WP_Query( $args );
											// if ( $query->have_posts() ) {
											// 	while ( $query->have_posts() ) {
											// 		$query->the_post(); 
											foreach($destacados['destacados_recomendados'] as $recomendado) {
													$post = $recomendado['destacado_recomendados'];
													$term = get_the_category($post);
											    	if(isset($term[0])) {
											    		$categoria = $term[0] && $term[0]->parent == '0' ? $term[0]->term_id : $term[0]->parent;
											    	} else {
											    		$categoria = '';
											    	}
													$term = get_term($categoria, 'category');
            										$color = get_field('color', $term) ? get_field('color', $term) : '#4F97C7';
													?>
													<div class="recomendado">
														<div class="imagen">
															<div class="dummy"></div>
															<a href="<?php echo get_permalink($post->ID); ?>" class="d-block" style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID, "listas"); ?>');">
															</a>
														</div>
														<a href="<?php echo get_permalink($post->ID); ?>" class="d-block titular" style="border-color: transparent transparent transparent <?php echo $color; ?>; background: <?php echo $color; ?>;"><span class="triangle"></span><p><?php echo $post->post_title; ?></p></a>
													</div>
												<?php }
											// } 
											wp_reset_postdata();
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
<?php get_footer(); ?>
<?php
	$mp4 = get_field('video', $post->ID) ? '<source src="'.get_field('video', $post->ID).'" type="video/mp4" />' : '';
	$ogg = get_field('video_onboarding_ogg', $post->ID) ? '<source src="'.get_field('video_onboarding_ogg', $post->ID).'" type="video/ogg" />' : '';
	$webm = get_field('video_onboarding_webm', $post->ID) ? '<source src="'.get_field('video_onboarding_webm', $post->ID).'" type="video/webm" />' : '';
	if($mp4|| $ogg || $webm) {
?>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery.ajax({
			url: scripts.ajax_url, 
			method: 'POST',
			data: {
				action : 'count_cookie'
			},
			success: function(r){
				if(r < 2) {
					jQuery.fancybox.defaults.baseClass = 'centrado';
					jQuery.fancybox.open('<div id="contenedor"><div class="dummy"></div><video id="video" width="560" height="320" preload="auto" autoplay="autoplay" class="video-js vjs-default-skin"><?php echo $mp4.$ogg.$webm; ?><div>Your browser does not support the HTML5 video tag.</div></video></div>', {
						    touch: false
						});
					jQuery(document).ready(function(){
						document.getElementById('video').addEventListener('ended',cerrarVideo,false);
						var promise = jQuery('video').prop('muted',true)[0].play();
						"use strict";
						if (promise !== undefined) {
						  promise.then(function (_) {}).catch(function (error) {
						    jQuery('#contenedor').prepend('<a href="#" id="play"><img src="wp-content/themes/dp/images/play.png" /></a>');
						  });
						}
					});
				}
		    }
		});
		jQuery(document).on('click', '#play', function(){
			jQuery('video').prop('muted',true)[0].play();
			jQuery('#play').remove();
		});
	});
</script>
<?php
	}
?>