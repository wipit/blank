<?php get_header();  ?>
<div class="container" id="cuerpo">
  <div class="row eventos" id="resultados">
    <div class="col-12">
      <div class="row">
        <div class="col-12 col-md-4">
          <button onclick="abrirFiltros();" class="btn btn-filtro d-block d-md-none">Filtrar por</button>
          <?php 
            the_archive_title( '<h3 class="archive-title">', '</h3>' ); 
            the_archive_description(); 
          ?>
          <div class="seleccionados d-block d-md-none"></div>
          <div id="filtros">
            <div class="d-flex justify-content-between"><h5>Filtrar por:</h5><i class="fa fa-lg fa-times d-block d-md-none" onclick="javascript:cerrarFiltros();"></i></div>
            <div class="seleccionados d-none d-lg-block"></div>
            <?php 
              $term = get_category( get_query_var( 'cat' ) );
              $categoria = $term && $term->parent == '0' ? $term->term_id : $term->parent;
              $term = get_term($categoria, 'category');
              $color = get_field('color', $term) ? get_field('color', $term) : '#4F97C7';
            ?>
            <style type="text/css">
              #seleccionados .badge, [type=checkbox]:checked + span {
                background: <?php echo $color; ?>;
              }
              [type=checkbox]:checked + span {
                border-color: <?php echo $color; ?>!important;
              }
            </style>
            <div id="subcategorias">
              <div class="dropdown">
                <a class="d-flex justify-content-between w-100 collapsed" data-toggle="collapse" data-target="#collapseSubcategorias" aria-expanded="false" aria-controls="collapseSubcategorias" href="#">Subcategoría<i class="fa fa-lg fa-chevron-up"></i><i class="fa fa-lg fa-chevron-down"></i></a>
                <div class="collapse" id="collapseSubcategorias">
                  <?php
                    $tax = $wp_query->get_queried_object();
                    $query = "SELECT distinct(tt.term_taxonomy_id) as term_id FROM wp_term_taxonomy tt INNER JOIN wp_term_relationships tr ON tt.term_taxonomy_id=tr.term_taxonomy_id INNER JOIN wp_posts wp ON tr.object_id=wp.ID INNER JOIN wp_postmeta wpp ON wpp.post_id=wp.ID WHERE tt.parent='".$tax->term_id."' AND wpp.meta_key='validez' AND date(wpp.meta_value) >= DATE(NOW())";
                    $results = $wpdb->get_results($query, ARRAY_A);
                    $obj=[];
                    foreach($results as $k=>$v){  $obj[] = $v['term_id']; }
                    if($obj) {
                      $terms = get_terms( array(
                          'include' => $obj
                      ) );
                      foreach($terms as $term) {
                        if ($term->parent != 0) { 
                          echo '<label><input type="checkbox" value="'.$term->slug.'" data-name="'.$term->name.'" class="filter-item categorias" /><span></span> '.$term->name.'</label><br/>';
                        }
                      }
                    }
                  ?>
                </div>
              </div>
            </div>
            <div id="localidades">
              <div class="dropdown">
                <a class="d-flex justify-content-between w-100 collapsed" data-toggle="collapse" data-target="#collapseLocalidades" aria-expanded="false" aria-controls="collapseLocalidades" href="#">Localidad<i class="fa fa-lg fa-chevron-up"></i><i class="fa fa-lg fa-chevron-down"></i></a>
                <div class="collapse" id="collapseLocalidades">
                  <?php
                    $tax = $wp_query->get_queried_object();
                    $query = 'SELECT  DISTINCT(tt.term_taxonomy_id) as term_taxonomy_id FROM wp_term_taxonomy tt INNER JOIN wp_term_relationships tr ON tt.term_taxonomy_id=tr.term_taxonomy_id INNER JOIN wp_posts wp ON tr.object_id=wp.ID INNER JOIN wp_postmeta wpp ON wpp.post_id=wp.ID INNER JOIN wp_terms wpt ON wpt.term_id = tt.term_id WHERE tt.taxonomy="localidad" AND wpp.meta_key="validez" AND date(wpp.meta_value) >= DATE(NOW()) AND EXISTS(SELECT DISTINCT(wp1.id) as term_taxonomy_id FROM wp_term_taxonomy tt1  INNER JOIN wp_term_relationships tr1 ON tt1.term_taxonomy_id=tr1.term_taxonomy_id INNER JOIN wp_posts wp1 ON tr1.object_id=wp1.ID INNER JOIN wp_postmeta wpp1 ON wpp1.post_id=wp1.ID WHERE tt1.parent="'.$tax->term_id.'" AND wpp1.meta_key="validez" AND date(wpp1.meta_value) >= DATE(NOW()) AND wp1.ID=wp.ID)';
                    $results = $wpdb->get_results($query, ARRAY_A);
                    $obj=[];
                    foreach($results as $k=>$v){  $obj[] = $v['term_taxonomy_id']; }
                    if($obj) {
                      $terms = get_terms( array(
                          'taxonomy' => 'localidad',
                          'hide_empty' => true,
                          'include' => $obj
                      ) );
                      foreach($terms as $term) {
                        echo '<label><input type="checkbox" value="'.$term->slug.'" data-name="'.$term->name.'" class="filter-item localidades" /><span></span> '.$term->name.'</label><br/>';
                      }
                    }
                  ?>
                </div>
              </div>
            </div>
            <button class="btn btn-primary d-block d-md-none" onclick="javascript:cerrarFiltros();">Filtrar</button>
          </div>
        </div>
        <div id="grilla" class="col-12 col-md-8">
          <div id="meses" class="d-flex justify-content-between"></div>
          <div id="noResultsContainer">
            <p>Pendientes. Próximamente tendremos nuevos eventos.</p>
          </div>
          <div class="row padd-left">
            <div id="total" class="w-100">
              <?php 
                global $elemento;
                global $cantidad;
                global $counter;
                global $tamaño;
                $all_children = get_term_children($tax->term_id, 'category');
                $tamaño = 'chico';
                $cantidad = 0;
                $x = 0;
                $counter = 1;
                $args = array(
                  'post_type'              => array( 'eventos' ),
                  'post_status'            => array( 'publish' ),
                  'posts_per_page'         => '-1',
                  'category__in' => $all_children, 
                  'meta_query' => array(
                    'validez' => array(
                      'key' => 'validez',
                      'compare' => '>=',
                      'type' => 'DATE',
                      'value' => date('Y-m-d')
                    )
                  ),
                  'orderby' => array( 
                    'validez' => 'ASC',
                  ),
                );
                $query = new WP_Query( $args );
                $meses = [];
                if ( $query->have_posts() ) {
                  while ( $query->have_posts() ) { 
                    $query->the_post(); 
                    $elemento = $post; 
                    $subcategorias = get_the_terms( $elemento->ID, 'category' );
                    $localidades = get_the_terms( $elemento->ID, 'localidad' );
                    $fecha = get_field('validez', $elemento->ID);
                    $mes = formatearFecha($fecha, "mes");
                    $meses[] = $mes;
                    ?>
                    <div data-mes="<?php echo $mes; ?>" class="resultado col-12 col-lg-6<?php if($localidades) foreach( $localidades as $localidad ) echo ' ' . $localidad->slug; ?><?php foreach( $subcategorias as $subcategoria ) echo ' ' . $subcategoria->slug.' '.$mes; ?>">
                      <?php require('parts/box.php'); ?>
                    </div>
                  <?php }  
                } ?>
              </div>
          </div>
          <div id="meses2" class="d-flex justify-content-between mt-5"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="row" id="evento">
    <div class="col-12 pt-0" id="informacion">
      <?php
        require('includes/anteriores.php');
      ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>