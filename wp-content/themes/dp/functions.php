<?php
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';

    add_action('wp_ajax_count_cookie', 'count_cookie');
    add_action('wp_ajax_nopriv_count_cookie', 'count_cookie');
    function count_cookie() {
        $count = isset($_COOKIE['count']) ? $_COOKIE['count'] : 0;
        $count++;
        setcookie('count', $count, time() + (86400 * 365), '/' ); 
        echo $count;
        die();
    }

    add_action('wp_ajax_accept_cookies', 'accept_cookies');
    add_action('wp_ajax_nopriv_accept_cookies', 'accept_cookies');
    function accept_cookies() {
        $count = isset($_COOKIE['accept']) ? $_COOKIE['accept'] : 0;
        if($_POST['accept']) {
            setcookie('accept', 1, time() + (86400 * 365), '/' ); 
            echo 1;
        } else {
            echo $count;
        }
        die();
    }

    add_action('wp_ajax_matar_cookies', 'matar_cookies');
    add_action('wp_ajax_nopriv_matar_cookies', 'matar_cookies');
    function matar_cookies() {
        unset($_COOKIE['logged']);
        setcookie('logged', '', time() - 3600, '/');
        unset($_COOKIE['id']);
        setcookie('id', '', time() - 3600, '/');
        unset($_COOKIE['jwt']);
        setcookie('jwt', '', time() - 3600, '/');
        unset($_COOKIE['eventos']);
        setcookie('eventos', '', time() - 3600, '/');
        unset($_COOKIE['cupones']);
        setcookie('cupones', '', time() - 3600, '/');
        unset($_COOKIE['count']);
        setcookie('count', '', time() - 3600, '/');
        unset($_COOKIE['user']);
        setcookie('user', '', time() - 3600, '/');
        die();
    }

    function relationship_options_filter($options, $field, $the_post) {
        $options['post_status'] = array('publish');
        return $options;
    }
    add_filter('acf/fields/post_object/query', 'relationship_options_filter', 10, 3);

    function redirect_page() {
        if (isset($_SERVER['HTTPS']) &&
            ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
            isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
            $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $protocol = 'https://';
        } else {
            $protocol = 'http://';
        }

        $currenturl = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $currenturl_relative = wp_make_link_relative($currenturl);

        if ($currenturl_relative == '/cupones/' || $currenturl_relative == '/cupones' || $currenturl_relative == 'cupones') {
            if(!isset($_COOKIE['logged'])) {
                exit( wp_redirect(home_url()) );
            }
        }
    }
    add_action( 'init', 'redirect_page' );

    function my_acf_google_map_api( $api ){
        $api['key'] = 'AIzaSyAon-X_CY4mVbjVlRqp7ge7ELcYS9kbT3E';
        return $api;
    }
    add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

    function remove_menus(){
        add_submenu_page('edit.php?post_type=sliders', 'Slides', 'Slides', 'manage_options', 'edit.php?post_type=slides');
        remove_submenu_page( 'index.php', 'update-core.php' );
        remove_menu_page( 'edit-comments.php' );
        // remove_menu_page( 'themes.php' );
        remove_menu_page( 'plugins.php' );
        // remove_menu_page( 'users.php' );
        // remove_menu_page( 'tools.php' );
        remove_menu_page( 'options-general.php' );
        // remove_menu_page( 'edit.php?post_type=acf-field-group');
        remove_menu_page( 'admin.php?page=cptui_manage_post_types' );
        remove_submenu_page('wpcf7', 'cf7md');
        remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
        if (!current_user_can('administrator')) {
            remove_menu_page('edit.php?post_type=sliders');
            remove_menu_page('edit.php?post_type=archivos');
            remove_menu_page('wpcf7');
            // if ( ! isset( $_GET['author'] ) ) {
            //     wp_redirect( add_query_arg( 'author', $user_ID ) );
            //     exit;
            // }
        }
        if (current_user_can('subscriber')) {
            remove_menu_page('edit.php');
        }
    }
    add_action( 'admin_menu', 'remove_menus' );

    add_action('wp_head', 'gtm_head_code');
    Function gtm_head_code(){
    ?>
        <!-- Google Tag Manager --> 
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': 
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], 
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer', '<?php echo GTM_KEY; ?>');</script> 
        <!-- End Google Tag Manager W4SHFB9--> 
        <script>window.dataLayer = window.dataLayer || [];</script>
    <?php
    }

    function footer_gigya() { ?>
        <script src="https://cdns.gigya.com/JS/socialize.js?apikey=3_6lTIHFIzusJkm-TMB23rOsdQTUfjJ_kphOHGbWc-xQbbVhl76PTg8UdPlTz7ethy" type="text/javascript">{ "regSource": "club.elnuevodia.com", "lang": "es", "siteName": "club.elnuevodia.com", "enabledProviders": "facebook,twitter,whatsapp,googleplus,email,pinterest", "connectWithoutLoginBehavior": "loginExistingUser", "autoLogin": true, "sessionExpiration": "-2" }</script>
    <?php }
    // add_action( 'wp_footer', 'footer_gigya' );

    function add_opengraph_doctype( $output ) {
        return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
    }
    add_filter('language_attributes', 'add_opengraph_doctype');

    remove_action( 'wp_head', '_wp_render_title_tag', 1 );
    add_action( 'wp_head', '_wp_render_title_tag_itemprop', 1 );

    function _wp_render_title_tag_itemprop() {
        if ( did_action( 'wp_head' ) || doing_action( 'wp_head' ) ) {     
            echo '<title itemprop="name">';
            if (is_archive()) { 
                wp_title('');
                // echo ' - ';
            } elseif (is_search()) { 
                echo 'Resultados de la búsqueda &quot;'.wp_specialchars($_GET['s']).'&quot; - '; 
            } elseif (!(is_404()) && (is_single()) || (is_page())) { 
                if(!is_front_page()) {
                    global $post;
                    if(is_single() && get_post_type($post->ID) == 'beneficios') {
                        $merchant = get_field('merchant', $post->ID);
                        if($merchant) {
                            echo $merchant->post_title;
                            echo ' - ';
                        }
                        echo wp_title('');
                    } else {
                        echo wp_title('');
                        echo ' - ';
                    }
                } 
            } elseif (is_404()) {
                echo 'Página no encontrada - '; 
            }
            if (!(is_single()) && !(is_archive())) { 
                bloginfo('name'); 
            }
            echo '</title>' . "\n";
        }
    }
     
    function insert_fb_in_head() {
        global $post;
        if ( !is_singular())
            return;
            // echo '<meta property="fb:admins" content="YOUR USER ID"/>'; get_the_title()
            if(get_post_type($post->ID) == 'eventos') {
                $titulo = '¡Disfruta de eventos exclusivos para ti!';
                $descripcion = '';
            } else if(get_post_type($post->ID) == 'beneficios') {
                $term = get_the_category($post->ID);
                $categoria = $term[0] && $term[0]->parent == '0' ? $term[0]->term_id : $term[0]->parent;
                $term = get_term($categoria, 'category');
                $cat = $term->name;
                $titulo = $cat.' con El Club El Nuevo Día';
                if($cat == 'Moda') {
                    $descripcion = 'Vístete siempre a la moda con el Club El Nuevo Día';
                } else if($cat == 'Gastronomía') {
                    $descripcion = 'Complace tu paladar con las ofertas de Club El Nuevo Día';
                } else if($cat == 'Turismo') {
                    $descripcion = 'Viaja y explora con el Club El Nuevo Día';
                } else if($cat == 'Servicios') {
                    $descripcion = 'El Club El Nuevo Día te trae servicios con grandes descuentos';
                } else if($cat == 'Otros') {
                    $descripcion = 'Disfruta de estas incomparables ofertas del Club El Nuevo Día';
                } else {
                    $descripcion = 'Cuídate bien con el Club El Nuevo Día';
                }
            } else if(get_post_type($post->ID) == 'post') {
                $titulo = 'Club El Nuevo Día te recomienda esta nota.';
                $descripcion = '';
            } else {
                $titulo = '¡Aprovecha estas ofertas del Club El Nuevo Día!';
                $descripcion = '';
            }
            echo '<meta property="og:locale:alternate" content="es_ES" />';
            echo '<meta property="og:title" content="' . $titulo . '"/>';
            echo '<meta property="og:description" content="' . $descripcion . '"/>';
            echo '<meta property="og:type" content="article" />';
            echo '<meta property="og:url" content="' . get_permalink() . '"/>';
            echo '<meta property="og:site_name" content="Club ENDI"/>';
        if(!has_post_thumbnail( $post->ID )) {
            if(!is_front_page())
                $default_image = get_site_url()."/wp-content/uploads/2018/12/acerca.png";
            else
                $default_image = get_site_url()."/wp-content/uploads/2019/02/logo_fb.jpg";
            echo '<meta property="og:image" content="' . $default_image . '"/>';
        } else {
            if(get_post_type($post->ID) == 'post') {
                $imagen = get_field('encabezado', $post->ID);
                echo '<meta property="og:image" content="'.$imagen.'"/>';
            } else {
                $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
                echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
            }
        }
        echo "
    ";
    }
    add_action( 'wp_head', 'insert_fb_in_head', 5 );

    function wps_change_role_name() {
        global $wp_roles;
        if (!isset($wp_roles))
            $wp_roles = new WP_Roles();
        $wp_roles->roles['contributor']['name'] = 'Ver y Añadir';
        $wp_roles->role_names['contributor'] = 'Ver y Añadir';
        $wp_roles->roles['subscriber']['name'] = 'Reportes';
        $wp_roles->role_names['subscriber'] = 'Reportes';
    }
    add_action('init', 'wps_change_role_name');

    // Orden en el front
    function my_pre_get_posts($query) {
        if(!is_admin()) {
            $query->set( 'posts_per_page', -1 );
            if( is_search() && $query->is_main_query() ) {
                $query->set('orderby', 'meta_value');
                $query->set('order', 'ASC');
                $query->set('meta_key', 'validez');
                $query->set('post_type', ['beneficios', 'eventos']);
                $query->set('meta_query', array(
                    'relation' => 'OR',
                    array(
                        'exclusivo' => array(
                            'key' => 'exclusivo',
                            'compare' => '!=',
                            'value' => 'si'
                        ),
                    ),
                    array(
                        'exclusivo' => array(
                            'key' => 'exclusivo',
                            'compare' => 'NOT EXISTS',
                        ),
                    ),
                ));
            } 
        }
    }
    add_action( 'pre_get_posts', 'my_pre_get_posts' );

    function remove_my_post_metaboxes() {
      remove_meta_box( 'tagsdiv-post_tag','post','normal' );
    }
    add_action('admin_menu','remove_my_post_metaboxes');

    function revcon_change_post_label() {
        global $menu;
        global $submenu;
        if (!current_user_can('subscriber')) {
            $menu[5][0] = 'Listas curadas';
            $submenu['edit.php'][5][0] = 'Listas curadas';
            $submenu['edit.php'][10][0] = 'Nueva lista curada';
        }
    }

    function revcon_change_post_object() {
        global $wp_post_types;
        $labels = &$wp_post_types['post']->labels;
        $labels->name = 'Listas curadas';
        $labels->singular_name = 'Listas curadas';
        $labels->add_new = 'Nueva lista';
        $labels->add_new_item = 'Nueva lista';
        $labels->edit_item = 'Editar lista';
        $labels->new_item = 'Nueva lista';
        $labels->view_item = 'Ver lista';
        $labels->search_items = 'Buscar lista';
        $labels->not_found = 'No se encontraron listas';
        $labels->not_found_in_trash = 'No se encontraron listas';
        $labels->all_items = 'Todas las listas curadas';
        $labels->menu_name = 'Listas curadas';
        $labels->name_admin_bar = 'Listas curadas';
    }
    add_action( 'admin_menu', 'revcon_change_post_label' );
    add_action( 'init', 'revcon_change_post_object' );

    function reorder_admin_menu( $__return_true ) {
        return array(
             'index.php', // Dashboard
             'edit.php?post_type=merchants', //Merchants
             'edit.php?post_type=beneficios', //Beneficios
             'generador-codigos-endi', //Códigos
             'edit.php?post_type=eventos', //Eventos
             'sorteo-usuarios', //Sorteos
             'edit.php?post_type=archivos', //Archivos
             'edit.php', // Posts
             'edit.php?post_type=page', // Pages
             'edit.php?post_type=sliders', //Sliders
             'modulo-reportes-endi', //Reportes
             'separator1', // --Space--
             'upload.php', // Media
             'wpcf7', //Contactos
             'separator2', // --Space--
             'themes.php', // Appearance
             'edit-comments.php', // Comments
             'users.php', // Users
             'plugins.php', // Plugins
             'tools.php', // Tools
             'options-general.php', // Settings
             'separator-last', // Last separator
       );
    }
    add_filter( 'custom_menu_order', 'reorder_admin_menu' );
    add_filter( 'menu_order', 'reorder_admin_menu' );

    function admin_enqueue_endi($hook) {
        if ('toplevel_page_sorteo-usuarios' != $hook) {
            return;
        }
        wp_enqueue_style('tokenize2', get_template_directory_uri() . '/css/tokenize2.css');
        wp_enqueue_script('jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js', array('jquery'), '', false);
        wp_enqueue_script('tokenize2', get_template_directory_uri() . '/js/tokenize2.js', array('jquery', 'jquery-ui'), '', false);
        wp_enqueue_script('admin_script', get_template_directory_uri() . '/js/admin.js', array('jquery', 'jquery-ui'), '', false);
        wp_localize_script('admin_script', 'admin_url', array('ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    }
    add_action( 'admin_enqueue_scripts', 'admin_enqueue_endi' );

    function __search_by_title_only( $search, $wp_query ) {
        global $wpdb;
        if(empty($search)) {
            return $search;
        }
        $q = $wp_query->query_vars;
        $n = !empty($q['exact']) ? '' : '%';
        $search =
        $searchand = '';
        foreach ((array)$q['search_terms'] as $term) {
            $term = esc_sql($wpdb->esc_like($term));
            $search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
            $searchand = ' AND ';
        }
        if (!empty($search)) {
            $search = " AND ({$search}) ";
        }
        return $search;
    }
    // add_filter('posts_search', '__search_by_title_only', 500, 2);

    function filter_plugin_updates( $value ) {
        unset( $value->response['acf-repeater-master/acf-repeater.php'] );
        return $value;
    }
    add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );

    function remove_core_updates(){
        global $wp_version; return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
    }
    add_filter('pre_site_transient_update_core','remove_core_updates');
    add_filter('pre_site_transient_update_plugins','remove_core_updates');
    add_filter('pre_site_transient_update_themes','remove_core_updates');

    function agregar_logo() {
        add_theme_support( 'custom-logo', array(
            'height'      => 89,
            'width'       => 265,
            'flex-height' => true,
        ) );
    }
    add_action( 'after_setup_theme', 'agregar_logo', 11 );

    function logo_personalizado() { ?>
        <style type="text/css">
            #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
            }
        </style>
    <?php }
    add_action( 'login_enqueue_scripts', 'logo_personalizado' );
    
    function logo_url() {
        return home_url();
    }
    add_filter( 'login_headerurl', 'logo_url' );
        
    function logo_title() {
        return 'Club El nuevo día';
    }
    add_filter( 'login_headertitle', 'logo_title' );
    
    function login_css() {
        wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/css/login.css' );
    }
    add_action( 'login_enqueue_scripts', 'login_css' );

    add_filter('cf7_custom_get_categorias', function($choices, $args = []){
	    // $events = get_posts("post_type=beneficios&posts_per_page=-1");
	    $terminos = array(
            'depth'               => 1,
            'echo'                => false,
            'exclude'             => ['1'],
            'hide_empty'          => 0,
            'order'               => 'ASC',
            'orderby'             => 'name',
            'parent'              => 0,
            'taxonomy'            => 'category',
        );
        $categorias = get_terms($terminos);
	    $choices = [];
		foreach($categorias as $categoria) {
			$term = get_term($categoria->term_id, 'category');
			$choices[$term->name] = $term->name;
		}
	    return $choices;
	}, 10, 2);

    add_filter( 'manage_beneficios_posts_columns', 'set_custom_edit_beneficios_columns' );
    function set_custom_edit_beneficios_columns($columns) {
        $columns['descuento'] = __( 'Tipo de beneficio', 'descuento' );
        $columns['validez'] = __( 'Validez', 'validez' );
        $columns['localidad'] = __( 'Municipio', 'localidad' );
        $columns['prioridad'] = __( 'Aparece primero', 'prioridad' );
        $columns['autor'] = __( 'Autor', 'autor' );
        return $columns;
    }

    add_action( 'manage_beneficios_posts_custom_column' , 'custom_beneficios_column', 10, 2 );
    function custom_beneficios_column( $column, $post_id ) {
        switch ( $column ) {
            case 'descuento' :
                $tipo = get_post_meta($post_id, 'descuento_tipo_de_beneficio', true);
                echo $tipo != 'dos' ? ucfirst($tipo) : '2x1';
                break;
            case 'prioridad' :
                echo get_field( 'prioridad', $post_id ) ? 'Sí' : 'No';
                break;
            case 'localidad' :
                $localidades = get_the_terms($post_id, 'localidad');
                $print = '';
                if($localidades) {
                    foreach ($localidades as $localidad) {
                        $print .= $localidad->name.', ';
                    }
                }
                echo rtrim(rtrim($print, ' '), ',');
                break;
            case 'validez' :
                $fecha = get_field('validez', $post_id);
                echo $fecha[6].$fecha[7].'/'.$fecha[4].$fecha[5].'/'.$fecha[0].$fecha[1].$fecha[2].$fecha[3];
                break;
            case 'autor' :
                $author_id = get_post_field ('post_author', $post_id);
                $display_name = get_the_author_meta( 'display_name' , $author_id ); 
                echo $display_name;
                break;
        }
    }

    add_filter( 'manage_eventos_posts_columns', 'set_custom_edit_eventos_columns' );
    function set_custom_edit_eventos_columns($columns) {
        $columns['localidad'] = __( 'Municipio', 'localidad' );
        $columns['fecha'] = __( 'Fecha del evento', 'fecha' );
        $columns['sorteo'] = __( 'Fecha del sorteo', 'sorteo' );
        $columns['inscripcion'] = __( 'Fecha límite de inscripción', 'inscripcion' );
        return $columns;
    }

    add_action( 'manage_eventos_posts_custom_column' , 'custom_eventos_column', 10, 2 );
    function custom_eventos_column( $column, $post_id ) {
        if( have_rows('sorteo', $post_id) ):
            while ( have_rows('sorteo', $post_id) ) : the_row();
                $sorteo = get_sub_field('fecha_del_sorteo');
                $limite = get_sub_field('fecha_limite');
            endwhile;
        endif;
        switch ( $column ) {
            // case 'tipo' :
            //     $categorias = get_the_terms($post_id, 'tipo'); 
            //     foreach($categorias as $categoria) {
            //         echo $categoria->name."<br/>";
            //     }
            //     break;
            case 'localidad' :
                $localidades = get_the_terms($post_id, 'localidad');
                $print = '';
                if(!$localidades)
                    break;
                foreach ($localidades as $localidad) {
                    $print .= $localidad->name.', ';
                }
                echo rtrim(rtrim($print, ' '), ',');
                break;
            case 'fecha' :
                echo get_field( 'fecha' ); 
                break;
            case 'sorteo' :
                echo $sorteo;
                break;
            case 'inscripcion' :
                echo $limite;
                break;
        }
    }

    add_filter( 'manage_archivos_posts_columns', 'set_custom_edit_archivos_columns' );
    function set_custom_edit_archivos_columns($columns) {
        $columns['fecha'] = __( 'Fecha del evento', 'fecha' );
        return $columns;
    }

    add_action( 'manage_archivos_posts_custom_column' , 'custom_archivos_column', 10, 2 );
    function custom_archivos_column( $column, $post_id ) {
        switch ( $column ) {
            case 'fecha' :
                echo get_field( 'fecha' ); 
                break;
        }
    }

    function get_disponibles($post) {
        global $wpdb;
        $disponibles = $wpdb->get_row("SELECT SUM(usos) as total FROM wp_codigos WHERE beneficio_id = '".$post."'");
        return $disponibles->total;
    }

    function get_descuento($post, $from = null) {
        $tipo = get_post_meta($post, 'descuento_tipo_de_beneficio', true);
        switch ($tipo) {
            case 'porcentaje':
                $beneficio = '<p><strong><span>'.get_post_meta($post, 'descuento_porcentaje', true).'</span><span class="small">%</span></strong><span> de descuento</span></p>';
                break;
            case 'fijo':
                $plata = get_post_meta($post, 'descuento_porcentaje', true);
                $beneficio = '<p><strong><span class="small">$ </span><span>'.formatoPrecio($plata).'</span></strong><span> de descuento</span></p>';
                break;
            case 'dos':
                $beneficio = '<p><strong>2x1</strong><span> oferta</span></p>';
                break;
            case 'regalo':
            case 'promo':
                if($from) {
                    // $plata = get_post_meta($post, 'descuento_precio_original', true);
                    // $beneficio = '<p><strong><span class="small">$ </span><span>'.formatoPrecio($plata).'</span></strong><span>de regalo</span></p>';
                    $regalo = get_post_meta($post, 'descuento_porcentaje_box', true);
                    $condicion = get_post_meta($post, 'descuento_condicion_box', true) ? get_post_meta($post, 'descuento_condicion_box', true) : '';
                    $beneficio = '<p class="fixp"><span class="pl-0" style="font-size: 1.1em;">'.$regalo.'</span><br/><span class="small pl-0">'.$condicion.'</span></p>';
                } else {
                    $regalo = get_post_meta($post, 'descuento_porcentaje', true);
                    $condicion = get_post_meta($post, 'descuento_condicion', true) ? get_post_meta($post, 'descuento_condicion', true) : '';
                    $beneficio = '<p class="beneficio_gratis"><span class="pl-0" style="font-size: 1.2em;">'.$regalo.'</span><br/><span class="small pl-0">'.$condicion.'</span></p>';
                }
                break;
            default:
                $beneficio = get_field('beneficio', $post);
                break;
        }
        return $beneficio;
    }

    function get_detalle($post) {
        $beneficio = '<div id="descuento">';
        $precio_original = get_post_meta($post, 'descuento_precio_original', true) ? get_post_meta($post, 'descuento_precio_original', true) : 0;
        $valor_promedio = get_post_meta($post, 'descuento_valor_promedio', true) ? get_post_meta($post, 'descuento_valor_promedio', true) : 0;
        $precio_original = $precio_original ? $precio_original : $valor_promedio;
        $tipo = get_post_meta($post, 'descuento_tipo_de_beneficio', true);
        switch ($tipo) {
            case 'porcentaje':
                $porcentaje = get_post_meta($post, 'descuento_porcentaje', true);
                $texto_porcentaje = round(($porcentaje * $precio_original / 100), 2);
                // $beneficio .= $precio_original ? '<div class="actual">$'.formatoPrecio($texto_porcentaje).' total a</div>' : '';
                $precio_final = $precio_original - $texto_porcentaje;
                break;
            case 'fijo':
                $porcentaje = get_post_meta($post, 'descuento_porcentaje', true);
                $texto_porcentaje = $porcentaje;
                // $beneficio .= $precio_original ? '<div class="actual">$'.formatoPrecio($texto_porcentaje).' Ahorro</div>' : '';
                $precio_final = round($precio_original - $porcentaje, 2); 
                break;
            case 'dos':
                $texto_porcentaje = round($precio_original / 2, 2);
                // $beneficio .= $precio_original ? '<div class="actual">$'.formatoPrecio($texto_porcentaje).' Ahorro</div>' : '';
                $precio_final = $precio_original - $texto_porcentaje;
                break;
            case 'regalo':
                // $porcentaje = get_post_meta($post, 'descuento_porcentaje', true);
                // $texto_porcentaje = $porcentaje;
                // $precio_final = $precio_original - $texto_porcentaje;
                break;
        }

        switch ($tipo) {
            case 'regalo':
                // $beneficio .= '<div class="anterior"><span class="linea"></span>$'.formatoPrecio($precio_original).' valor original</div>';
                $beneficio .= '<div class="actual border-0 ml-1">$'.formatoPrecio($precio_original).' de regalo</div></div>';
                // $beneficio .= '&nbsp;</div>';
                break;
            default:
                $beneficio .= $precio_original ? '<div class="anterior"><span class="linea"></span>$'.formatoPrecio($precio_original).' valor original</div>' : '<div class="anterior"><span class="linea"></span>$'.formatoPrecio($valor_promedio).' valor promedio</div>';
                $beneficio .= '<div class="actual">$'.formatoPrecio($precio_final).' total a pagar</div></div>';
                break;
        }
        return $beneficio;
    }

    function formatearFecha($fecha, $tipo) {
        $meses = ['01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Septiembre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre'];
        if($tipo == 'beneficio') {
            $dia = substr($fecha,8,2);
            $mes = substr($fecha,5,2);
        } else if($tipo == 'single') {
            $dia = substr($fecha,8,2);
            $mes = substr($fecha,5,2);
            $ano = substr($fecha,0,4);
            return $dia.' de '.$meses[$mes].' de '.$ano;
        } else if($tipo == 'chino') {
            $dia = substr($fecha,8,2);
            $mes = substr($fecha,5,2);
            $ano = substr($fecha,0,4);
            return strtolower($meses[$mes]).' '.$dia.' de '.$ano;
        } else if($tipo == 'mobile') {
            $dia = substr($fecha,0,2);
            $mes = substr($fecha,3,2);
            return $dia.' '.strtolower($meses[$mes]);
        } else if($tipo == 'corto') {
            $dia = substr($fecha,8,2);
            $mes = substr($fecha,5,2);
            return $dia.' de '.strtolower($meses[$mes]);
        } else if($tipo == 'corto_evento') {
            $dia = substr($fecha,0,2);
            $mes = substr($fecha,3,2);
            return $dia.' de '.strtolower($meses[$mes]);
        }else if($tipo == 'evento') {
            $dia = substr($fecha,0,2);
            $mes = substr($fecha,3,2);
            $ano = substr($fecha,6,4);
            return $dia.' de '.$meses[$mes];
        } else if($tipo == 'limite') {
            $dia = substr($fecha,8,2);
            $mes = substr($fecha,5,2);
            $ano = substr($fecha,0,4);
            return $dia.'/'.$mes.'/'.$ano;
        } else if($tipo == 'formato') {
            $dia = substr($fecha,0,2);
            $mes = substr($fecha,3,2);
            $ano = substr($fecha,6,4);
            $resto = substr($fecha,11,11);
            return $mes.'-'.$dia.'-'.$ano.' '.$resto;
        } else if($tipo == 'nombre') {
            $fechaot = strtotime($fecha);
            switch (date('w', $fechaot)){ 
                case 0: $nombre = "Domingo"; break; 
                case 1: $nombre = "Lunes"; break; 
                case 2: $nombre = "Martes"; break; 
                case 3: $nombre = "Miércoles"; break; 
                case 4: $nombre = "Jueves"; break; 
                case 5: $nombre = "Viernes"; break; 
                case 6: $nombre = "Sábado"; break; 
            } 
            $dia = substr($fecha,0,2);
            $mes = substr($fecha,3,2);
            $hora = date('g:i a', $fechaot);
            return $nombre.' '.$dia.' de '.$meses[$mes].' '.$hora;
            // substr($fecha,11,5).' hs'
        } else if($tipo == 'mes') {
            $mes = substr($fecha,3,2);
            return strtolower($meses[$mes]);
        } else if($tipo == 'fix') {
            $dia = substr($fecha,0,2);
            $mes = substr($fecha,3,2);
            $ano = substr($fecha,6,4);
            return $dia.'.'.$mes.'.'.$ano;
        } else {
            $dia = substr($fecha,0,2);
            $mes = substr($fecha,3,2);
        }
        return $dia.' de '.$meses[$mes];
    }

    function achatarArray($item) {
        $item = $item['post'];
        return $item;
    }

    function filtroVencidos($item) {
        $elemento = isset($item['beneficio_eventos']) ? $item['beneficio_eventos'] : $item['destacado_beneficios'];
        return $elemento;
    }

    function removerVencidos($item) {
        $tipo = get_post_type($item);
        $validez = get_field('validez', $item);
        if($tipo == 'slides') {
            return true;
        }
        $date = $tipo == 'eventos' ? DateTime::CreateFromFormat('d/m/Y H:i a', $validez) : DateTime::CreateFromFormat('Y-m-d', $validez);
        $dt = new DateTime();
        if($date->format('Y-m-d H:i:s') >= $dt->format('Y-m-d H:i:s'))
            return true;
        else
            return false;
    }

    // Orden en el back
    function set_post_order_in_admin($query) {
        global $pagenow;
        if ( 0 && is_admin() && 'edit.php' == $pagenow && !isset($_GET['orderby']) && isset($_GET['post_type']) && $_GET['post_type'] == 'beneficios') {
            $query->set( 'meta_key', 'prioridad' );
            $query->set( 'orderby', array( 'meta_value_num' => 'DESC', 'validez' => 'ASC' ) );
            $query->set( 'order', 'DESC' );       
        }
        if(is_admin() && 'edit.php' == $pagenow && !isset($_GET['orderby']) && isset($_GET['post_type']) && ($_GET['post_type'] == 'beneficios' || $_GET['post_type'] == 'eventos' || $_GET['post_type'] == 'archivos')) {
            $query->set( 'orderby', 'date' );
            $query->set( 'order', 'DESC' ); 
        }
    }
    add_filter('pre_get_posts', 'set_post_order_in_admin', 5 );

    function my_custom_taxonomy_columns( $columns ) {
        $columns['color'] = __('Color');
        // $columns['estado'] = __('Estado');
        return $columns;
    }
    add_filter('manage_edit-category_columns' , 'my_custom_taxonomy_columns');

    function add_category_column_content($content,$column_name,$term_id){
        $term= get_term($term_id, 'category');
        switch ($column_name) {
            case 'color':
                $color = get_field('color', $term);
                $content = $color ? "<span style='display: block; width: 21px; height: 21px; background: ".$color."; border: 1px solid grey;'></span>" : "";
                break;
            // case 'estado':
            //     $content = get_field('estado', $term) == 0 ? 'Oculto' : 'Visible';
            //     break;
            default:
                $content = '';
                break;
        }
        return $content;
    }
    add_filter('manage_category_custom_column', 'add_category_column_content', 10, 3);

    function theme_header_metadata() {
        global $post;
        if($post && 0) {
    ?>
        <meta property="og:url" content="<?php echo "http://".$_SERVER['HTTP_HOST'].get_permalink($post->ID) ?>" />
        <meta property="og:title" content="<?php echo $post->post_title; ?>" />
        <meta property="og:type" content="article" />
    <?php
        }
    }
    add_action( 'wp_head', 'theme_header_metadata' );

    function club_remove_wp_version() {
        return '';
    }
    add_filter('the_generator', 'club_remove_wp_version');

    add_action('wp_ajax_traer_categorias', 'traer_categorias' );
    add_action('wp_ajax_nopriv_traer_categorias', 'traer_categorias' );
    function traer_categorias($tipo) {
        if($_POST['tipo'] == 'eventos') {
            $terminos = array(
                'depth'               => 1,
                'echo'                => false,
                'hide_empty'          => 0,
                'order'               => 'ASC',
                'orderby'             => 'name',
                'parent'              => 47,
                'taxonomy'            => 'category',
                'title_li'            => '',
            );
        } else {
            $terminos = array(
                'depth'               => 1,
                'echo'                => false,
                'exclude'             => ['1', '47'],
                'hide_empty'          => 0,
                'order'               => 'ASC',
                'orderby'             => 'name',
                'parent'              => 0,
                'taxonomy'            => 'category',
                'title_li'            => '',
            );
        }
        
        $resultado = [];
        $categorias = get_terms($terminos);
        foreach($categorias as $categoria) {
            $resultado[$categoria->slug] = htmlspecialchars_decode($categoria->name);
        }
        echo json_encode($resultado);
        die();
    }

    function traerCategorias() {
        $terminos = array(
            'depth'               => 1,
            'echo'                => false,
            'exclude'             => ['1', '47'],
            'hide_empty'          => 0,
            'order'               => 'ASC',
            'orderby'             => 'name',
            'parent'              => 0,
            'taxonomy'            => 'category',
            'title_li'            => '',
        );

        $resultado = [];
        $categorias = get_terms($terminos);
        foreach($categorias as $categoria) {
            $temp = [];
            $term = get_term($categoria->term_id, 'category');
            $temp['nombre'] = $categoria->name;
            $temp['slug'] = $categoria->slug;
            $temp['color'] = get_field('color', $term);
            $resultado[] = $temp;
        }
        return $resultado;
    }

    function traerSubcategorias() {
        $terminos = array(
            'depth'               => 1,
            'echo'                => false,
            'exclude'             => ['1', '47'],
            'hide_empty'          => 0,
            'order'               => 'ASC',
            'orderby'             => 'name',
            'parent'              => 47,
            'taxonomy'            => 'category',
            'title_li'            => '',
        );

        $resultado = [];
        $categorias = get_terms($terminos);
        foreach($categorias as $categoria) {
            $temp = [];
            $term = get_term($categoria->term_id, 'category');
            $temp['nombre'] = $categoria->name;
            $temp['slug'] = $categoria->slug;
            $temp['color'] = get_field('color', $term);
            $resultado[] = $temp;
        }
        return $resultado;
    }

    function traerDeTodo($cat_ids) {
        if(count($cat_ids) < 1)
            return [];
        $terminos = array(
            'depth'               => 1,
            'echo'                => false,
            // 'exclude'             => ['1', '47'],
            // 'hide_empty'          => true,
            'order'               => 'ASC',
            'orderby'             => 'name',
            // 'parent'              => 0,
            'taxonomy'            => 'category',
            'title_li'            => '',
            'include'             => $cat_ids
        );

        $resultado = [];
        $categorias = get_terms($terminos);
        foreach($categorias as $categoria) {
            $temp = [];
            $term = get_term($categoria->term_id, 'category');
            $temp['nombre'] = $categoria->name;
            $temp['slug'] = $categoria->slug;
            $temp['color'] = get_field('color', $term);
            $resultado[] = $temp;
        }
        return $resultado;

        $subterminos = array(
            'depth'               => 1,
            'echo'                => false,
            'hide_empty'          => true,
            'order'               => 'ASC',
            'orderby'             => 'name',
            'parent'              => 47,
            'taxonomy'            => 'category',
            'title_li'            => '',
        );
        $subcategorias = get_terms($subterminos);
        foreach($subcategorias as $subcategoria) {
            $temp = [];
            $term = get_term($subcategoria->term_id, 'category');
            $temp['nombre'] = $subcategoria->name;
            $temp['slug'] = $subcategoria->slug;
            $temp['color'] = get_field('color', $term);
            $resultado[] = $temp;
        }
        usort($resultado, function($a, $b) {
            return strcmp($a['nombre'], $b['nombre']);
        });
        return $resultado;
    }

    function traerCupones() {
        if(!$_COOKIE['id'])
            return [];
        global $wpdb;
        $cupones = $wpdb->get_results("SELECT * FROM wp_cupones WHERE usuario_id = '".$_COOKIE['id']."'");
        $resultado = [];
        foreach($cupones as $cupon) {
            $resultado[] = $cupon->cupon_id;
        }
        return $resultado;
    }

    add_action('wp_ajax_traer_cupones', 'traer_cupones' );
    add_action('wp_ajax_nopriv_traer_cupones', 'traer_cupones' );
    function traer_cupones() {
        global $wpdb;
        // $usuario = $wpdb->get_col("SELECT CustID FROM wp_usuarios WHERE id = '".$_COOKIE['id']."'");
        //echo "SELECT CustID FROM wp_usuarios WHERE id = '".$_COOKIE['id']."'";
        // $CustID = $usuario[0];
        //$usuarios = $wpdb->get_col("SELECT id FROM wp_usuarios WHERE CustID = '".$CustID."'");
        //$query = '
        //  SELECT p.*, c.created_at as agregado, c.ID as cupon FROM '. $wpdb->posts .' p
        //  INNER JOIN wp_cupones c ON p.ID = c.cupon_id WHERE c.usuario_id IN ('.implode(",", $usuarios).') AND p.post_status="publish" ORDER BY agregado DESC
        //';
        $CustID = $_COOKIE['CustID'];
        if($_POST['tipo'] == 'eventos') {
            $query = '
              SELECT p.*, c.created_at as agregado, c.ID as cupon FROM '. $wpdb->posts .' p
              INNER JOIN wp_eventos c ON p.ID = c.evento_id WHERE c.CustID ='.$CustID.' AND ganador="Y" AND p.post_status="publish" ORDER BY agregado DESC
            ';
        } else {
            $query = '
              SELECT p.*, c.created_at as agregado, c.ID as cupon FROM '. $wpdb->posts .' p
              INNER JOIN wp_cupones c ON p.ID = c.cupon_id WHERE c.CustID ='.$CustID.' AND p.post_status="publish" ORDER BY agregado DESC
            ';
        }
        //echo  $query;
        $results = $wpdb->get_results($query, OBJECT);
        $posts = [];
        $x = 0;
        $categorias = array();
        foreach($results as $post) {
            $posts[$x] = $post;
            $posts[$x]->url = get_permalink($post->ID);
            $categoria = get_post_type($post->ID);
            $imagen = get_field('thumbnail', $post->ID) ? get_field('thumbnail', $post->ID) : get_the_post_thumbnail_url($post->ID);
            $posts[$x]->imagen = $imagen;
            $term = get_the_category($post->ID);
            if($_POST['tipo'] == 'eventos') {
                $categoria = $term[0] && $term[0]->parent == '47' ? $term[0]->term_id : $term[0]->parent;
            } else {
                $categoria = $term[0] && $term[0]->parent == '0' ? $term[0]->term_id : $term[0]->parent;
            }
            $term = get_term($categoria, 'category');
            if(!isset($categorias[$term->slug]))
                $categorias[$term->slug] = $term->name;
            $posts[$x]->categoria = $term->slug;
            $color = get_field('color', $term) ? get_field('color', $term) : '#001CB4';
            $posts[$x]->color = $color;
            $beneficio = get_descuento($post->ID, 'box');
            $posts[$x]->beneficio = $beneficio;
            if($_POST['tipo'] == 'eventos') {
                $vencimiento = get_field('sorteo_fecha_del_sorteo', $post->ID);
                $posts[$x]->vencimiento = $vencimiento;
                $posts[$x]->vencimiento_formateado = formatearFecha($vencimiento, 'corto_evento');
                $fecha = get_field('validez', $post->ID);
                $posts[$x]->fecha = $fecha;
                $posts[$x]->fecha_formateada = formatearFecha($fecha, 'corto_evento');
            } else {
                $vencimiento = get_field('validez', $post->ID);
                $posts[$x]->vencimiento = $vencimiento;
                $posts[$x]->vencimiento_formateado = formatearFecha($vencimiento, 'corto');
                $posts[$x]->vencido = $vencimiento < date('Y-m-d');
            }
            $exclusivo = get_field('exclusivo', $post->ID);
            $posts[$x]->exclusivo = $exclusivo;
            if($_POST['tipo'] == 'eventos') {
                $posts[$x]->vencido = false;
            } else {
                if($exclusivo == 'si') {
                    if(!isset($_COOKIE['nuevo']) || $_COOKIE['nuevo'] == '1') {
                        $posts[$x]->vencido = $vencimiento < date('Y-m-d');
                    } else {
                        $posts[$x]->vencido = true;
                    }
                } else {
                    $posts[$x]->vencido = $vencimiento < date('Y-m-d');
                }
            }
            $merchant = get_field('merchant', $post->ID);
            $ademas = $merchant ? $merchant->post_title : '';
            $posts[$x]->titulo = $ademas ? $ademas : $post->post_title;
            $x++;
        }
        $resultado = array();
        $resultado['lista_ajax'] = $posts;
        $resultado['categorias'] = $categorias;
        echo json_encode( $resultado, true );
        die();
    }

    function my_nav_wrap() {
        $wrap  = '<ul id="%1$s" class="%2$s">';
        $wrap .= '%3$s';
        // $terminos = array(
        //     'depth'               => 1,
        //     'echo'                => false,
        //     'exclude'             => ['1'],
        //     'hide_empty'          => 0,
        //     'order'               => 'ASC',
        //     'orderby'             => 'name',
        //     'parent'              => 0,
        //     'taxonomy'            => 'category',
        //     'title_li'            => '',
        // );
        // $categorias = get_terms($terminos);
        // foreach($categorias as $categoria) {
        //     $term = get_term($categoria->term_id, 'category');
        //     $color = get_field('color', $term);
        //     $estado = get_field('estado', $term);
        //     if($estado == 1)
        //         $wrap .= '<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" class="menu-item menu-item-type-custom menu-item-object-custom nav-item" style="border-color: '.$color.'; color: '.$color.';"><a title="'.$categoria->name.'" href="/category/'.$categoria->slug.'/" class="nav-link">'.$categoria->name.'</a></li>';
        // }
        $wrap .= '</ul>';
        return $wrap;
    }

    add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
    function special_nav_class($classes, $item){
        if( in_array('current-menu-item', $classes) ){
            $classes[] = 'active ';
        }
        if( 'category' == $item->object ){
            $category = get_category( $item->object_id );
            $classes[] = 'menu-' . $category->slug;
        }

        global $post;
        if( is_object($post) && ($post->post_type == 'beneficios' || $post->post_type == 'eventos')) {
            $categories = get_the_category($post->ID);
            if(!empty($categories)) {
                $slug = $categories[0]->slug;
                $categoria = $slug;
                $parent = $categories[0]->category_parent;
                if($parent > 0) {
                    $padre = get_category($parent);
                    $categoria= $padre->slug;
                }
            }
            $menu_slug = strtolower(trim($item->url));
            if (strpos($menu_slug,$categoria) !== false) {
               $classes[] = 'active';
            }
        }
        return $classes;
    }

    function add_menuclass($ulclass) {
       return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
    }
    // add_filter('wp_nav_menu','add_menuclass');

    function club_change_admin_footer () {
        return;
        echo 'Desarrollado por <a href="http://www.dinardi.com.ar" target="_blank">Dinardi Project</a></p>';
    }
    add_filter('admin_footer_text', 'club_change_admin_footer');

    add_action('wp_dashboard_setup', 'club_dashboard_msg');
    function club_dashboard_msg() {
        global $wp_meta_boxes;
        wp_add_dashboard_widget('custom_help_widget', 'Club WP - Message', 'club_admin_msg_text');
    }

    function club_admin_msg_text() {
        echo '<p>¡Bienvenido!</p>';
        // echo '<p>Theme desarrollado por <a href="http://www.dinardi.com.ar">Dinardi Project</a></p>';
    }

    function club_footer_menu() {
      register_nav_menu('footer-menu',__( 'Footer Menu' ));
    }
    add_action( 'init', 'club_footer_menu' );

    function club_acerca_menu() {
      register_nav_menu('acerca-menu',__( 'Acerca Menu' ));
    }
    add_action( 'init', 'club_acerca_menu' );

    function club_widget_area() {
      $args = array(
        'id'            => 'club_sidebar',
        'name'          => __( 'Special Footer Text', 'text_domain' ),
        'description'   => __( 'Prints out special text added from the widget page', 'text_domain' ),
        'before_title'  => '<div class="club_sidebar">',
        'after_title'   => '</div>',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
      );
      register_sidebar( $args );
    }
    add_action( 'widgets_init', 'club_widget_area' );

    function club_remove_search( $query, $error = true ) {
        if ( is_search() ) {
            $query->is_search = false;
            $query->query_vars[s] = false;
            $query->query[s] = false;
        // to error
        if ( $error == true )
            $query->is_404 = true;
        }
    }
    // add_action( 'parse_query', 'club_remove_search' );
    // add_filter( 'get_search_form', create_function( '$a', "return null;" ) );

    function change_read_more() {
        return '<a class="readmore" href="' . get_permalink() . '">...</a>';
    }
    add_filter( 'the_content_more_link', 'change_read_more' );

    function club_enqueue() {
        wp_enqueue_style('bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
        wp_enqueue_style('slick', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css');
        wp_enqueue_style('animate', '//cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css');
        wp_enqueue_style('style', get_template_directory_uri() . '/style.css?v='.rand(0,1000), array(), null);
        wp_enqueue_style('responsive', get_template_directory_uri() . '/css/responsive.css');
        wp_enqueue_style('fontawesome', '//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
        wp_enqueue_style('fancybox', '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css');
        wp_enqueue_script('googletag', '//www.googletagservices.com/tag/js/gpt.js', false, '', true);
        // wp_enqueue_script('promise-auto', 'https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.js', false, '', true);
        // wp_enqueue_script('babel', 'https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/7.2.5/polyfill.min.js', false, '', true);
        wp_enqueue_script('popper', '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array('jquery'), '', false);
        wp_enqueue_script('bootstrapcdn', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array('jquery'), '', false);
        wp_enqueue_script('slick', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array('jquery'), '', false);
        wp_enqueue_script('isotope', '//unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js', array('jquery'), '', false);
        wp_enqueue_script('fancybox', '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js', array('jquery'), '', false);
        wp_enqueue_script('maps', '//maps.google.com/maps/api/js?key=AIzaSyApf0dYSpWCukWXKtLKOIbVgGk1lDmfVNk', array('jquery'), '', false);
        wp_enqueue_script('scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), null, false);
        wp_localize_script('scripts', 'scripts', array(
            'siteurl' => get_option('siteurl'),
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'screenset_gigya' => SCREENSET_GIGYA,
        ));
        $categorias = traerCategorias();
        $custom_css = '';
        foreach($categorias as $categoria) {
            $custom_css .= "
                body #menu #menu-menu-principal li.menu-{$categoria['slug']}.active, body #menu #menu-menu-principal li.menu-{$categoria['slug']}:hover {
                    border-bottom: 5px solid {$categoria['color']}!important;
                }";
        }
        wp_add_inline_style( 'style', $custom_css );
    }
    add_action('wp_enqueue_scripts', 'club_enqueue');

    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    register_nav_menus( array(
        'header' => 'Custom Primary Menu',
    ) );
    function club_widgets_init() {
      register_sidebar( array(
        'name'          => 'Redes sociales',
        'id'            => 'redes',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="d-none">',
        'after_title'   => '</h4>',
      ) );
    }
    add_action( 'widgets_init', 'club_widgets_init' );

    add_filter( 'get_the_archive_title', function ( $title ) {
        if( is_category() ) {
            $title = single_cat_title( '', false );
        }
        return $title;
    });

    function obtenerIDs($obj) {
        return $obj->term_id;
    }

    remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );

    function cptui_register_my_cpts() {
        // flush_rewrite_rules( true );
        $labels = array(
            "name" => __( "Slides", "DP_Cupon" ),
            "singular_name" => __( "Slide", "DP_Cupon" ),
            "menu_name" => __( "Slides", "DP_Cupon" ),
            "all_items" => __( "Slides", "DP_Cupon" ),
            "add_new" => __( "Nuevo slide", "DP_Cupon" ),
            "add_new_item" => __( "Nuevo slide", "DP_Cupon" ),
            "edit_item" => __( "Editar slide", "DP_Cupon" ),
            "new_item" => __( "Nuevo slide", "DP_Cupon" ),
            "view_item" => __( "Ver slide", "DP_Cupon" ),
            "view_items" => __( "Ver slides", "DP_Cupon" ),
            "search_items" => __( "Buscar", "DP_Cupon" ),
            "not_found" => __( "No hay resultado", "DP_Cupon" ),
            "not_found_in_trash" => __( "No hay resultado", "DP_Cupon" ),
            "parent_item_colon" => __( "Slide padre", "DP_Cupon" ),
            "featured_image" => __( "Imagen del slide", "DP_Cupon" ),
            "set_featured_image" => __( "Imagen del slide", "DP_Cupon" ),
            "remove_featured_image" => __( "Quitar imagen del slide", "DP_Cupon" ),
            "use_featured_image" => __( "Usar como imagen del slide", "DP_Cupon" ),
            "archives" => __( "Slides", "DP_Cupon" ),
            "insert_into_item" => __( "Insertar en slide", "DP_Cupon" ),
            "uploaded_to_this_item" => __( "Subir al slide", "DP_Cupon" ),
            "filter_items_list" => __( "Filtrar slides", "DP_Cupon" ),
            "items_list_navigation" => __( "Slides", "DP_Cupon" ),
            "items_list" => __( "Slide", "DP_Cupon" ),
            "attributes" => __( "Atributos de los slides", "DP_Cupon" ),
            "name_admin_bar" => __( "Slide", "DP_Cupon" ),
            "parent_item_colon" => __( "Slide padre", "DP_Cupon" ),
        );

        $args = array(
            "label" => __( "Slides", "DP_Cupon" ),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => false,
            "show_ui" => true,
            "delete_with_user" => false,
            "show_in_rest" => true,
            "rest_base" => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive" => false,
            "show_in_menu" => false,
            "show_in_nav_menus" => false,
            "exclude_from_search" => true,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => true,
            "rewrite" => array( "slug" => "slides", "with_front" => false ),
            "query_var" => true,
            "menu_position" => 8,
            "menu_icon" => "dashicons-images-alt",
            "supports" => array( "title", "thumbnail" ),
        );
        register_post_type( "slides", $args );

        $labels = array(
            "name" => __( "Beneficios", "DP_Cupon" ),
            "singular_name" => __( "Beneficio", "DP_Cupon" ),
            "menu_name" => __( "Beneficios", "DP_Cupon" ),
            "all_items" => __( "Beneficios", "DP_Cupon" ),
            "add_new" => __( "Agregar beneficio", "DP_Cupon" ),
            "add_new_item" => __( "Agregar beneficio", "DP_Cupon" ),
            "edit_item" => __( "Editar beneficio", "DP_Cupon" ),
            "new_item" => __( "Nuevo beneficio", "DP_Cupon" ),
            "view_item" => __( "Ver beneficio", "DP_Cupon" ),
            "view_items" => __( "Ver beneficios", "DP_Cupon" ),
            "search_items" => __( "Buscar beneficio", "DP_Cupon" ),
            "not_found" => __( "Beneficio no encontrado", "DP_Cupon" ),
            "not_found_in_trash" => __( "Beneficio no encontrado", "DP_Cupon" ),
            "parent_item_colon" => __( "Beneficio padre", "DP_Cupon" ),
            "featured_image" => __( "Imagen del beneficio (desktop)", "DP_Cupon" ),
            "set_featured_image" => __( "Imagen del beneficio", "DP_Cupon" ),
            "remove_featured_image" => __( "Quitar imagen del beneficio", "DP_Cupon" ),
            "use_featured_image" => __( "Usar como imagen del beneficio", "DP_Cupon" ),
            "archives" => __( "Beneficios", "DP_Cupon" ),
            "insert_into_item" => __( "Insertar en beneficio", "DP_Cupon" ),
            "uploaded_to_this_item" => __( "Subir al beneficio", "DP_Cupon" ),
            "filter_items_list" => __( "Filtrar beneficios", "DP_Cupon" ),
            "items_list_navigation" => __( "Beneficios", "DP_Cupon" ),
            "items_list" => __( "Beneficios", "DP_Cupon" ),
            "attributes" => __( "Atributos de los beneficios", "DP_Cupon" ),
            "name_admin_bar" => __( "Beneficio", "DP_Cupon" ),
            "parent_item_colon" => __( "Beneficio padre", "DP_Cupon" ),
        );

        $args = array(
            "label" => __( "Beneficios", "DP_Cupon" ),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "delete_with_user" => false,
            "show_in_rest" => true,
            "rest_base" => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive" => true,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            // "capability_type" => "beneficio",
            'map_meta_cap' => true,
            "hierarchical" => true,
            "rewrite" => array( "slug" => "beneficios", "with_front" => false, 'feeds' => false, 'pages' => false ),
            // , "permastruct" => "/%category%/%field_merchant_slug%/%postname%/"
            "query_var" => true,
            "menu_position" => 5,
            "menu_icon" => "dashicons-star-empty",
            "supports" => array( "title", "editor", "thumbnail" ),
            "taxonomies" => array( "category", "localidad" ),
        );
        register_post_type( "beneficios", $args );

        $labels = array(
            "name" => __( "Eventos", "DP_Cupon" ),
            "singular_name" => __( "Evento", "DP_Cupon" ),
            "menu_name" => __( "Eventos", "DP_Cupon" ),
            "all_items" => __( "Eventos", "DP_Cupon" ),
            "add_new" => __( "Agregar evento", "DP_Cupon" ),
            "add_new_item" => __( "Agregar evento", "DP_Cupon" ),
            "edit_item" => __( "Editar evento", "DP_Cupon" ),
            "new_item" => __( "Nuevo evento", "DP_Cupon" ),
            "view_item" => __( "Ver evento", "DP_Cupon" ),
            "view_items" => __( "Ver eventos", "DP_Cupon" ),
            "search_items" => __( "Buscar evento", "DP_Cupon" ),
            "not_found" => __( "Evento no encontrado", "DP_Cupon" ),
            "not_found_in_trash" => __( "Evento no encontrado", "DP_Cupon" ),
            "parent_item_colon" => __( "Evento padre", "DP_Cupon" ),
            "featured_image" => __( "Imagen del evento (desktop)", "DP_Cupon" ),
            "set_featured_image" => __( "Imagen del evento", "DP_Cupon" ),
            "remove_featured_image" => __( "Quitar imagen del evento", "DP_Cupon" ),
            "use_featured_image" => __( "Usar como imagen del evento", "DP_Cupon" ),
            "archives" => __( "Eventos", "DP_Cupon" ),
            "insert_into_item" => __( "Insertar en evento", "DP_Cupon" ),
            "uploaded_to_this_item" => __( "Subir al evento", "DP_Cupon" ),
            "filter_items_list" => __( "Filtrar eventos", "DP_Cupon" ),
            "items_list_navigation" => __( "Eventos", "DP_Cupon" ),
            "items_list" => __( "Eventos", "DP_Cupon" ),
            "attributes" => __( "Atributos de los eventos", "DP_Cupon" ),
            "name_admin_bar" => __( "Evento", "DP_Cupon" ),
            "parent_item_colon" => __( "Evento padre", "DP_Cupon" ),
        );

        $args = array(
            "label" => __( "Eventos", "DP_Cupon" ),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "delete_with_user" => false,
            "show_in_rest" => true,
            "rest_base" => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive" => true,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => true,
            "rewrite" => array( "slug" => "eventos", "with_front" => false, 'feeds' => false, 'pages' => false ),
            // "permastruct" => "/%category%/%field_merchant_slug%/%postname%/"
            "query_var" => true,
            "menu_position" => 6,
            "menu_icon" => "dashicons-megaphone",
            "supports" => array( "title", "editor", "thumbnail" ),
            "taxonomies" => array( "tipo", "localidad", "category" ),
        );
        register_post_type( "eventos", $args );

        $labels = array(
            "name" => __( "Evento pasado", "DP_Cupon" ),
            "singular_name" => __( "Eventos pasados", "DP_Cupon" ),
            "menu_name" => __( "Eventos pasados", "DP_Cupon" ),
            "all_items" => __( "Eventos pasados", "DP_Cupon" ),
            "add_new" => __( "Agregar evento pasado", "DP_Cupon" ),
            "add_new_item" => __( "Agregar evento pasado", "DP_Cupon" ),
            "edit_item" => __( "Editar evento pasado", "DP_Cupon" ),
            "new_item" => __( "Nuevo evento pasado", "DP_Cupon" ),
            "view_item" => __( "Ver evento pasado", "DP_Cupon" ),
            "view_items" => __( "Ver eventos pasados", "DP_Cupon" ),
            "search_items" => __( "Buscar evento pasado", "DP_Cupon" ),
            "not_found" => __( "Evento pasado no encontrado", "DP_Cupon" ),
            "not_found_in_trash" => __( "Evento pasado no encontrado", "DP_Cupon" ),
            "parent_item_colon" => __( "Evento pasado padre", "DP_Cupon" ),
            "featured_image" => __( "Thumb", "DP_Cupon" ),
            "set_featured_image" => __( "Thumb", "DP_Cupon" ),
            "remove_featured_image" => __( "Quitar thumb", "DP_Cupon" ),
            "use_featured_image" => __( "Usar como thumb", "DP_Cupon" ),
            "archives" => __( "Eventos pasados", "DP_Cupon" ),
            "insert_into_item" => __( "Insertar en evento pasado", "DP_Cupon" ),
            "uploaded_to_this_item" => __( "Subir al evento pasado", "DP_Cupon" ),
            "filter_items_list" => __( "Filtrar eventos pasados", "DP_Cupon" ),
            "items_list_navigation" => __( "Eventos pasados", "DP_Cupon" ),
            "items_list" => __( "Eventos pasados", "DP_Cupon" ),
            "attributes" => __( "Atributos de los eventos pasados", "DP_Cupon" ),
            "name_admin_bar" => __( "Evento pasado", "DP_Cupon" ),
            "parent_item_colon" => __( "Evento pasado padre", "DP_Cupon" ),
        );

        $args = array(
            "label" => __( "Archivos", "DP_Cupon" ),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "delete_with_user" => false,
            "show_in_rest" => true,
            "rest_base" => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive" => true,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array( "slug" => "archivos", "with_front" => true ),
            "query_var" => true,
            "menu_position" => 6,
            "menu_icon" => "dashicons-archive",
            "supports" => array( "title", "editor", "thumbnail" ),
            "taxonomies" => array( "tipo", 'localidad' ),
        );
        register_post_type( "archivos", $args );

        $labels = array(
            "name" => __( "Sliders", "DP_Cupon" ),
            "singular_name" => __( "Slider", "DP_Cupon" ),
            "menu_name" => __( "Sliders", "DP_Cupon" ),
            "all_items" => __( "Sliders", "DP_Cupon" ),
            "add_new" => __( "Nuevo slider", "DP_Cupon" ),
            "add_new_item" => __( "Nuevo slider", "DP_Cupon" ),
            "edit_item" => __( "Editar slider", "DP_Cupon" ),
            "new_item" => __( "Nuevo slider", "DP_Cupon" ),
            "view_item" => __( "Ver slider", "DP_Cupon" ),
            "view_items" => __( "Ver slider", "DP_Cupon" ),
            "search_items" => __( "Buscar slider", "DP_Cupon" ),
            "not_found" => __( "Slider no encontrado", "DP_Cupon" ),
            "not_found_in_trash" => __( "Slider no encontrado", "DP_Cupon" ),
            "parent_item_colon" => __( "Slider padre", "DP_Cupon" ),
            "featured_image" => __( "Imagen del slider", "DP_Cupon" ),
            "set_featured_image" => __( "Elegir como imagen del slider", "DP_Cupon" ),
            "remove_featured_image" => __( "Eliminar como imagen del slider", "DP_Cupon" ),
            "use_featured_image" => __( "Usar como imagen del slider", "DP_Cupon" ),
            "archives" => __( "Archivo de sliders", "DP_Cupon" ),
            "insert_into_item" => __( "Insertar en slider", "DP_Cupon" ),
            "uploaded_to_this_item" => __( "Subir al slider", "DP_Cupon" ),
            "filter_items_list" => __( "Filtrar sliders", "DP_Cupon" ),
            "items_list_navigation" => __( "Sliders", "DP_Cupon" ),
            "items_list" => __( "Sliders", "DP_Cupon" ),
            "attributes" => __( "Atributos del slider", "DP_Cupon" ),
            "name_admin_bar" => __( "Sliders", "DP_Cupon" ),
            "parent_item_colon" => __( "Slider padre", "DP_Cupon" ),
        );

        $args = array(
            "label" => __( "Sliders", "DP_Cupon" ),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "delete_with_user" => false,
            "show_in_rest" => true,
            "rest_base" => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive" => false,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array( "slug" => "sliders", "with_front" => true ),
            "query_var" => true,
            "menu_position" => 8,
            "menu_icon" => "dashicons-images-alt2",
            "supports" => array( "title" ),
        );
        register_post_type( "sliders", $args );

        $labels = array(
            "name" => __( "Merchants", "DP_Cupon" ),
            "singular_name" => __( "Merchant", "DP_Cupon" ),
            "menu_name" => __( "Merchants", "DP_Cupon" ),
            "all_items" => __( "Merchants", "DP_Cupon" ),
            "add_new" => __( "Nuevo merchant", "DP_Cupon" ),
            "add_new_item" => __( "Nuevo merchant", "DP_Cupon" ),
            "edit_item" => __( "Editar merchant", "DP_Cupon" ),
            "new_item" => __( "Nuevo merchant", "DP_Cupon" ),
            "view_item" => __( "Ver merchant", "DP_Cupon" ),
            "view_items" => __( "Ver merchant", "DP_Cupon" ),
            "search_items" => __( "Buscar merchant", "DP_Cupon" ),
            "not_found" => __( "Merchant no encontrado", "DP_Cupon" ),
            "not_found_in_trash" => __( "Merchant no encontrado", "DP_Cupon" ),
            "parent_item_colon" => __( "Merchant padre", "DP_Cupon" ),
            "featured_image" => __( "Imagen del merchant", "DP_Cupon" ),
            "set_featured_image" => __( "Elegir como imagen del merchant", "DP_Cupon" ),
            "remove_featured_image" => __( "Eliminar como imagen del merchant", "DP_Cupon" ),
            "use_featured_image" => __( "Usar como imagen del merchant", "DP_Cupon" ),
            "archives" => __( "Archivo de merchants", "DP_Cupon" ),
            "insert_into_item" => __( "Insertar en merchant", "DP_Cupon" ),
            "uploaded_to_this_item" => __( "Subir al merchant", "DP_Cupon" ),
            "filter_items_list" => __( "Filtrar merchants", "DP_Cupon" ),
            "items_list_navigation" => __( "Merchants", "DP_Cupon" ),
            "items_list" => __( "Merchants", "DP_Cupon" ),
            "attributes" => __( "Atributos del merchant", "DP_Cupon" ),
            "name_admin_bar" => __( "Merchants", "DP_Cupon" ),
            "parent_item_colon" => __( "Merchant padre", "DP_Cupon" ),
        );

        $args = array(
            "label" => __( "Merchants", "DP_Cupon" ),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "delete_with_user" => false,
            "show_in_rest" => true,
            "rest_base" => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive" => false,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "exclude_from_search" => false,
            'capabilities' => array(
                'edit_post' => 'edit_merchant',
                'edit_posts' => 'edit_merchants',
                'edit_others_posts' => 'edit_other_merchants',
                'publish_posts' => 'publish_merchants',
                'read_post' => 'read_merchant',
                'read_private_posts' => 'read_private_merchants',
                'delete_post' => 'delete_merchant'
            ),
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array( "slug" => "merchants", "with_front" => true ),
            "query_var" => true,
            "menu_position" => 8,
            "menu_icon" => "dashicons-universal-access",
            "supports" => array( "title" ),
        );
        register_post_type( "merchants", $args );
    }
    add_action( 'init', 'cptui_register_my_cpts' );

    function cptui_register_localidad() {
        $labels = array(
            "name" => __( "Municipio", "DP_Cupon" ),
            "singular_name" => __( "Municipio", "DP_Cupon" ),
            "menu_name" => __( "Municipios", "DP_Cupon" ),
            "all_items" => __( "Municipios", "DP_Cupon" ),
            "edit_item" => __( "Editar municipio", "DP_Cupon" ),
            "view_item" => __( "Ver municipio", "DP_Cupon" ),
            "update_item" => __( "Editar municipio", "DP_Cupon" ),
            "add_new_item" => __( "Agregar municipio", "DP_Cupon" ),
            "new_item_name" => __( "Nuevo municipio", "DP_Cupon" ),
            "parent_item" => __( "Municipio padre", "DP_Cupon" ),
            "parent_item_colon" => __( "Municipio padre", "DP_Cupon" ),
            "search_items" => __( "Buscar municipio", "DP_Cupon" ),
            "popular_items" => __( "Municipios populares", "DP_Cupon" ),
            "separate_items_with_commas" => __( "Separar municipios con comas", "DP_Cupon" ),
            "add_or_remove_items" => __( "Agregar o eliminar municipios", "DP_Cupon" ),
            "choose_from_most_used" => __( "Elegir entre los municipios más usados", "DP_Cupon" ),
            "not_found" => __( "Municipio no encontrado", "DP_Cupon" ),
            "no_terms" => __( "No hay municipios", "DP_Cupon" ),
            "items_list_navigation" => __( "Municipios", "DP_Cupon" ),
            "items_list" => __( "Municipios", "DP_Cupon" ),
        );

        $args = array(
            // "capabilities" => array(
            //     'manage_terms' => 'publish_posts',
            //     'edit_terms' => 'publish_posts',
            //     'delete_terms' => 'publish_posts',
            //     'assign_terms' => 'publish_posts'
            // ), 
            "label" => __( "Municipios", "DP_Cupon" ),
            "labels" => $labels,
            "public" => true,
            "publicly_queryable" => true,
            "hierarchical" => false,
            "show_ui" => true,
            // 'meta_box_cb' => 'drop_cat',
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "query_var" => true,
            "rewrite" => array( 'slug' => 'localidad', 'with_front' => true, ),
            "show_admin_column" => false,
            "show_in_rest" => false,
            "rest_base" => "localidad",
            "rest_controller_class" => "WP_REST_Terms_Controller",
            "show_in_quick_edit" => false,
            );
        register_taxonomy( "localidad", array( "beneficios", "eventos" ), $args );
    }
    add_action( 'init', 'cptui_register_localidad' );

    add_action( 'pre_insert_term', function ( $term, $taxonomy )
    {
        global $pagenow;
        return ( 'localidad' === $taxonomy && $pagenow == 'post.php' )
            ? new WP_Error( 'term_addition_blocked', __( 'No se pueden agregar nuevas localidades' ) )
            : $term;
    }, 0, 2 );

    function is_site_admin(){
        return in_array('administrator',  wp_get_current_user()->roles);
    }

    function drop_cat( $post, $box ) {
        $defaults = array('taxonomy' => 'category');
        if ( !isset($box['args']) || !is_array($box['args']) )
            $args = array();
        else
            $args = $box['args'];
        extract( wp_parse_args($args, $defaults), EXTR_SKIP );
        $tax = get_taxonomy($taxonomy);
        ?>
        <div id="taxonomy-<?php echo $taxonomy; ?>" class="acf-taxonomy-field categorydiv">

                <?php 
                $name = ( $taxonomy == 'category' ) ? 'post_category' : 'tax_input[' . $taxonomy . ']';
                echo "<input type='hidden' name='{$name}[]' value='0' />"; 
                ?>
                <?php $term_obj = wp_get_object_terms($post->ID, $taxonomy ); ?>
                <ul id="<?php echo $taxonomy; ?>checklist" data-wp-lists="list:<?php echo $taxonomy?>" class="categorychecklist form-no-clear">
                    <?php //wp_terms_checklist($post->ID, array( 'taxonomy' => $taxonomy) ) ?>
                </ul>

                <?php
                    $array = [];
                    foreach($term_obj as $term) {
                        $array[$term->term_id] = $term->name;
                    }
                ?>

                <?php wp_dropdown_categories( array( 'taxonomy' => $taxonomy, 'hide_empty' => 0, 'name' => "{$name}[]", 'selected' => $array, 'orderby' => 'name', 'hierarchical' => 0, 'show_option_none' => '&mdash;', 'multiple'          => true ) ); ?>

        </div>
        <?php
    }

    add_filter( 'wp_dropdown_cats', 'wp_dropdown_cats_multiple', 10, 2 );

    function wp_dropdown_cats_multiple( $output, $r ) {

        if( isset( $r['multiple'] ) && $r['multiple'] ) {

            $output = preg_replace( '/^<select/i', '<select multiple data-live-search="true" data-style="btn-info"', $output );

            $output = str_replace( "name='{$r['name']}'", "name='{$r['name']}[]'", $output );

            $selected = is_array($r['selected']) ? $r['selected'] : explode( ",", $r['selected'] );
            foreach ( array_map( 'trim', $selected ) as $value )
                $output = str_replace( "value=\"{$value}\"", "value=\"{$value}\" selected", $output );

        }

        return $output;
    }

    function custom_meta_box() {
        // remove_meta_box( 'tagsdiv-localidad', 'beneficios', 'side' );
        // add_meta_box( 'tagsdiv-types', 'Types', 'types_meta_box', 'sponsors', 'side' );
    }
    add_action('add_meta_boxes', 'custom_meta_box');

    function add_theme_caps() {
        $role = get_role( 'administrator' );
        $role->add_cap( 'edit_beneficio' );
        $role->add_cap( 'edit_beneficios' );
        $role->add_cap( 'edit_other_beneficios' );
        $role->add_cap( 'publish_beneficios' );
        $role->add_cap( 'read_beneficio' );
        $role->add_cap( 'read_private_beneficios' );
        $role->add_cap( 'delete_beneficio' );
        $role->add_cap( 'edit_merchant' ); 
        $role->add_cap( 'edit_merchants' ); 
        $role->add_cap( 'edit_other_merchants' ); 
        $role->add_cap( 'publish_merchants' ); 
        $role->add_cap( 'read_merchant' ); 
        $role->add_cap( 'read_private_merchants' ); 
        $role->add_cap( 'delete_merchant' );
        $role->add_cap('manage_reportes');

        $subscriber = get_role('subscriber');
        $subscriber->add_cap('manage_reportes');
    }
    add_action( 'admin_init', 'add_theme_caps');

    function custom_rewrite_tag() {
      add_rewrite_tag('%id%', '([^&]+)');
    }
    add_action('init', 'custom_rewrite_tag', 10, 0);

    function custom_rewrite_rule() {
        add_rewrite_rule('^cuponera/([^/]*)/?','index.php?page_id=347&id=$matches[1]','top');
        add_rewrite_rule('eventos/?$','index.php?category_name=eventos','top');
    }
    add_action('init', 'custom_rewrite_rule', 10, 0);

    add_action('acf/validate_save_post', 'my_acf_validate_save_post', 10, 0);
    function my_acf_validate_save_post() {
        if (!empty($_POST['acf'])) {
            foreach ($_POST['acf'] as $field_key => $value) {
                $field = acf_get_field($field_key);
                if (!$field) {
                    continue;
                }
                if($field['name'] == 'fecha') {
                    $start = $_POST['acf'][$field_key];
                    $start = new DateTime($start);
                }
                if($field['name'] == 'validez') {
                    $end = $_POST['acf'][$field_key];
                    $end = new DateTime($end);
                }
                if($field['name'] == 'sorteo') {
                    foreach($_POST['acf'][$field_key] as $k => $sub) {
                        $field = acf_get_field($k);
                        if($field['name'] == 'fecha_del_sorteo') {
                            $sorteo = $_POST['acf'][$field_key][$k];
                            $sorteo = new DateTime($sorteo);
                        }
                        if($field['name'] == 'fecha_limite') {
                            $limite = $_POST['acf'][$field_key][$k];
                            $limite = new DateTime($limite);
                        }
                    }
                }
            }
        }

        if(!$start || !$end)
            return;

        if($start > $end) {
            acf_reset_validation_errors();
            acf_add_validation_error('', 'El fin debe ser después del comienzo');
        }
        if($limite > $sorteo) {
            acf_reset_validation_errors();
            acf_add_validation_error('', 'El límite de inscripción debe ser antes de la fecha de sorteo');
        }
    }

    function formatoPrecio($precio) {
        $formateado = number_format($precio, 2, '.', ',');
        return preg_replace('/\.00*$/', '', $formateado);
    }

    function generate_dynamic_metadata( $post_meta = null, $post = null ) {
      if ( ! array_key_exists( 'merchant_slug', $post_meta ) ) {
        $post_meta['merchant_slug'] = 'endi';
      }
      return $post_meta;
    }
    add_filter( 'wpcfp_get_post_metadata', 'generate_dynamic_metadata', 1, 2 );

    // add_filter( 'register_post_type_args', 'wpd_change_post_type_args', 10, 2 );
    function wpd_change_post_type_args( $args, $post_type ){
        // flush_rewrite_rules( true );
        // if( 'post' == $post_type ){
        //     $args['rewrite']['with_front'] = false;
        // }
        return $args;
    }

    function add_rewrite_rules( $wp_rewrite )
    {
        $new_rules = array(
            'nota/(.+?)/(.+?)/?$' => 'index.php?post_type=post&name='. $wp_rewrite->preg_index(2),
        );

        $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
    }
    add_action('generate_rewrite_rules', 'add_rewrite_rules'); 

    function change_blog_links($post_link, $id=0){

        $post = get_post($id);

        if( is_object($post) && $post->post_type == 'post'){
            $categoria = 'indefinida';
            $categories = get_the_category($post->ID);
            if(!empty($categories)) {
                $slug = $categories[0]->slug;
                $categoria = $slug;
                $parent = $categories[0]->category_parent;
                if($parent > 0) {
                    $padre = get_category($parent);
                    $categoria= $padre->slug;
                }
            }
            return home_url('/nota/'.$categoria.'/'. $post->post_name.'/');
        }

        return $post_link;
    }
    add_filter('post_link', 'change_blog_links', 1, 3);

    function filter_featured_image_admin_text( $content, $post_id, $thumbnail_id ){
        $post_type = get_post_type($post_id);
        if($post_type == 'page')
            $help_text = '<p>' . __( 'Tamaño recomendado: 1920x507 Formato: sólo .jpg', 'club_endi' ) . '</p>';
        else if($post_type == 'beneficios' || $post_type == 'eventos')
            $help_text = '<p>' . __( 'Tamaño recomendado: 1210x440 Formato: sólo .jpg', 'club_endi' ) . '</p>';
        else if($post_type == 'post')
            $help_text = '<p>' . __( 'Tamaño recomendado: 460x400 Formatos recomendados: .jpg, .png', 'club_endi' ) . '</p>';
        else if($post_type == 'archivos') {
            $help_text = '<p>' . __( 'Tamaño recomendado: 750x750 Formatos recomendados: .jpg, .png', 'club_endi' ) . '</p>';
        }
        else
            $help_text = '';
        return $help_text . $content;
    }
    add_filter( 'admin_post_thumbnail_html', 'filter_featured_image_admin_text', 10, 3 );

    function another_rewrite_rule($rules) {
        $rules = array(
            '.*eventos/[^/]+/([^/]+).*$' => 'index.php?eventos=$matches[1]'
        );
        return $rules;
    }
    add_filter('eventos_rewrite_rules', 'another_rewrite_rule');


    function custom_rewrites_rules($rules) {
        $terminos = array(
            'depth'               => 1,
            'echo'                => false,
            'exclude'             => ['1', '47'],
            'hide_empty'          => 0,
            'order'               => 'ASC',
            'orderby'             => 'name',
            'parent'              => 0,
            'taxonomy'            => 'category',
            'title_li'            => '',
        );
        $categorias = get_terms($terminos);
        foreach($categorias as $categoria) {
            add_rewrite_rule('.*'.$categoria->slug.'$','index.php?category_name='.$categoria->slug,'top');
            add_rewrite_rule('.*'.$categoria->slug.'/[^/]+/[^/]+/[^/]+/([^/]+).*$','index.php?page_id=347&id=$matches[1]','top');
            add_rewrite_rule('.*'.$categoria->slug.'/[^/]+/[^/]+/([^/]+).*$', 'index.php?beneficios=$matches[1]', 'top');
        }
    }
    add_action( 'init', 'custom_rewrites_rules' );


    function custom_update_title($post_id, $post, $update) {
        if($post->post_type == 'beneficios' || $post->post_type == 'eventos') {
            $name = sanitize_title($post->post_title);
            if($post->post_name != $name) {
                remove_action('save_post', 'custom_update_title');
                wp_update_post(array('ID' => $post->ID, 'post_name' => $name));
                add_action('save_post', 'custom_update_title', 10, 3);
            }
        }
        return;
    }
    add_action('save_post', 'custom_update_title', 10, 3);


    function filter_post_type_permalink($permalink, $post, $leavename, $sample) {
        if($post->post_type == 'beneficios') {
            $merchant = get_field('merchant', $post->ID);
            $pieces = array(
                '%category%',
                '%merchant%',
                '%postname%',
            );
            $replacements = array(
                '',
                $merchant->post_name,
                $post->post_name,
            );
            $custom_set = implode('/', $pieces);

            if(strpos($custom_set, $pieces[0]) !== false) {
                $categories = get_the_category($post->ID);
                if(!empty($categories)) {
                    $slug = $categories[0]->slug;
                    $replacements[0] = $slug;
                    $parent = $categories[0]->category_parent;
                    if($parent > 0) {
                        $replacements[0] = get_category_parents($parent, false, '/', true) . $slug;
                    }
                }
            }
            $replaced = str_replace($pieces, $replacements, $custom_set);
            $replaced = preg_replace('/\/\/*/', '/', $replaced);

            $permalink = home_url($replaced);
            $permalink = user_trailingslashit($permalink, 'single');
        } else if($post->post_type == 'eventos') {
            $pieces = array(
                '%category%',
                '%postname%',
            );
            $replacements = array(
                '',
                $post->post_name,
            );
            $custom_set = implode('/', $pieces);

            if(strpos($custom_set, $pieces[0]) !== false) {
                $categories = get_the_category($post->ID);
                if(!empty($categories)) {
                    $slug = $categories[0]->slug;
                    $replacements[0] = $slug;
                    $parent = $categories[0]->category_parent;
                    if($parent > 0) {
                        $replacements[0] = get_category_parents($parent, false, '/', true) . $slug;
                    }
                }
            }
            $replaced = str_replace($pieces, $replacements, $custom_set);
            $replaced = preg_replace('/\/\/*/', '/', $replaced);

            $permalink = home_url($replaced);
            $permalink = user_trailingslashit($permalink, 'single');
        }
        return $permalink;
    }
    add_filter('post_type_link', 'filter_post_type_permalink', 10, 4);

    add_theme_support( 'post-thumbnails' );
    add_image_size( 'encabezado', 1210, 440, true );
    add_image_size( 'listas', 460, 400, true );
    add_image_size( 'thumb', 750, 750, true );
    add_image_size( 'thumb_mobile', 385, 380, true );
    add_image_size( 'slides', 1920, 440, true );

    add_filter( 'image_size_names_choose', 'wpshout_custom_sizes' );
    function wpshout_custom_sizes( $sizes ) {
        return array_merge( $sizes, array(
            'encabezado' => __( 'Encabezado Beneficio / Evento' ),
            'listas' => __( 'Listas Curadas' ),
            'thumb' => __( 'Evento Pasado' ),
            'thumb_mobile' => __( 'Thumb mobile' ),
            'slides' => __( 'Slides Home' ),
        ) );
    }
?>