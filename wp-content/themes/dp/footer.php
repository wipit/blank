			<?php wp_reset_query(); if(!(get_post_type() == 'page' && get_the_ID() == '347')) { ?>
				<div id="aliado">
					<div class="container d-flex justify-content-center">
						<a class="btn btn-primary" href="<?php echo get_template_directory_uri(); ?>/../../../se-aliado-del-club-endi">Convierte tu negocio en aliado del Club El Nuevo Día</a>
					</div>
				</div>
				<footer id="footer">
					<div class="container d-flex justify-content-around">
						<a href="https://www.elnuevodia.com/" target="_blank" class="m-0">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ElNuevoDia_white.png" />
						</a>
						<?php
							wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container_class' => 'footer-navigation row d-flex', 'menu_class' => 'd-flex justify-content-around align-items-center col-12 m-0' ) ); 
							if ( is_active_sidebar( 'redes' ) ) : ?>
								<ul id="sidebar" class="p-0">
									<?php dynamic_sidebar( 'redes' ); ?>
								</ul>
							<?php endif;
						?>
						<div class="footer-cr">
			                <a href="http://www.gfrmedia.com/" target="_blank">
			                    <img src="/club_elnuevodia_qa_thef/wp-content/themes/dp/images/logo-gfrmedia.png" style="float: left; width: 24px;">
			                </a>
			                <p>© Derechos Reservados 2019</p>
			            </div>
					</div>
				</footer>
			<?php } ?>
		</div>
		<?php wp_footer(); ?>
		<script type="text/javascript">
			jQuery.ajax({
				url: scripts.ajax_url, 
				method: 'POST',
				data: {
					action : 'accept_cookies'
				}
			});

			function aceptarCookie() {
				jQuery.ajax({
					url: scripts.ajax_url, 
					method: 'POST',
					data: {
						action : 'accept_cookies',
						accept : true
					},
					success: function(r){
						if(r == '1') {
							jQuery("#accept").remove();
						}
				    }
				});
			}
		</script>
	</body>
</html>