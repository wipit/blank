<?php get_header(); ?>
	<div class="container-fluid" id="cuerpo">
		<div class="row" id="archivo">
			<div class="container">
				<div class="row">
					<?php
						$x = 1;
						$videos = [];
						$cabecera = get_field('cabecera');
					?>
					<div id="video" class="col-12">
						<?php
							if($cabecera['tipo'] == 'video' && $cabecera['video']) { 
								$videos[] = $cabecera['video'];
						?>
								<div id="video_<?php echo $x; ?>" class="video"></div>
								<?php $x++; ?>
							<?php } else if($cabecera['tipo'] == 'imagen' && $cabecera['imagen']) { ?>
								<img src="<?php echo $cabecera['imagen']; ?>" />
							<?php } else if($cabecera['tipo'] == 'youtube' && $cabecera['youtube']) { ?>
								<div class="dummy"></div>
								<iframe class="youtube" width="560" height="315" src="https://www.youtube.com/embed/<?php echo $cabecera['youtube']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<?php } ?>
					</div>
					<div id="informacion" class="col-12">
						<div class="container">
							<div class="row">
								<div class="col-12 col-lg-4 pr-0">
									<?php
										echo '<div id="resumen">';
										echo '<h1>'.$post->post_title.'</h1>';
										$fecha = get_field('fecha');
										if($fecha) {
									?>
										<p><span class="celeste"><i class="fa fa-lg fa-calendar"></i></span> <?php echo $fecha; ?></p>
									<?php
										}
										$contenido_adicional = get_field('contenido_adicional');
										if($contenido_adicional) {
									?>
										<p id="descarga" v-cloak>
											<boton-archivo :url="'<?php echo $contenido_adicional['url']; ?>'" :ready="ready" :suscripto="suscripto" :logueado="logueado" :titulo="'<?php echo htmlentities($post->post_title); ?>'" inline-template>
												<a class="celeste" :href="url" target="_blank" v-if="suscripto && logueado && 0">
													<i class="fa fa-lg fa-download"></i> Descargar contenido adicional
												</a>
												<a class="celeste" href="javascript:;" @click="mostrarPopup(logueado)" v-else>
													<i class="fa fa-lg fa-download"></i> Descargar contenido adicional
												</a>
											</boton-archivo>
										</p>
									<?php
										}
										echo '</div>';
									?>
								</div>
								<div class="col-12 col-lg-8">
									<div id="texto">
										<?php
											$contenido = apply_filters('the_content', get_post_field('post_content', $post->ID));
											if($contenido)
												echo $contenido;
										?>
									</div>
								</div>
							</div>
							<div class="row" id="slider_archivo">
								<div class="col-12">
									<div class="slides">
										<?php
											$slider = get_field('slider');
											if($slider) {
												foreach($slider as $slide) {
													echo '<div class="slide">';
													if($slide['slide'] == 'video' && $slide['video']) {
														$videos[] = $slide['video'];
														echo '<div id="video_'.$x.'" class="video"></div><p class="pie">'.$slide['pie_de_imagen'].'</p>';
														$x++;
													} else if($slide['slide'] == 'imagen' && $slide['imagen']) {
														echo '<img class="w-100" src="'.$slide['imagen'].'" /><p class="pie">'.$slide['pie_de_imagen'].'</p>';
													} else if($slide['slide'] == 'youtube' && $slide['youtube']) { ?>
														<div class="dummy"></div>
														<iframe class="youtube" width="560" height="315" src="https://www.youtube.com/embed/<?php echo $slide['youtube']; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
														<p class="pie"><?php echo $slide['pie_de_imagen']; ?></p>
													<?php }
													echo "</div>";
												}
											}
										?>
									</div>
								</div>
							</div>
							<div class="row" id="ganadores">
								<div class="col-12">
									<?php
										$ganadores = get_field('ganadores');
										if($ganadores) {
											echo '<h3>Listado de ganadores</h3>';
											echo '<ul>';
											foreach($ganadores as $ganador) {
												echo '<li><span>'.$ganador['ganador'].'</span></li>';
											}
											echo '</ul>';
										}
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row">
					<?php require_once('includes/proximos.php'); ?>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
<?php
	if(count($videos > 0)) {
		$x = 1;
		foreach($videos as $video) { ?> 
			<script type="text/javascript">
				var videoPlayer = videoPlayer || [];
				videoPlayer['video_<?php echo $x; ?>'] = {
					videoID: <?php echo $video; ?>,
					videoAutoStart: false,
					videoResponsive: true,
					videoPublisher: 'elnuevodia',
					videoMinimizesOnFloat: true
				};
			</script>
		<?php $x++; } ?>
		<script type="text/javascript" src="https://assets.gfrvideo.com/assets/embed/embed-lib.js"></script>
	<?php }
?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/archivo.js?v7"></script>