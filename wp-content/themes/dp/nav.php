<div class="container">
  <nav class="navbar navbar-expand-lg p-0 pl-lg-3 pr-lg-3" role="navigation">
    <!-- menú desktop -->
    <div id="menu" class="row w-100 m-0">
      <?php
        wp_nav_menu( array(
          'theme_location'  => 'header',
          'depth'            => 1,
          'container'       => 'div',
          'container_class' => 'collapse navbar-collapse w-100',
          'container_id'    => 'categorias',
          'menu_class'      => 'navbar-nav w-100 d-flex justify-content-center',
          'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
          'walker'          => new WP_Bootstrap_Navwalker(),
          'items_wrap'      => my_nav_wrap()
        ) ); 
      ?>
    </div>
  </nav>
</div>
<!-- menú mobile -->
 <div id="wrapper">
    <div class="overlay"></div>
    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
      <div id="menu_head">
        <div class="d-flex justify-content-between align-items-center">
          <h4><span v-text="perfil.firstName ? '¡Hola, ' + perfil.firstName + '!' : ''"></span></h4>
          <button type="button" class="d-block d-lg-none btn btn-transparente" data-toggle="offcanvas">Cerrar</button>
        </div>
        <h6 v-if="logueado && suscripto"><a :href="base + '/cupones'">Tus cupones <span v-if="nuevas && nuevas != '0'" v-text="nuevas"></span></a></h6>
        <h6 v-if="logueado && suscripto"><a :href="base + '/mis-eventos'">Tus eventos</a></h6>
        <hr v-if="logueado" />
      </div>
      <div class="contenedor">
        <form :action="base + '/'" id="search2">
          <input type="text" name="s" />
          <img class="lupa" src="<?php echo get_stylesheet_directory_uri(); ?>/images/lupa.png" />
        </form>
        <div id="head_botones" class="d-flex justify-content-between align-items-center" v-if="!logueado || !suscripto">
          <a v-if="!suscripto" class="btn btn-primary gris" href="https://suscripciones.elnuevodia.com/?hpt=header_button" target="_blank">Suscríbete</a>
          <a class="btn btn-primary celeste" v-if="!logueado" href="#" onclick="gigya.accounts.showScreenSet({screenSet:'<?php echo SCREENSET_GIGYA; ?>', deviceType: 'auto', mobileScreenSet: '<?php echo SCREENSET_GIGYA; ?>', startScreen: 'gigya-login-screen'})">Login</a>
        </div>
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'header',
            'depth'            => 1,
            'container'       => 'div',
            'container_class' => '',
            'container_id'    => 'categorias',
            'menu_class'      => '',
            'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
            'walker'          => new WP_Bootstrap_Navwalker(),
            'items_wrap'      => my_nav_wrap()
          ) ); 
        ?>
        <div v-if="logueado">
          <a class="d-block" v-if="suscripto" href="https://micuenta.elnuevodia.com/suscripcion" target="_blank"><i class="fa fa-lg fa-user"></i>  Mi cuenta</a>
          <a class="d-block" v-if="!suscripto" href="https://micuenta.elnuevodia.com/perfil" target="_blank"><i class="fa fa-lg fa-user"></i>  Mi cuenta</a>
          <a class="d-block" href="#" @click="onLogout"><i class="fa fa-sign-out"></i> Cerrar sesión</a>
        </div>
      </div>
    </nav>
</div>