<?php 
	/* Template Name: Cupones */

	if(!isset($_COOKIE['logged'])) {
		header("Location: http://".$_SERVER['HTTP_HOST']);
		die();
	}
?>
	<?php get_header(); ?>
	<div id="ahorro" v-if="logueado && ahorro" v-cloak>
		<div class="container">
			<p><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/money.png" />¡Llevas <strong>$<span v-text="ahorro"></span></strong> ahorrados!</p>
		</div>
	</div>
	<div class="container" id="cuerpo">
		<div class="row">
			<div id="cupones" class="col-12" v-cloak>
				<div class="row" id="head">
					<div class="col-12 col-lg-6">
						<div class="container">
							<h3><?php the_title(); ?></h3>
							<p>Presiona el cupón que desees descargar:</p>
						</div>
					</div>
					<div class="col-12 col-lg-6">
						<selects-cupones v-on:filtro-categoria="filtroCategoria" v-on:filtro="filtro" v-on:filtrar="filtrar" v-on:cambiar-categoria="cambiarCategoria" v-on:orden="orden" v-on:ordenar="ordenar" :incluidas="categorias" inline-template>
							<div class="row">
								<div id="select-categorias" class="col-12 col-lg-6">
									<label>Ver</label>
									<div class="dropdown">
									  	<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-text="labelFiltro"></button>
									  	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									  		<div class="d-block d-md-none d-flex justify-content-between"><h2>Ver</h2><i class="fa fa-lg fa-times"></i></div>
										    <a class="dropdown-item" href="#" @click="determinarFiltro('cupones', '')" :class="[ filtro == 'cupones' ? 'is-checked' : '']">Todos los cupones <i class="fa fa-lg fa-check"></i></a>
										    <a class="dropdown-item" href="#" @click="determinarFiltro('eventos', '')" :class="[ filtro == 'eventos' ? 'is-checked' : '']">Todos los eventos <i class="fa fa-lg fa-check"></i></a>
										    <hr />
										    <a class="dropdown-item" href="#" @click="determinarFiltro('vencidos', '')" :class="[ filtro == 'vencidos' ? 'is-checked' : '']">Cupones vencidos <i class="fa fa-lg fa-check"></i></a>
										    <hr v-if="Object.keys(incluidas).length > 0" />
										    <div class="cats" v-if="Object.keys(incluidas).length > 0">
										    	<p>Por categoría:</p>
										    	<?php 
										    		$categorias = traerCategorias(); 
										    		foreach($categorias as $categoria) {
										    	?>
										    		<a class="dropdown-item" href="#" :class="[ filtro == 'categoria' && valor == '<?php echo $categoria['slug']; ?>' ? 'is-checked' : '']" @click="determinarFiltro('categoria', '<?php echo $categoria['slug']; ?>')" v-if='Object.keys(incluidas).includes("<?php echo $categoria['slug'] ?>")'><span><span class="color" style="background: <?php echo $categoria['color']; ?>"></span> <?php echo $categoria['nombre']; ?></span></span> <i class="fa fa-lg fa-check"></i></a>
										    	<?php } ?>
										    </div>
									  	</div>
									</div>
								</div>

								<div id="select-orden" class="col-12 col-lg-6">
									<label>Ordenar por</label>
									<div class="dropdown">
									  	<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-text="labelOrden"></button>
									  	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									  		<div class="d-block d-md-none d-flex justify-content-between"><h2>Ordenar por</h2><i class="fa fa-lg fa-times"></i></div>
										    <a class="dropdown-item" href="#" :class="['agregado'==orden? 'is-checked' : '']" @click="determinarOrden('agregado')">Más recientes <i class="fa fa-lg fa-check"></i></a>
										    <hr />
										    <a class="dropdown-item" href="#" :class="['vencimiento'==orden? 'is-checked' : '']" @click="determinarOrden('vencimiento')">Fecha de vencimiento <i class="fa fa-lg fa-check"></i></a>
									  	</div>
									</div>
								</div>
							</div>
						</selects-cupones>
					</div>
				</div>
				<div class="resultados_totales row">
					<div class="w-100" id="resultados">
						<isotope ref="cpt" :item-selector="'element-item'" :paginado="paginado.por_pagina" :list="final" :options='option' @filter="filterOption=arguments[0]" @sort="sortOption=arguments[0]" @layout="currentLayout=arguments[0]">
						    <div v-for="element in final" @click="selected=element" :key="element.ID" class="resultado col-12 col-lg-6" :class="element.vencido ? 'finalizado' : ''">
						    	<?php 
						    		if(0) {
						    			// href="javascript:;" :data-fancybox="!element.vencido" data-type="iframe" :data-src="element.url+element.cupon"
						    		}
						    	?>
						    	<a :href="element.url+element.cupon" target="_blank" class="mini d-none d-sm-inline-block alargado w-100">
									<div class="position-relative" :style="{'background-image': 'url('+element.imagen+')'}">
										<div v-if="element.vencido" class="oscuro"></div>
										<div class="dummy"></div>
										<div class="ubicar">
											<h6 v-text="element.titulo"></h6>
											<p v-if="element.exclusivo == 'si' && element.vencido === true">Disponible sólo para nuevos socios</p>
											<p v-else=""><i class="fa fa-lg fa-calendar"></i> Vence el {{element.vencimiento_formateado}}</p>
											<span class="d-block fondo" :style="{background: element.color}" style="color: white;" v-html="element.beneficio"></span>
										</div>
									</div>
								</a>
								<a :href="element.url+element.cupon" target="_blank" class="mini d-inline-block d-sm-none alargado w-100">
									<div class="position-relative" :style="{'background-image': 'url('+element.imagen+')'}">
										<div v-if="element.vencido" class="oscuro"></div>
										<div class="dummy"></div>
										<div class="ubicar">
											<h6 v-text="element.post_title"></h6>
											<p v-if="element.exclusivo == 'si' && element.vencido === true">Disponible sólo para nuevos socios</p>
											<p v-else=""><i class="fa fa-lg fa-calendar"></i> Vence el {{element.vencimiento_formateado}}</p>
											<span class="d-block fondo" :style="{background: element.color}" style="color: white;" v-html="element.beneficio"></span>
										</div>
									</div>
								</a>
						    </div>
						</isotope>
						<div v-if="!ready">
							loading
						</div>
						<div v-if="visibles < 1 && ready">
							<p class="no-resultados">Aún no tienes cupones para redimir. Te invitamos a añadir cupones de tu categoría favorita.</p>
						</div>
						<div id="paginado_cupones" class="d-flex justify-content-between" v-if="paginado.paginas > 1">
							<span @click="mover('-')" v-if="paginado.actual != 1"><i class="fa fa-lg fa-chevron-left"></i></span><span v-else></span>
							<!-- <span @click="mover(n)" v-for="n in paginado.paginas" v-bind:class="{ 'font-weight-bold': n == paginado.actual }">{{ n }}</span> -->
							<span>Página {{ paginado.actual }} de {{ paginado.paginas }}</span>
							<span @click="mover('+')" v-if="paginado.actual != paginado.paginas"><i class="fa fa-lg fa-chevron-right"></i></span><span v-else></span>
						</div>
					</div>
				</div>
	    	</div>
	    </div>
	</div>
<?php get_footer(); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/isotope/vue_isotope.js?v2"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/cupones.js?jazmin2019"></script>