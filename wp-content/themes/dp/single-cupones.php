<?php 
	/**
	 * Template Name: Cuponera
	 */
	global $wp_query;
	if(!isset($_COOKIE['logged']) || !$_COOKIE['logged'] || !$wp_query->query_vars['id'] || !isset($_COOKIE['user'])) {
		header("Location: ".get_site_url());
		die();
	} else {
		global $wpdb;
        $results = $wpdb->get_results('SELECT d.codigo, c.cupon_id, c.usuario_id, c.codigo_id FROM wp_cupones c LEFT JOIN wp_codigos d on c.codigo_id = d.id WHERE c.ID = "'.$wp_query->query_vars['id'].'" AND usuario_id = "'.$_COOKIE['id'].'"', OBJECT);
        if(!$results[0]) {
        	header("Location: ".get_site_url());
			die();
        }
	}
	get_header();
	$wp_query = new WP_Query(['p' => $results[0]->cupon_id, 'post_type' => 'beneficios']);
	if($wp_query->have_posts()) : 
        while ( $wp_query->have_posts() ) : $wp_query->the_post(); 
        	$term = get_the_category($post->ID);
			$subcategoria = get_cat_name($term[0]->term_id);
			$merchant = get_field('merchant', $post->ID);
?>
<div id="header" class="d-none"></div>
<div id="cuerpo" class="container-fluid cupones">
	<div class="row">
		<div id="beneficio" class="cuponera col-12">
			<div class="row">
				<div id="imprimible"class="col-12 imprimible">
					<div>
						<?php
							if (isset($_SERVER['HTTP_USER_AGENT'])) {
							    $agent = $_SERVER['HTTP_USER_AGENT'];
							}
							if (strlen(strstr($agent, 'Firefox')) > 0 && strlen(strstr($agent, 'Android')) > 0) {
							} else {
						?>
						<a id="flotante" data-subcategoria="<?php echo $subcategoria; ?>" data-merchant="<?php echo $merchant ? $merchant->post_title : ''; ?>" data-titulo="<?php echo htmlentities($post->post_title); ?>" href="#" onclick="javascript:window.print();" class="d-print-none"><i class="fa fa-lg fa-print"></i> <span>Oprime aquí para imprimir</span></a>
					<?php } if(0) { ?>
						<a id="cerrar" href="javascript:parent.jQuery.fancybox.close();" class="d-print-none d-none d-sm-block"><button type="button" data-toggle="offcanvas" class="hamburger is-open"><span class="hamb-top"></span> <span class="hamb-middle"></span> <span class="hamb-bottom"></span></button>
						</a>
					<?php } ?>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-12">
								<div id="recortable">
									<div id="cinta" class="d-md-flex justify-content-between">
										<?php 
											echo the_custom_logo(); 
											echo '<h6>'.$post->post_title.'</h6>';
										?>
									</div>
									<div id="imagen" style="background-image: url('<?php echo get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_field('thumbnail', $post->ID); ?>">
										<div class="dummy"></div>
										<img src="<?php echo get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID) : get_field('thumbnail', $post->ID); ?>" class="d-block d-sm-none" />
										<?php
											$term = get_the_category($post->ID);
									    	$categoria = $term[0] && $term[0]->parent == '0' ? $term[0]->term_id : $term[0]->parent;
								    		$term = get_term($categoria, 'category');
				    						$color = get_field('color', $term) ? get_field('color', $term) : '#4F97C7';
										?>
										<span class="d-block fondo" style="color: <?php echo $color; ?>;">
											<?php 
												echo get_descuento($post->ID, 'box');
											?>
											<div class="fix d-none"></div>
										</span>
									</div>
									<div id="personales" class="d-md-flex justify-content-between">
										<div id="data">
											<p><strong><?php echo $_COOKIE['user']->firstName && $_COOKIE['user']->lastName ? $_COOKIE['user']->firstName.' '.$_COOKIE['user']->lastName : ''; ?></strong></p>
											<?php
												$validez = get_field('validez', $post->ID);
												echo '<p>Válido hasta el '.strtolower(formatearFecha($validez, 'single')).'</p>'; 
											?>
										</div>
										<div id="codigo">
											<h2><?php echo $results[0]->codigo; ?></h2>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="informacion" class="col-12 imprimible">
					<div class="container">
						<div class="row padded-fix">
							<div class="col-4 order-print-2 order-1 boton-beneficio">
								<div class="d-none d-lg-block" id="boton-beneficio" v-cloak>
									<?php $validez = get_field('validez', $post->ID); ?>
								</div>
								<?php
									echo '<div id="resumen">';
									$local = get_field('local');
									if($local['nombre']) {
								?>
									<p><span class="celeste"><i class="fa fa-lg fa-map-marker"></i></span> <?php echo $local['nombre']; ?></p>
								<?php
									}
									if($local['telefono']) {
								?>
									<p class="telefono"><span class="celeste"><a style="color: inherit;" href="tel:<?php echo preg_replace( '/[^0-9]/', '', $local['telefono']); ?>"><i class="fa fa-lg fa-volume-control-phone"></i></span> <?php echo $local['telefono']; ?></a></p>
								<?php
									}
									if($local['website']) {
								?>
									<p><a class="web" href="<?php echo $local['website']; ?>" target="_blank"><?php echo $local['website']; ?></a></p>
								<?php
									}
									echo '</div>';
								?>
							</div>
							<div class="col-8 order-print-1 order-2">
								<div id="texto">
									<?php
										echo '<h4 class="d-none d-lg-block">'.$post->post_title.'</h4>';
										$contenido = apply_filters('the_content', get_post_field('post_content', $post->ID));
										if($contenido)
											echo $contenido.'<hr/>';
										$horarios = get_field('horarios', $post->ID);
										if($horarios)
											echo '<p><strong>Horario para redimir la oferta</strong></p>'.$horarios.'<hr/>';
										// if($validez)
										// 	echo '<p>Válido hasta el '.strtolower(formatearFecha($validez, 'single')).'</p><hr/>';
										$bases = get_field('bases', $post->ID);
										if($bases)
											echo '<p><strong>Letra pequeña</strong>'.$bases.'</p><hr/>';
										if($local['sucursales']) {
											echo '<h3>Localidades para redimir</h3>';
											echo '<div id="locales">';
											foreach($local['sucursales'] as $sucursal) {
								    			echo '<p class="mt-4"><strong>'.$sucursal['nombre_sucursal'].'</strong></p>';
								    			echo '<p>'.$sucursal['ubicacion']['address'].'</p>';
								    		} 
											echo '</div>';
										}
									?>
								</div>
							</div>
						</div>
						<div class="row padded-fix map">
							<?php
								if($local['sucursales']) {
							?>
								<div class="col-12">
									<div id="mapa" class="d-print-none">
										<div id="map"></div>
										<div class="dummy"></div>
									</div>
								</div>
							<?php
								}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
	include_once('includes/maps.php'); 
endwhile;
endif;
?>
<?php get_footer(); ?>