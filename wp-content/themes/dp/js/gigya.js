var app = new Vue({
    el: '#header',
    data: {
        ready: false,
        logueado: false,
        perfil: {},
        cupones: [],
        eventos: [],
        id: false,
        jwt: '',
        nuevas: 0,
        nuevo: true,
        ahorro: 0,
        suscripto: false,
        base: window.scripts.siteurl,
        es_eventos: window.location.href.indexOf('eventos') !== -1
    },
    mounted: function() {   
        this.GigyaLoadFunctions()
        let self = this
        gigya.socialize.addEventHandlers({
            onLogin: self.onLoginHandler, 
            onLogout: self.onLogoutHandler
        })
    },
    computed: {
        exclusivo: function() {
            return this.nuevo && !this.logueado
        }
    },
    methods: {
        chequear_status: function(res) {
            var form_data = new FormData
            form_data.append('action', 'chequear_status')
            // form_data.append('user', serialize(res.profile))
            // form_data.append('UID', res.UID)
            axios.post(this.base + '/wp-admin/admin-ajax.php', form_data)
            .then(function (response) {
                // console.log('--- CHEQUEAR STATUS ---')
                // console.log(response.data)
                app.logueado = response.data.logged
                app.perfil = response.data.user
                app.cupones = response.data.cupones
                app.eventos = response.data.eventos
                app.id = response.data.id
                app.suscripto = response.data.suscripto
                if(window.location.href.indexOf('/cupones') > 0) {
                    app.nuevas = 0
                } else {
                    app.nuevas = response.data.nuevas
                }
                // app.calcularAhorro()
            })
        },
        GigyaLoadFunctions: function() { 
            let self = this
            gigya.accounts.getAccountInfo({
                callback: function(res) {
                    // console.log('--- GIGYA RESPONSE ---')
                    // console.log(res)
                    if (res.errorCode === 0) {
                        app.traerData(res)
                    } else {
                        self.ready = true
                    }
                }
            })
        },
        agregar: function() {
            this.nuevas = parseInt(this.nuevas) + 1
        },
        vaciar: function() {
            this.nuevas = 0
        },
        cambiarSuscripto: function(valor) {
            this.suscripto = valor
        },
        enviarGTM: function(category) {
            var subseccion = jQuery('#gtm_subseccion').val()
              var merchant = jQuery('#gtm_merchant').val()
              var oferta = jQuery('#gtm_oferta').val()
              window.dataLayer.push({
                'event': 'botonApretado',
                'category': category,
                'action': subseccion,
                'label': merchant + oferta
              })
        },
        getAccountInfoHandler: function(r) {
            if(r.isActive && r.UID != 'FALSE') {
                app.perfil = r.profile
                app.perfil.UID = r.UID
                var form_data = new FormData;
                form_data.append('action', 'actualizar_datos')
                form_data.append('datos', JSON.stringify(app.perfil))
                axios.post(this.base + '/wp-admin/admin-ajax.php', form_data)
                .then(function (response) {})
            }
        },
        desplegarMenu: function() {
            if(!app.logueado) {
                this.enviarGTM('Ventana login')
                gigya.accounts.showScreenSet({screenSet:window.scripts.screenset_gigya, startScreen: 'gigya-login-screen', deviceType: 'auto', mobileScreenSet: window.scripts.screenset_gigya})
            } else {
                gigya.accounts.logout({callback: app.onLogout})
            }
        },
        desplegarMobile: function(){
            gigya.accounts.showScreenSet({screenSet:window.scripts.screenset_gigya, startScreen: 'gigya-login-screen', deviceType: 'auto', mobileScreenSet: window.scripts.screenset_gigya})
        },
        onLogout: function(response) {
            gigya.socialize.logout()
            if(window.location.href.indexOf('cupones') !== -1) {
                var url = window.location.href.replace('/cupones', '')
                setTimeout(function(){
                    window.location.href = url
                },100)  
            }
        },
        onLoginHandler: function(eventObj) {
            // console.log(eventObj)
            app.ready = false
            if(jQuery('#popup').length > 0)
                parent.jQuery.fancybox.close()
            app.traerData(eventObj)
        },
        traerData: function(objeto) {
            var self = this
            gigya.accounts.getJWT({callback: function(res){
                // console.log('--- GET JWT RESPONSE ---')
                // console.log(res)
                self.jwt = res.id_token
                let usuario = objeto.user ? objeto.user : objeto.profile
                let elUID = objeto.UID ? objeto.UID : objeto.user.UID
                var form_data = new FormData;
                form_data.append('action', 'chequear_existencia')
                form_data.append('uid', elUID)
                form_data.append('user', JSON.stringify(usuario))
                form_data.append('jwt', self.jwt)
                axios.post(app.base + '/wp-admin/admin-ajax.php', form_data)
                .then(function (response) {
                    if(response.data.nuevo !== true) {
                        if(window.patear)
                            window.location.replace(app.base)
                    } else {
                        jQuery('.exclusivo').css('display', 'block');
                    }
                    app.ready = true
                    if(response.data.respuesta == 'nuevo') {
                        var dateStr = Math.round(new Date().getTime()/1000.0);
                        var siteUID = response.data.id;
                        var yourSig = app.createSignature(siteUID, dateStr);
                        var params = {
                            siteUID: siteUID,
                            timestamp: dateStr,
                            cid: '',
                            signature: yourSig
                        };
                        gigya.socialize.notifyRegistration(params);
                    } else {
                        app.cupones = response.data.cupones
                        app.eventos = response.data.eventos
                    }
                    self.enviarGTM('Login exitoso')
                    app.logueado = true
                    app.suscripto = response.data.suscripto
                    app.nuevas = response.data.nuevas
                    app.nuevo = response.data.nuevo
                    app.perfil = usuario
                    app.perfil.UID = elUID
                    app.id = response.data.id
                    app.ready = true
                    app.calcularAhorro()
                })
            }})
        },
        onLogoutHandler: function(eventObj) {
            app.ready = false
            var form_data = new FormData;
            // form_data.append('action', 'destruir_sesion')
            form_data.append('action', 'matar_cookies')
            axios.post(this.base + '/wp-admin/admin-ajax.php', form_data)
            .then(function (response) {
                jQuery('.exclusivo').css('display', 'none')
                app.ready = true
                app.suscripto = false
                app.logueado = false
                app.perfil = {}
                app.cupones = []
                app.nuevas = 0
                app.id = false
            })
        },
        createSignature: function(UID, timestamp) {
            var encodedUID = encodeURIComponent(UID);
            return encodedUID;
        },
        calcularAhorro: function() {
            var form_data = new FormData;
            form_data.append('action', 'calcular_ahorro')
            form_data.append('id', this.id)
            axios.post(this.base + '/wp-admin/admin-ajax.php', form_data)
            .then(function (response) {
                if(response.data) {
                    app.ahorro = response.data
                }
            })
        }
    }
});