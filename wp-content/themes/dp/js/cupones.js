Vue.component("selects-cupones", {
    props: ["incluidas"],
    data: function() {
        return {
            orden: window.location.href.indexOf('cupones') !== -1 ? "agregado" : "vencimiento",
            filtro: window.location.href.indexOf('cupones') !== -1 ? 'cupones' : 'eventos',
            valor: null,
            categorias: []
        }
    },
    computed: {
        labelFiltro: function() {
            switch (this.filtro) {
                // case "":
                // case "todos":
                // case "show all":
                //     return "Todos";
                case "cupones":
                    return "Todos los cupones";
                case "eventos":
                    return "Todos los eventos";
                case "vencidos":
                    return "Cupones vencidos";
                default:
                    return this.categorias[this.valor]
            }
        },
        labelOrden: function() {
            if(this.orden == 'vencimiento') {
                let label = window.location.href.indexOf('cupones') !== -1 ? "Fecha de vencimiento" : "Fecha del sorteo"
                return label
            } else if(this.orden == 'fecha') {
                return "Fecha del evento"
            } else {
                return "Más recientes"
            }
        }
    },
    mounted: function() {
        var a = this,
            t = new FormData;
        t.append("action", "traer_categorias"), t.append("tipo", window.location.href.indexOf('cupones') !== -1 ? 'cupones' : 'eventos'), axios.post(app.base + "/wp-admin/admin-ajax.php", t).then(function(t) {
            a.categorias = t.data, 
            a.filtro = window.location.href.indexOf('cupones') !== -1 ? 'cupones' : 'eventos'
        }), this.borrar_nuevas()
    },
    watch: {
        filtro: function() {
            this.$emit("filtrar", this.filtro)
        },
        valor: function() {
            this.$emit("cambiar-categoria", this.valor)
        }
    },
    methods: {
        determinarFiltro: function(a, t) {
            this.filtro = a, this.valor = t
        },
        determinarOrden: function(a) {
            this.$emit("ordenar", a), this.orden = a
        },
        borrar_nuevas: function() {
            if(window.location.href.indexOf('eventos') !== -1)
                return
            var a = new FormData;
            a.append("action", "borrar_nuevas"), axios.post(app.base + "/wp-admin/admin-ajax.php", a)
        }
    }
});
var ahorro = new Vue({
        el: "#ahorro",
        computed: {
            ahorro: function() {
                return app.ahorro ? app.ahorro : 0
            },
            logueado: function() {
                return !!app.logueado && app.logueado
            }
        }
    }),
    count = 0,
    cupones = new Vue({
        el: "#cupones",
        data: {
            selected: null,
            sortOption: "agregado",
            filterOption: "show all",
            lista_ajax: [],
            categorias: [],
            categoria: "",
            cat: "",
            ready: !1,
            en_total: 0,
            filtrado: null,
            ordenado: window.location.href.indexOf('cupones') !== -1 ? 'agregado' : 'vencimiento',
            paginado: {
                por_pagina: 8,
                paginas: 1,
                actual: 1
            },
            option: {
                itemSelector: ".element-item",
                getFilterData: {
                    "show all": function(a) {
                        return !1 === a.vencido
                    },
                    categoria: function(a) {
                        return a.categoria == cupones.categoria
                    },
                    vencidos: function(a) {
                        return !0 === a.vencido
                    },
                    por_paginado: function(a) {
                        return a.paginado == cupones.paginado.actual
                    }
                },
                getSortData: {
                    vencimiento: "vencimiento",
                    agregado: "agregado",
                    fecha: "fecha"
                }
            }
        },
        methods: {
            scrollToTop: function() {
                const a = document.documentElement.scrollTop || document.body.scrollTop;
                a > 0 && (window.requestAnimationFrame(this.scrollToTop), window.scrollTo(0, a - a / 8))
            },
            sort: function(a) {
                var t = "agregado" != a;
                this.$refs.cpt.sort(a, t)
            },
            filter: function(a, t) {
                this.categoria = t, this.$refs.cpt.filter(a)
            },
            filtroCategoria: function(a) {
                this.filter("categoria", a)
            },
            filtro: function(a) {
                this.filter(a)
            },
            filtrar: function(a) {
                let url = window.location.href
                if(a == 'eventos' && url.indexOf('cupones') !== -1)
                    window.location.href = "/mis-eventos"
                if(a == 'cupones' && url.indexOf('eventos') !== -1)
                    window.location.href = "/cupones"
                this.filtrado = a
            },
            cambiarCategoria: function(a) {
                this.cat = a
            },
            orden: function(a) {},
            ordenar: function(a) {
                this.ordenado = a
            },
            mover: function(a) {
                this.scrollToTop(), this.paginado.actual = "number" == typeof a ? a : "-" == a ? this.paginado.actual - 1 : this.paginado.actual + 1
            },
            acomodar: function() {
                this.paginado.actual = 1
                if(this.ordenado == 'agregado') {
                    // this.lista_filtrada.sort((a,b) => (a.agregado > b.agregado) ? -1 : ((b.agregado > a.agregado) ? 1 : 0))
                    this.lista_filtrada.sort(function (a, b) {
                      return a.agregado > b.agregado ? -1 : b.agregado > a.agregado ? 1 : 0;
                    })
                } else if(this.ordenado == 'fecha') {
                    this.lista_filtrada.sort(function (a, b) {
                      return a.fecha > b.fecha ? -1 : b.fecha > a.fecha ? 1 : 0;
                    })
                } else {
                    // this.lista_filtrada.sort((a,b) => (a.vencimiento > b.vencimiento) ? 1 : ((b.vencimiento > a.vencimiento) ? -1 : 0))
                    this.lista_filtrada.sort(function (a, b) {
                      return a.vencimiento > b.vencimiento ? 1 : b.vencimiento > a.vencimiento ? -1 : 0;
                    })
                }
                this.paginar()
            },
            paginar: function() {
                var a = 1,
                    t = 1,
                    o = this;
                this.lista_filtrada.forEach(function(i) {
                    return i.paginado = t, o.paginado.por_pagina == a ? (a = 1, t++) : a++, i
                })
            }
        },
        watch: {
            filtrado: function() {
                this.acomodar()
            },
            filtrados: function() {
                this.filtrados != this.paginado.por_pagina && (this.en_total = this.filtrados, this.paginado.paginas = Math.ceil(this.en_total / this.paginado.por_pagina)), this.acomodar()
            },
            ordenado: function() {
                this.acomodar();
                var a = this.lista_ajax;
                this.lista_ajax = [];
                var t = this;
                setTimeout(function() {
                    t.lista_ajax = a, t.acomodar()
                }, 200)
            }
        },
        computed: {
            lista_filtrada: function() {
                return "categoria" == this.filtrado ? this.lista_ajax.filter(function(a) {
                    return a.categoria === cupones.cat
                }) : "vencidos" == this.filtrado ? this.lista_ajax.filter(function(a) {
                    return !0 === a.vencido
                }) : "eventos" == this.filtrado || "cupones" == this.filtrado ? this.lista_ajax.filter(function(a) {
                    return !1 === a.vencido
                }) : this.lista_ajax
            },
            final: function() {
                return this.lista_filtrada.filter(function(a) {
                    return a.paginado === cupones.paginado.actual
                })
            },
            filtrados: function() {
                return this.lista_filtrada ? this.lista_filtrada.length : 0
            },
            visibles: function() {
                return this.final ? this.final.length : 0
            },
            ahorro: function() {
                return app.ahorro
            },
            logueado: function() {
                return app.logueado
            }
        },
        mounted: function() {
            var a = new FormData;
            a.append("action", "traer_cupones"), a.append("tipo", window.location.href.indexOf('cupones') !== -1 ? 'cupones' : 'eventos'), axios.post(app.base + "/wp-admin/admin-ajax.php", a).then(function(a) {
                cupones.lista_ajax = a.data.lista_ajax, cupones.categorias = a.data.categorias, cupones.ready = !0
            })
        }
    });