Vue.component("boton-eventos", {
    data: function() {
        return {}
    },
    computed: {
        expirado: function() {
            return new Date(this.validez) < new Date(this.hoy)
        },
        canjeado: function() {
            return !!Object.keys(this.eventos).length && this.eventos.indexOf(String(this.post)) > -1
        }
    },
    props: ["user", "post", "validez", "hoy", "ready", "id", "logueado", "eventos", "suscripto", "url", "subcategoria", "titulo"],
    methods: {
        abrirLogin: function() {
            this.gtm("no logueado"), gigya.accounts.showScreenSet({
                screenSet: window.scripts.screenset_gigya,
                deviceType: "auto",
                mobileScreenSet: window.scripts.screenset_gigya,
                startScreen: "gigya-login-screen"
            })
        },
        gtm: function(t) {
            window.dataLayer.push({
                event: "botonApretado",
                category: "Botón Quiero Participar",
                action: this.subcategoria,
                label: this.titulo + " [ " + t + " ]"
            })
        },
        canjearEvento: function() {
        	let self = this
        	parent.jQuery.fancybox.defaults.baseClass = "centrado"
        	parent.jQuery.fancybox.open({
				src  : '#ninja',
				type : 'inline',
				opts : {
					afterShow : function( instance, current ) {
						jQuery(document).on( 'nfFormSubmitResponse', function() {
				            self.enviarEvento()
				        })
					},
					afterClose: function() {
						jQuery(document).off( 'nfFormSubmitResponse')
					}
				}
			})
        },
        enviarEvento: function() {
        	this.gtm("suscripto")
        	let t = this
            // window.open(t.url)
            let e = new FormData;
            e.append("action", "agregar_evento"), e.append("post", t.post), axios.post(app.base + "/wp-admin/admin-ajax.php", e).then(function(e) {
                e.data ? t.$emit("agregar", t.post) : alert("Hubo un error al agregar el cupón")
            })
        },
        mostrarPopup: function() {
            this.gtm("no suscripto"), parent.jQuery.fancybox.defaults.baseClass = "centrado", parent.jQuery.fancybox.open('<div id="popup2"><h2>Evento exclusivo para socios del Club El Nuevo Día.</h2><div><a class="btn btn-default" href="https://suscripciones.elnuevodia.com/?hpt=header_button" target="_blank">Suscríbete aquí</a></div></div>')
        }
    }
});
var participar = new Vue({
        el: "#boton1",
        data: {},
        computed: {
            ready: function() {
                return app.ready
            },
            logueado: function() {
                return app.logueado
            },
            perfil: function() {
                return app.perfil
            },
            eventos: function() {
                return app.eventos
            },
            id: function() {
                return app.id
            },
            suscripto: function() {
                return app.suscripto
            }
        },
        methods: {
            agregar: function(t) {
                this.eventos.push(t.toString())
            }
        }
    }),
    participar3 = new Vue({
        el: "#texto",
        data: {},
        computed: {
            ready: function() {
                return app.ready
            },
            logueado: function() {
                return app.logueado
            },
            perfil: function() {
                return app.perfil
            },
            eventos: function() {
                return app.eventos
            },
            id: function() {
                return app.id
            },
            suscripto: function() {
                return app.suscripto
            }
        },
        methods: {
            agregar: function(t) {
                this.eventos.push(t.toString())
            }
        }
    });