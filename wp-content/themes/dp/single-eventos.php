<?php get_header(); ?>
	<div class="container-fluid" id="cuerpo">
		<div class="row">
			<div id="evento" class="col-12">
				<div class="row">
					<div id="top" class="col-12 beneficio">
						<style type="text/css">
							#whatever {
								background-image: url('<?php echo get_the_post_thumbnail_url($post->ID) ? get_the_post_thumbnail_url($post->ID, 'encabezado') : get_field('thumbnail', $post->ID); ?>');
							}
							@media(max-width: 767px) {
								#whatever {
									background-image: url('<?php echo get_field('thumbnail_mobile', $post->ID) ? get_field('thumbnail_mobile', $post->ID, 'encabezado') : get_field('thumbnail', $post->ID); ?>');
								}
							}
						</style>
						<div id="whatever" class="slide d-block">
							<div class="dummy"></div>
							<a class="link" href="#">
							    <?php 
							    	$tipo = get_post_type($post->ID);
							    	$term = get_the_category($post->ID);
		    						$color = '#4F97C7';
						    		$beneficio = get_field('beneficio', $post->ID);
							    	$subcategoria = get_cat_name($term[0]->term_id);
						    		$local = get_field('ubicacion');
						    		$fecha = get_field('fecha');
						    		echo '<div class="only_mobile d-block d-lg-none"><h5 class="d-block d-lg-none">Evento</h5><h4>'.$post->post_title.'</h4><p>';
						    		echo $local['nombre'] ? '<span>'.$local['nombre'].'</span>' : '';
						    		echo $local['nombre'] && $fecha ? ' / ' : '';
						    		echo $fecha ? '<span>'.formatearFecha($fecha, 'mobile').'</span>' : '';
						    		echo '</p>';
						    		$limite = get_post_meta($post->ID, 'sorteo_fecha_limite', true) ? get_post_meta($post->ID, 'sorteo_fecha_limite', true) : '';
						    		$leyenda = get_post_meta($post->ID, 'sorteo_leyenda_sorteo', true) ? get_post_meta($post->ID, 'sorteo_leyenda_sorteo', true) : 'Puedes participar del sorteo hasta';
							    	if($limite)
							    		echo '<p class="limite">'.$leyenda.' '.formatearFecha($limite, 'chino').'</p>';
						    		echo '</div>';
							    	echo '<div class="info">';
							    	echo '<span class="fondo" style="background: '.$color.';"></span>';
							    	echo '<div class="wrapper d-flex flex-column justify-content-center"><div class="titular"><h5 class="d-none d-lg-block"><span class="opaco">Evento</span>';
							    	echo'</h5><div class="fix">'.$beneficio.'</div>';
							    	$referencia = get_field('precio_original', $post->ID);
							    	if($referencia) {
							    		// $numero = preg_replace( '/[^0-9]/', '', $beneficio);
							    		// echo '<p class="valorados">Valorados en <strong>$'.formatoPrecio($referencia / $numero).'</strong> c/u</p>';
							    		echo '<p class="valorados">Valorados en <strong>$'.formatoPrecio($referencia).'</strong></p>';
							    	}
							    	if($limite)
							    		echo '<p class="limite d-none d-lg-block">'.$leyenda.' '.formatearFecha($limite, 'chino').'</p>';
							    	echo '</div></div></div>';
							    	$pie = get_post_meta($post->ID, 'pie_de_imagen_texto_pie', true);
							    	if($pie)
							    		$color = get_post_meta($post->ID, 'pie_de_imagen_color_pie', true) ? get_post_meta($post->ID, 'pie_de_imagen_color_pie', true) : '#fff';
										echo '<p class="pie" style="color: '.$color.';">'.$pie.'</p>';
							    ?>
							</a>
						</div>
					</div>
					<div id="informacion" class="col-12">
						<div class="container">
							<div class="row padded-fix">
								<div class="col-12 col-lg-4 order-12 order-lg-1 boton-beneficio">
									<span class="d-none d-lg-block" id="boton1">
										<div v-cloak>
											<?php $validez = get_post_meta($post->ID, 'sorteo_fecha_limite', true); ?>
											<boton-eventos :url="'<?php echo get_field('url_gfr', $post->ID); ?>'" :post="<?php echo $post->ID; ?>" :validez="'<?php echo $validez; ?>'" :hoy="'<?php echo date('Y-m-d'); ?>'" :user="perfil" :id="id" :eventos="eventos" :ready="ready" :suscripto="suscripto" :logueado="logueado" :subcategoria="'<?php echo $subcategoria; ?>'" :titulo="'<?php echo str_replace("'", "", htmlentities($post->post_title)); ?>'" v-on:agregar="agregar" inline-template>
												<div v-if="ready">
													<button class='btn btn-primary' disabled='disabled' v-if="expirado">Finalizado</button>
													<span v-else>
														<button class="btn btn-primary" @click="abrirLogin" v-if="!logueado">Quiero participar</button>
														<span v-else>
															<button class='btn btn-primary' disabled='disabled' v-if="canjeado">Ya participaste</button>
															<span v-else>
																<button class="btn btn-primary" @click="canjearEvento" v-if="suscripto">Quiero participar</button>
																<button class="btn btn-primary" @click="mostrarPopup" v-else>Quiero participar</button>
															</span>
														</span>
													</span>
												</div>
											</boton-eventos>
										</div>
									</span>
									<?php
										echo '<div id="resumen">';
										if($local['nombre']) {
									?>
										<p><span class="celeste"><i class="fa fa-lg fa-home"></i></span> <span><?php echo $local['nombre']; ?></span><span class="clear"></span></p>
									<?php
										}
										if($local['ubicacion']) {
									?>
										<p><span class="celeste"><i class="fa fa-lg fa-map-marker"></i></span> <span><?php echo $local['ubicacion']['address']; ?></span><span class="clear"></span></p>
									<?php
										}
										$fecha = get_field('fecha');
										if($fecha) {
									?>
										<p><span class="celeste"><i class="fa fa-lg fa-calendar"></i></span> <span><?php echo formatearFecha($fecha, 'nombre'); ?></span><span class="clear"></span></p>
									<?php 
										$contenido_adicional = get_field('archivo');
										if($contenido_adicional) {
									?>
										<p class="sans" id="descarga" v-cloak>
											<boton-archivo :url="'<?php echo $contenido_adicional; ?>'" :ready="ready" :suscripto="suscripto" :logueado="logueado" :titulo="'<?php echo str_replace("'", "", htmlentities($post->post_title)); ?>'" inline-template>
												<a :href="url" target="_blank" v-if="suscripto && logueado && 0">
													<i class="fa fa-lg fa-download celeste"></i> Descargar contenido adicional
												</a>
												<a href="javascript:;" @click="mostrarPopup(logueado)" v-else>
													<i class="fa fa-lg fa-download celeste"></i> Descargar contenido adicional
												</a>
											</boton-archivo>
										</p>
									<?php
										}
									?>
									<?php
										}
										$anuncio = get_field('anuncio');
										if($anuncio) {
											echo '<div class="box">'.$anuncio.'</div>';
										}
										echo '</div>';
									?>
								</div>
								<div class="col-12 col-lg-8 order-1 order-lg-12">
									<div id="texto" v-cloak>
										<div class="d-block d-lg-none">
											<boton-eventos :url="'<?php echo get_field('url_gfr', $post->ID); ?>'" :post="<?php echo $post->ID; ?>" :validez="'<?php echo $validez; ?>'" :hoy="'<?php echo date('Y-m-d'); ?>'" :user="perfil" :id="id" :eventos="eventos" :ready="ready" :suscripto="suscripto" :logueado="logueado" :subcategoria="'<?php echo $subcategoria; ?>'" :titulo="'<?php echo str_replace("'", "", htmlentities($post->post_title)); ?>'" v-on:agregar="agregar" inline-template>
												<div v-if="ready">
													<button class='btn btn-primary' disabled='disabled' v-if="expirado">Finalizado</button>
													<span v-else>
														<button class="btn btn-primary" @click="abrirLogin" v-if="!logueado">Quiero participar</button>
														<span v-else>
															<button class='btn btn-primary' disabled='disabled' v-if="canjeado">Ya participaste</button>
															<span v-else>
																<button class="btn btn-primary" @click="canjearEvento" v-if="suscripto">Quiero participar</button>
																<button class="btn btn-primary" @click="mostrarPopup" v-else>Quiero participar</button>
															</span>
														</span>
													</span>
												</div>
											</boton-eventos>
										</div>
										<?php
											echo '<h4 class="d-none d-lg-block">'.$post->post_title.'</h4>';
											$contenido = apply_filters('the_content', get_post_field('post_content', $post->ID));
											if($contenido)
												echo $contenido;
											include_once('includes/share.php'); 
											include_once('includes/share_mobile.php');
											$bases = get_field('bases', $post->ID);
											if($bases)
												echo '<hr/><p><strong>Letra pequeña</strong></p>'.$bases;
										?>
									</div>
								</div>
							</div>
							<div class="row padded-fix map">
								<?php
									$locales = get_field('locales');
									if($locales && 0) {
								?>
									<div class="col-12">
										<h3>Localidades para redimir</h3>
										<div id="mapa">
											<div id="map"></div>
											<div class="dummy"></div>
										</div>
										<script type="text/javascript">
										    var locations = [
										    	<?php 
										    		$x = 1;
										    		foreach($locales as $sucursal) {
										    			echo "['".$sucursal['nombre']."', '".$sucursal['ubicacion']['address']."', ".$sucursal['ubicacion']['lat'].", ".$sucursal['ubicacion']['lng'].", ".$x."],";
										    			$x++;
										    		} 
										    	?>
										    ];

										    var map = new google.maps.Map(document.getElementById('map'), {
										      zoom: 11,
										      center: new google.maps.LatLng(18.45, -66.11),
										      mapTypeId: google.maps.MapTypeId.ROADMAP,
										      disableDefaultUI: true
										    });

										    var infowindow = new google.maps.InfoWindow();
										    var marker, i;
										    var bounds = new google.maps.LatLngBounds();
										    var icon = {
											    url: '<?php echo get_stylesheet_directory_uri(); ?>/images/marker@2x.png',
											    scaledSize: new google.maps.Size(24, 34)
											};

										    for (i = 0; i < locations.length; i++) {  
										    	const position = { lat: locations[i][2], lng: locations[i][3] }
										    	bounds.extend(position);
											    marker = new google.maps.Marker({
											        position: new google.maps.LatLng(locations[i][2], locations[i][3]),
											        map: map,
											        icon: icon
											    });

										      google.maps.event.addListener(marker, 'click', (function(marker, i) {
										        return function() {
										        	infowindow.setContent('<strong>'+locations[i][0] + '</strong><br/>' + locations[i][1]);
										        	infowindow.open(map, marker);
										        }
										      })(marker, i));
										    }
										    map.fitBounds(bounds);
										    var listener = google.maps.event.addListener(map, "idle", function() { 
											if (map.getZoom() > 14) map.setZoom(14); 
												google.maps.event.removeListener(listener); 
											});
										  </script>
									</div>
								<?php
									}
								?>
							</div>
							<div class="row">
								<div class="col-12">
									<?php
										//require('includes/anteriores.php');
									?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="ninja" style="display: none; min-width: 50%;">
			<?php 
				$ninja = get_field('shortcode_ninjaforms', $post->ID);
				if($ninja) {
					echo do_shortcode($ninja);
				}
			?>
		</div>
	</div>
<?php get_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/evento.js?v7"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/archivo.js?v7"></script>
<script type="text/javascript">
	var id = <?php echo $post->ID; ?>;
	var tipo = '<?php echo $tipo; ?>';
</script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/visitas.js"></script>