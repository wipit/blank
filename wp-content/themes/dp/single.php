<?php get_header(); ?>
	<div class="container-fluid" id="cuerpo">
		<div class="row">
			<div id="lista" class="col-12">
				<div class="row">
					<div id="top" class="col-12">
						<div class="slide d-block" style="background-image: url('<?php echo get_field('encabezado', $post->ID) ? get_field('encabezado', $post->ID) : get_the_post_thumbnail_url($post->ID); ?>">
							<div class="dummy"></div>
							<a class="link" href="#">
							    <?php 
							    	$tipo = get_post_type($post->ID);
							    	$term = get_the_category($post->ID);
							    	$categoria = $term[0] && $term[0]->parent == '0' ? $term[0]->term_id : $term[0]->parent;
						    		$term = get_term($categoria, 'category');
		    						$color = get_field('color', $term) ? get_field('color', $term) : '#4F97C7';
							    	echo '<div class="info">';
							    	echo '<span class="fondo" style="background: '.$color.';"></span>';
							    	echo '<div class="wrapper d-lg-flex flex-lg-column justify-content-lg-between"><div class="titular d-lg-flex flex-lg-column justify-content-lg-between"><h5><span class="opaco">';
							    	echo get_cat_name($categoria) ? get_cat_name($categoria) : ucfirst($tipo);
							    	echo '</span>';
							    	echo '</h5><h1>'.get_the_title().'</h1><p class="fecha"><i class="fa fa-lg fa-calendar"></i> <span>'.get_the_date("d.m.y").'</span></p></div>';
							    	echo '</div></div>';
							    	$pie = get_post_meta($post->ID, 'pie_de_imagen_texto_pie', true);
							    	if($pie)
							    		$color = get_post_meta($post->ID, 'pie_de_imagen_color_pie', true) ? get_post_meta($post->ID, 'pie_de_imagen_color_pie', true) : '#fff';
										echo '<p class="pie" style="color: '.$color.';">'.$pie.'</p>';
							    ?>
							</a>
						</div>
					</div>
					<div id="informacion" class="col-12">
						<div class="container">
							<div class="row">
								<div class="col-12 padded">
									<?php include_once('includes/share.php'); ?>
									<?php include_once('includes/share_mobile.php'); ?>
									<?php 
										$content = apply_filters('the_content', $post->post_content);
										echo $content;
										$contenido_adicional = get_field('contenido_adicional');
										if($contenido_adicional) {
									?>
										<p class="sans" id="descarga" v-cloak>
											<boton-archivo :url="'<?php echo $contenido_adicional['url']; ?>'" :ready="ready" :suscripto="suscripto" :logueado="logueado" :titulo="'<?php echo htmlentities($post->post_title); ?>'" inline-template>
												<a class="celeste" :href="url" target="_blank" v-if="suscripto && logueado && 0">
													<i class="fa fa-lg fa-download"></i> Descargar contenido adicional
												</a>
												<a class="celeste" href="javascript:;" @click="mostrarPopup(logueado)" v-else>
													<i class="fa fa-lg fa-download"></i> Descargar contenido adicional
												</a>
											</boton-archivo>
										</p>
									<?php
										}
									?>
									<div id="sugeridos">
										<?php 
											$sugeridos = get_field('recomendados', $post->ID);
											if($sugeridos) {
												foreach($sugeridos as $sugerido) {
													$id = $sugerido['beneficio']->ID;
													$validez = get_field('validez', $id);
													$tipo = get_post_type($id);
											        $date = $tipo == 'eventos' ? DateTime::CreateFromFormat('d/m/Y H:i a', $validez) : DateTime::CreateFromFormat('Y-m-d', $validez);
											        $dt = new DateTime();
											        $vencido = $date->format('Y-m-d H:i:s') >= $dt->format('Y-m-d H:i:s') ? '' : '<h1></h1>';
											        $clase = $date->format('Y-m-d H:i:s') >= $dt->format('Y-m-d H:i:s') ? '' : ' vencido';
													echo "<div class='sugerido".$clase."'><a class='mini' href='".get_permalink($id)."'>";
													$merchant = get_field('merchant', $id);
													$ademas = $merchant ? $merchant->post_title : '';
													$titulo = $ademas ? $ademas : $sugerido['beneficio']->post_title;
													echo "<h3>".$titulo."</h3>";
													$imagen = get_field('thumbnail', $id) ? get_field('thumbnail', $id) : get_the_post_thumbnail_url($id);
													if($imagen) {
														$categoria = get_post_type($id);
														if($categoria == 'beneficios') {
															$term = get_the_category($id);
															$categoria = $term[0] && $term[0]->parent == '0' ? $term[0]->term_id : $term[0]->parent;
															$term = get_term($categoria, 'category');
															$color = get_field('color', $term) ? get_field('color', $term) : '#001CB4';
															$beneficio = get_descuento($id, 'box');
															$beneficio = $date->format('Y-m-d H:i:s') >= $dt->format('Y-m-d H:i:s') ? $beneficio.'<i class="fa fa-lg fa-chevron-right"></i>' : '<p>Beneficio finalizado</p>';
														} else {
															$color = 'rgba(64, 83, 108, 0.9)';
															$beneficio = get_field('beneficio', $id);
															$beneficio = $date->format('Y-m-d H:i:s') >= $dt->format('Y-m-d H:i:s') ? $beneficio.'<i class="fa fa-lg fa-chevron-right"></i>' : '<p>Evento finalizado</p>';
														}
														echo "<figure style='background-image: url(".$imagen.");'><span class='d-block dummy'></span><span class='d-block descuento fondo' style='background: ".$color.";'>".$beneficio."</span>".$vencido."</figure>";
													}
													$bajada = $sugerido['bajada'];
													if($bajada)
														echo '<p>'.$bajada.'</p>';
													echo "</a></div>";
												}
											}
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/archivo.js?v7"></script>