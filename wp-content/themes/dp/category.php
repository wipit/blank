<?php 
  get_header();  
  global $elemento;
  global $cantidad;
  global $counter;
  global $tamaño;
  $tax = $wp_query->get_queried_object();
  $all_children = get_term_children($tax->term_id, 'category');
  $tamaño = 'chico';
  $cantidad = 0;
  $x = 0;
  $counter = 1;
  $args = array(
    'post_type'              => array( 'beneficios' ),
    'post_status'            => array( 'publish' ),
    'posts_per_page'         => '-1',
    'category__in' => $all_children, 
    'meta_query' => array(
      'relation' => 'AND',
      array(
        'relation' => 'AND',
        array(
          'validez' => array(
            'key' => 'validez',
            'compare' => '>=',
            'type' => 'DATE',
            'value' => date('Y-m-d')
          ),
        ),
        array(
          'prioridad' => array(
            'key' => 'prioridad',
            'compare' => 'EXISTS',
          ),
        ),
      ),
      array(
        'relation' => 'OR',
        array(
          'exclusivo' => array(
            'key' => 'exclusivo',
            'compare' => '!=',
            'value' => 'si'
          ),
        ),
        array(
          'exclusivo' => array(
            'key' => 'exclusivo',
            'compare' => 'NOT EXISTS',
          ),
        ),
      ),
    ),
    'orderby' => array( 
      'prioridad' => 'DESC',
      'validez' => 'ASC',
    ),
  );
  $query = new WP_Query( $args );
  $post_ids = wp_list_pluck( $query->posts, 'ID' );
?>
<div class="container" id="cuerpo">
  <div class="row" id="resultados">
    <div class="col-12">
      <div class="row">
        <div class="col-12 col-md-4">
          <button onclick="abrirFiltros();" class="btn btn-filtro d-block d-lg-none">Filtrar por</button>
          <?php 
            the_archive_title( '<h3 class="archive-title">', '</h3>' ); 
            the_archive_description(); 
          ?>
          <div class="seleccionados d-block d-md-none"></div>
          <div id="filtros">
            <div class="d-flex justify-content-between"><h5>Filtrar por:</h5><button type="button" class="hamburguesa d-block d-lg-none is-open" onclick="javascript:cerrarFiltros();"><span class="hamb-top"></span> <span class="hamb-middle"></span> <span class="hamb-bottom"></span></button></div>
            <div class="seleccionados d-none d-lg-block"></div>
            <?php 
              $term = get_category( get_query_var( 'cat' ) );
              $categoria = $term && $term->parent == '0' ? $term->term_id : $term->parent;
              $term = get_term($categoria, 'category');
              $color = get_field('color', $term) ? get_field('color', $term) : '#4F97C7';
            ?>
            <style type="text/css">
              .seleccionados .badge, [type=checkbox]:checked + span {
                background: <?php echo $color; ?>!important;
              }
              [type=checkbox]:checked + span {
                border-color: <?php echo $color; ?>!important;
              }
            </style>
            <div id="subcategorias">
              <div class="dropdown">
                <a class="d-flex justify-content-between w-100 collapsed" data-toggle="collapse" data-target="#collapseSubcategorias" aria-expanded="false" aria-controls="collapseSubcategorias" href="#">Subcategoría<i class="fa fa-lg fa-chevron-up"></i><i class="fa fa-lg fa-chevron-down"></i></a>
                <div class="collapse" id="collapseSubcategorias">
                  <?php
                    $sql_query = "SELECT distinct(tt.term_taxonomy_id) as term_id FROM wp_term_taxonomy tt INNER JOIN wp_term_relationships tr ON tt.term_taxonomy_id=tr.term_taxonomy_id INNER JOIN wp_posts wp ON tr.object_id=wp.ID INNER JOIN wp_postmeta wpp ON wpp.post_id=wp.ID WHERE tt.parent='".$tax->term_id."' AND wpp.post_id IN (".implode(',', $post_ids).")";
                    $results = $wpdb->get_results($sql_query, ARRAY_A);
                    $obj=[];
                    foreach($results as $k=>$v){  $obj[] = $v['term_id']; }
                    if($obj) {
                      $terms = get_terms( array(
                          // 'taxonomy' => 'category',
                          // 'hide_empty' => true,
                          // 'child_of' => $tax->term_id
                          'include' => $obj
                      ) );
                      foreach($terms as $term) {
                        if ($term->parent != 0) { 
                          echo '<label><input type="checkbox" value="'.$term->slug.'" data-name="'.$term->name.'" class="filter-item categorias" /><span></span> '.$term->name.'</label><br/>';
                        }
                      }
                    }
                  ?>
                </div>
              </div>
            </div>
            <div id="localidades">
              <div class="dropdown">
                <a class="d-flex justify-content-between w-100 collapsed" data-toggle="collapse" data-target="#collapseLocalidades" aria-expanded="false" aria-controls="collapseLocalidades" href="#">Localidad<i class="fa fa-lg fa-chevron-up"></i><i class="fa fa-lg fa-chevron-down"></i></a>
                <div class="collapse" id="collapseLocalidades">
                  <?php
                    $tax = $wp_query->get_queried_object();
                    $sql_query = 'SELECT DISTINCT(tt.term_taxonomy_id) as term_taxonomy_id FROM wp_term_taxonomy tt INNER JOIN wp_term_relationships tr ON tt.term_taxonomy_id=tr.term_taxonomy_id INNER JOIN wp_posts wp ON tr.object_id=wp.ID INNER JOIN wp_postmeta wpp ON wpp.post_id=wp.ID INNER JOIN wp_terms wpt ON wpt.term_id = tt.term_id WHERE tt.taxonomy="localidad" AND wpp.meta_key="validez" AND wpp.post_id IN ('.implode(",", $post_ids).') AND EXISTS(SELECT DISTINCT(wp1.id) as term_taxonomy_id FROM wp_term_taxonomy tt1  INNER JOIN wp_term_relationships tr1 ON tt1.term_taxonomy_id=tr1.term_taxonomy_id INNER JOIN wp_posts wp1 ON tr1.object_id=wp1.ID INNER JOIN wp_postmeta wpp1 ON wpp1.post_id=wp1.ID WHERE tt1.parent="'.$tax->term_id.'" AND wpp1.meta_key="validez" AND date(wpp1.meta_value) >= DATE(NOW()) AND wp1.ID=wp.ID)';
                    $results = $wpdb->get_results($sql_query, ARRAY_A);
                    $obj=[];
                    foreach($results as $k=>$v){  $obj[] = $v['term_taxonomy_id']; }
                    if($obj) {
                      $terms = get_terms( array(
                          'taxonomy' => 'localidad',
                          'hide_empty' => true,
                          'include' => $obj
                      ) );
                      foreach($terms as $term) {
                        echo '<label><input type="checkbox" value="'.$term->slug.'" data-name="'.$term->name.'" class="filter-item localidades" /><span></span> '.$term->name.'</label><br/>';
                      }
                    }
                  ?>
                </div>
              </div>
            </div>
            <button class="btn btn-primary d-block d-md-none" onclick="javascript:cerrarFiltros();">Filtrar</button>
          </div>
        </div>
        <div id="grilla" class="col-12 col-md-8">
          <div id="noResultsContainer">
            <p>Pendientes. Próximamente tendremos nuevos cupones.</p>
          </div>
          <div class="row padd-left">
            <div id="total" class="w-100">
              <?php 
                if ( $query->have_posts() ) {
                  while ( $query->have_posts() ) { 
                    $query->the_post(); 
                    $elemento = $post; 
                    $subcategorias = get_the_terms( $elemento->ID, 'category' );
                    $localidades = get_the_terms( $elemento->ID, 'localidad' );
                    ?>
                    <div class="resultado col-12 col-lg-6<?php if($localidades) foreach( $localidades as $localidad ) echo ' ' . $localidad->slug; ?><?php foreach( $subcategorias as $subcategoria ) echo ' ' . $subcategoria->slug; ?>">
                      <?php require('parts/box.php'); ?>
                    </div>
                  <?php }  
                } ?>
              </div>
          </div>
          <div id="paginado" class="d-flex justify-content-between"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>