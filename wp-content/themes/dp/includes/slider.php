<style type="text/css">
	#primero {
		background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/suscriptores_desktop.jpg');
	}
	@media(max-width: 767px) {
		#primero {
			background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/suscriptores_mobile.jpg');
		}
	}
</style>
<?php
	$args = array(
		'post_type'              => array( 'sliders' ),
		// 'name'               => 'Home',
		'posts_per_page'         => '1'
	);
	$query = new WP_Query( $args );
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$slides = get_field('slides');
			$array_slides = array_map('achatarArray', $slides);
			$slides = array_filter($array_slides, 'removerVencidos');
			if($slides) { ?>
				<ul class="slides p-0 m-0">
					<li class="slide d-block" id="primero">
						<div class="dummy"></div>
						<div class="link">
							<div class="info eventos">
							    <span class="fondo" style="background: #4F97C7;"></span>
							    <div class="wrapper d-flex flex-column justify-content-between">
							        <div class="titular">
							            <h5 class="animated"></h5>
							            <h5 class="animated" data-ademas="Slide Fijo Login" style="font-family: 'Roboto', sans-serif; font-weight: 300;">Suscriptor de El Nuevo Día, para disfrutar de las mejores ofertas y descuentos de Club El Nuevo Día, haz login.</h5></br><a class="d-none d-sm-inline-block" href="#" onclick="gigya.accounts.showScreenSet({screenSet:'<?php echo SCREENSET_GIGYA; ?>', deviceType: 'auto', mobileScreenSet: '<?php echo SCREENSET_GIGYA; ?>', startScreen: 'gigya-login-screen'})" class="animated" style="font-family: 'Roboto', sans-serif;">Login</a>
							        </div>
							        <div class="detalles">
							            <p class="animated">
							            </p>
							        </div>
							    </div>
							</div>
							<div class="d-flex d-sm-none" style="background: #4F97C7;"><a href="#" onclick="gigya.accounts.showScreenSet({screenSet:'<?php echo SCREENSET_GIGYA; ?>', deviceType: 'auto', mobileScreenSet: '<?php echo SCREENSET_GIGYA; ?>', startScreen: 'gigya-login-screen'})" style="font-family: 'Roboto', sans-serif;">Login</a>
							    <div class="espacio_dots" style="flex: 1;"></div>
							</div>
							<div class="volanta d-sm-none" style="color: #4F97C7;"><p>¡Comienza a disfrutar!</p></div>
						</div>
					</li>
				<?php foreach($slides as $slide) {
					$post = $slide;
					$tipo = get_post_type($post);
					$imagen = get_the_post_thumbnail_url($post) ? get_the_post_thumbnail_url($post, 'encabezado') : get_field('thumbnail', $post);
					$term = get_the_category($post);
			    	if(isset($term[0])) {
			    		$categoria = $term[0] && $term[0]->parent == '0' ? $term[0]->term_id : $term[0]->parent;
			    	} else {
			    		$categoria = '';
			    	}
					if($tipo == 'beneficios' || $tipo == 'eventos') {
						$validez = get_field('validez') ? get_field('validez') : get_field('fin');
						$ademas = 'No definido';
						$volanta = get_cat_name($categoria) ? get_cat_name($categoria) : ucfirst($tipo);
				    	if($tipo == 'beneficios') {
				    		$term = get_term($categoria, 'category');
    						$color = get_field('color', $term) ? get_field('color', $term) : '#4F97C7';
				    		$bajada = '<span><i class="fa fa-lg fa-calendar"></i> Válido hasta el '.strtolower(formatearFecha($validez, 'beneficio')).'</span>';
				    		$merchant = get_field('merchant');
				    		if($merchant) {
				    			$bajada .= '<span><i class="fa fa-lg fa-map-marker"></i> '.$merchant->post_title.'</span>';
				    			$ademas = $merchant->post_title;
				    		}
				    		$beneficio = get_descuento($post);
				    		// $ademas = $term[0] ? $term[0]->name : '';
				    	} else {
				    		$color = '#4F97C7';
				    		$fecha = get_field('fecha');
				    		$redencion = get_field('ubicacion');
				    		$bajada = '<span><i class="fa fa-lg fa-calendar"></i> '.strtolower(formatearFecha($fecha, 'evento')).'</span><span><i class="fa fa-lg fa-map-marker"></i> '.$redencion['nombre'].'</span>';
				    		$beneficio = get_field('beneficio');
				    		$term = get_the_category($post);
				    		$ademas = $term[0] ? $term[0]->name : '';
				    	}
				    	$link = get_permalink($post);
				    	$cta = 'Ver detalles';
				    	$full = '';
				    	$titulo = get_the_title($post);
					} else {
						$validez = '';
						$color = get_field('color');
						$volanta = '';
						$ademas = 'Slide';
						$bajada = get_field('bajada');
						$beneficio = '';
						$link = get_field('link_boton');
						$cta = get_field('texto_boton');
						$tipo = 'slides_';
						$full = ' full';
						$titulo = get_field('titulo');
					}
					if(1) {
				?>
					<li class="slide d-block<?php echo $full; ?>" style="background-image: url('<?php echo $imagen?>'); ">
						<div class="dummy"></div>
						<div class="link">
						    <?php 
						    	echo '<div class="info '.$tipo.'">';
						    	echo '<span class="fondo" style="background: '.$color.';"></span>';
						    	echo '<div class="wrapper d-flex flex-column justify-content-between"><div class="titular"><h5 class="animated">';
						    	echo $volanta;
						    	echo '</h5>';
						    	echo '<h4 class="animated" data-ademas="'.$ademas.'">'.$titulo.'</h4>';
						    	if($tipo == 'slides_') {
						    		echo '<h5 class="animated">'.$bajada.'</h5>';
						    	} 
						    	echo '<a class="d-none d-sm-inline-block" href="'.$link.'" class="animated">'.$cta.'</a></div>';
						    	if($tipo != 'slides_') {
						    		echo '<div class="detalles"><p class="animated">'.$bajada.'</p></div>';
						    	} 
						    	echo '</div></div>';
						    	echo '<div class="d-flex d-sm-none" style="background: '.$color.';"><a href="'.$link.'">'.$cta.'</a><div class="espacio_dots" style="flex: 1;"></div></div>';
						    	if($tipo == 'beneficios' || $tipo == 'eventos')
						    		echo '<div class="volanta" style="color: '.$color.';">'.$beneficio.'</div>';
						    	else
						    		echo '<div class="volanta d-block d-sm-none">&nbsp;</div>';
						   //  	$pie = get_post_meta($post->ID, 'pie_de_imagen_texto_pie', true);
						   //  	if($pie)
						   //  		$color = get_post_meta($post->ID, 'pie_de_imagen_color_pie', true) ? get_post_meta($post->ID, 'pie_de_imagen_color_pie', true) : '#fff';
									// echo '<p class="pie" style="color: '.$color.';">'.$pie.'</p>';
						    ?>
						</div>
					</li>
				<?php } } ?>
				</ul>
			<?php }
		}
	} 
	wp_reset_postdata();
?>