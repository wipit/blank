<div id="similares">
	<h3>Ofertas similares</h3>
	<?php
		$term = get_the_category($post->ID);
		$categoria = $term[0] && $term[0]->parent == '0' ? $term[0]->term_id : $term[0]->parent;
		
		$localidad = wp_get_post_terms($post->ID, 'localidad');
		$localidades = array_map("obtenerIDs", $localidad);

		$tipo = get_post_meta($post->ID, 'descuento_tipo_de_beneficio', true);
		// 1: misma categoría + mismo descuento
		$args1 = array(
			'post_type' => array( 'beneficios' ),
			'post_status' => array( 'publish' ),
			'posts_per_page' => '12',
			'post__not_in' => array($post->ID),
	 	 	'category__in' => $categoria, 
	        'meta_query' => array(
	        	'relation' => 'AND',
				'descuento' => array(
					'key' => 'descuento_tipo_de_beneficio',
					'value' => $tipo
				),
				'validez' => array(
					'key' => 'validez',
					'compare' => '>=',
					'type' => 'DATE',
					'value' => date('Y-m-d')
				)
	        ),
	        'orderby' => array( 
	          'validez' => 'ASC',
	        ),
		);
		$query1 = new WP_Query($args1);
		$post_ids = wp_list_pluck( $query1->posts, 'ID' );
		$post_ids[] = $post->ID;

		// 2. misma categoría + misma localidad
		$args2 = array(
			'post_type' => array( 'beneficios' ),
			'post_status' => array( 'publish' ),
			'posts_per_page' => '12',
			'post__not_in' => $post_ids,
	 	 	'tax_query' => array(
		        array (
		            'taxonomy' => 'localidad',
		            'field' => 'term_id',
		            'terms' => $localidades,
		        )
		    ),
	 	 	'category__in' => $categoria, 
	        'meta_query' => array(
	          'validez' => array(
	            'key' => 'validez',
	            'compare' => '>=',
	            'type' => 'DATE',
	            'value' => date('Y-m-d')
	          )
	        ),
	        'orderby' => array( 
	          'validez' => 'ASC',
	        ),
		);
		$query2 = new WP_Query($args2);
		$new_post_ids = wp_list_pluck($query2->posts, 'ID');
		$post_ids = array_merge($post_ids, $new_post_ids);

		// 3. misma categoria
		$args3 = array(
			'post_type' => array( 'beneficios' ),
			'post_status' => array( 'publish' ),
			'posts_per_page' => '12',
			'post__not_in' => $post_ids,
	 	 	'category__in' => $categoria, 
	        'meta_query' => array(
	          'validez' => array(
	            'key' => 'validez',
	            'compare' => '>=',
	            'type' => 'DATE',
	            'value' => date('Y-m-d')
	          )
	        ),
	        'orderby' => array( 
	          'validez' => 'ASC',
	        ),
		);
		$query3 = new WP_Query($args3);
		$new_post_ids = wp_list_pluck($query3->posts, 'ID');
		$post_ids = array_merge($post_ids, $new_post_ids);

		// 4. misma localidad
		$args4 = array(
			'post_type' => array( 'beneficios' ),
			'post_status' => array( 'publish' ),
			'posts_per_page' => '12',
			'post__not_in' => $post_ids,
	 	 	'tax_query' => array(
		        array (
		            'taxonomy' => 'localidad',
		            'field' => 'term_id',
		            'terms' => $localidades,
		        )
		    ),
	        'meta_query' => array(
	          'validez' => array(
	            'key' => 'validez',
	            'compare' => '>=',
	            'type' => 'DATE',
	            'value' => date('Y-m-d')
	          )
	        ),
	        'orderby' => array( 
	          'validez' => 'ASC',
	        ),
		);
		$query4 = new WP_Query($args4);
		$new_post_ids = wp_list_pluck($query4->posts, 'ID');
		$post_ids = array_merge($post_ids, $new_post_ids);

		// 5. beneficios de mismo descuento
		$args5 = array(
			'post_type' => array( 'beneficios' ),
			'post_status' => array( 'publish' ),
			'posts_per_page' => '12',
			'post__not_in' => $post_ids,
	 	 	'meta_query' => array(
	        	'relation' => 'AND',
				'descuento' => array(
					'key' => 'descuento_tipo_de_beneficio',
					'value' => $tipo
				),
				'validez' => array(
					'key' => 'validez',
					'compare' => '>=',
					'type' => 'DATE',
					'value' => date('Y-m-d')
				)
	        ),
	        'orderby' => array( 
	          'validez' => 'ASC',
	        ),
		);
		$query5 = new WP_Query($args5);
		$new_post_ids = wp_list_pluck($query5->posts, 'ID');
		$post_ids = array_merge($post_ids, $new_post_ids);

		$cantidad = 12;
		$x = 0;
		$tamaño = 'chico';
		$unido = array_slice(array_merge($query1->posts, $query2->posts, $query3->posts, $query4->posts, $query5->posts), 0, $cantidad);
		
		if ($unido) {
			echo '<div class="grilla row"><div id="slider_recomendados" class="w-100">';
			foreach ( $unido as $post ) {
				$elemento = $post;
				require(get_template_directory().'/parts/box.php');
			}
			echo '</div></div>';
		}
	?>
</div>