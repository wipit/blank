<div id="proximos" class="col-12">
	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1365 844">
	  <defs>
	    <style>
	      .cls-1 {
	        opacity: 0.267;
	        fill: url(#linear-gradient);
	      }
	    </style>
	    <linearGradient id="linear-gradient" x1="1.698" y1="1" x2="0" y2="0.797" gradientUnits="objectBoundingBox">
	      <stop offset="0" stop-color="#34aeff"/>
	      <stop offset="0.977" stop-color="#071824"/>
	      <stop offset="1" stop-color="#061621"/>
	    </linearGradient>
	  </defs>
	  <path id="Path_66" data-name="Path 66" class="cls-1" d="M-15,1365V0L844,1365Z" transform="translate(0 844) rotate(-90)"/>
	</svg>
	<div class="container">
		<div class="row_alt">
			<div class="w-100">
				<h3 class="d-flex justify-content-between align-items-center">Próximos eventos <a class="d-none d-md-block" href="<?php echo get_template_directory_uri(); ?>/../../../eventos/">Ver más <span><i class="fa fa-lg fa-chevron-right"></i></span></a></h3>
				<div class="grilla row">
					<?php
					// dd();
						// $args = array(
						// 	'post_type'              => array( 'eventos' ),
						// 	'post_status'            => array( 'published' ),
						// 	'posts_per_page'         => '5',
				  		//  'order'    => 'ASC',
					 	//  'orderby'  => 'fecha',
					 	//  'meta_key' => 'fecha',
					 	//  'meta_query'	=> array(
						// 		array(
						// 			'key'	 	=> 'fecha',
						// 			'value'		=> date('Y-m-d'),
						// 			'type'		=> 'date',
						// 			'compare'	=> '>='
						// 		),
						// 	),
						// );
						// $query = new WP_Query( $args );
						// global $elemento;
						// global $cantidad;
						// global $counter;
						// $x = 1;
						// $counter = 1;
						// if ( $query->have_posts() ) {
						// 	$cantidad = $query->post_count;
						// 	while ( $query->have_posts() ) {
						// 		$query->the_post(); 
						// 		$elemento = $post;
						// 		get_template_part( 'parts/grilla' );
						// 		$x++; 
						// 	}
						// } 
						// wp_reset_postdata();
						$home = get_page_by_title('Destacados home');
						$destacados = get_field('eventos', $home);
						global $elemento;
						global $cantidad;
						global $x;
						$x = 0;
						$flat_destacados = array_map("filtroVencidos", $destacados['destacados_eventos']);
						$array_destacados = array_filter($flat_destacados, 'removerVencidos');
						$cantidad = count($array_destacados);
						foreach($array_destacados as $elemento) {
							get_template_part( 'parts/cuadricula' );
						}
					?>
				</div>
				<div class="text-center d-md-none"><a href="<?php echo get_template_directory_uri(); ?>/../../../eventos">Ver más eventos <span><i class="fa fa-lg fa-chevron-right"></i></span></a></div>
			</div>
		</div>
	</div>
</div>