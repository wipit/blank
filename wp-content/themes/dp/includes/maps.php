<script type="text/javascript">
    var locations = [
    	<?php 
    		$x = 1;
    		foreach($local['sucursales'] as $sucursal) {
    			echo "['".addslashes($sucursal['nombre_sucursal'])."', '".addslashes($sucursal['ubicacion']['address'])."', ".$sucursal['ubicacion']['lat'].", ".$sucursal['ubicacion']['lng'].", '".$sucursal['telefono']."', ".$x."],";
    			$x++;
    		} 
    	?>
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 11,
      center: new google.maps.LatLng(18.45, -66.11),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      // disableDefaultUI: true,
      zoomControl: false,
      mapTypeControl: false,
      scaleControl: false,
      streetViewControl: false,
      rotateControl: false,
      fullscreenControl: true
    });

    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    var bounds = new google.maps.LatLngBounds();

    var icon = {
	    url: '<?php echo get_stylesheet_directory_uri(); ?>/images/marker@2x.png',
	    scaledSize: new google.maps.Size(24, 34)
	};

    for (i = 0; i < locations.length; i++) {  
    	const position = { lat: locations[i][2], lng: locations[i][3] }
    	bounds.extend(position);
	    marker = new google.maps.Marker({
	        position: new google.maps.LatLng(locations[i][2], locations[i][3]),
	        map: map,
	        icon: icon
	    });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
            var contenido = '<strong>'+locations[i][0] + '</strong><br/>' + locations[i][1];
            if(locations[i][4])
              contenido += '<br/><a href="tel:' + locations[i][4] + '">' + locations[i][4] + '</a>';
            if(locations[i][1])
              contenido += '<br/><a href="https://maps.google.com/maps?q='+locations[i][1]+'&z=16&t=m&hl=es-ES" target="_blank">Ampliar mapa</a>';
        	infowindow.setContent(contenido);
        	infowindow.open(map, marker);
        }
      })(marker, i));
    }
    map.fitBounds(bounds);
    var listener = google.maps.event.addListener(map, "idle", function() { 
  	  if (map.getZoom() > 14) map.setZoom(14); 
  		  google.maps.event.removeListener(listener); 
  	});
</script>