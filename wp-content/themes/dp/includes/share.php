<div class="share_bar d-none d-sm-flex">
	<h5>Compartir: </h5>
	<div id="desplegable">
		<div id="white">
			<li onclick="javascript:shareFacebook('<?php echo get_permalink(); ?>');"><i class="fa fa-lg fa-facebook"></i></li>
			<li onclick="javascript:shareTwitter('<?php echo get_permalink(); ?>');"><i class="fa fa-lg fa-twitter"></i></li>
			<li onclick="javascript:shareGigya();"><i class="fa fa-lg fa-envelope-o"></i></li>
			<li id="share_endi"></li>
		</div>
	</div>
</div>