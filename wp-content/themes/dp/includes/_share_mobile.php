<div id="shares" class="d-sm-none">
	<li onclick="javascript:shareFacebook('<?php echo get_permalink(); ?>');"><i class="fa fa-lg fa-facebook"></i></li>
	<li onclick="javascript:shareTwitter('<?php echo get_permalink(); ?>');"><i class="fa fa-lg fa-twitter"></i></li>
	<li onclick="javascript:shareGigya();"><i class="fa fa-lg fa-envelope-o"></i></li>
	<li onclick="javascript:shareWhatsapp();"><i class="fa fa-lg fa-whatsapp"></i></li>
</div>