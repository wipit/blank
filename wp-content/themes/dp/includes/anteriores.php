<?php global $elemento;
  global $cantidad;
  global $counter;
  global $tamaño;
  $tamaño = 'chico';
  $cantidad = 0;
  $x = 0;
  $counter = 1;
  $args = array(
    'post_type'              => array( 'archivos' ),
    'post_status'            => array( 'publish' ),
    'posts_per_page'         => '4',
    'meta_query' => array(
      'validez' => array(
        'key' => 'fecha',
        'compare' => '<=',
        'type' => 'DATE',
        'value' => date('Y-m-d')
      )
    ),
    'orderby' => array( 
      'fecha' => 'DESC',
    ),
  );
  $query = new WP_Query( $args );
  if ( $query->have_posts() ) { ?>
  <div id="similares">
  	<h3 class="d-md-flex justify-content-md-between align-items-md-center">Eventos anteriores <a class="d-none d-md-block" href="<?php echo get_template_directory_uri(); ?>/../../../archivos">Ver más<span><i class="fa fa-lg fa-chevron-right"></i></span></a></h3>
  	<?php 
            	echo '<div class="grilla row"><div id="slider_recomendados" class="w-100">';
              while ( $query->have_posts() ) { 
                	$query->the_post(); 	
  				$elemento = $post;
  				require(get_template_directory().'/parts/box.php');
  			}
  			echo '</div></div>';
  	?>
  <div class="text-center d-md-none"><a class="mt-0" href="<?php echo get_template_directory_uri(); ?>/../../../archivos">Ver más eventos <span><i class="fa fa-lg fa-chevron-right" style="margin-left: 14px;"></i></span></a></div>
  </div>
<?php } ?>