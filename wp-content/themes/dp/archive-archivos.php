<?php get_header();  ?>
<div class="container" id="cuerpo">
  	<div class="row" id="archivos">
  		<div class="col-12">
  			<h3>Eventos anteriores</h3>
  		</div>
		<?php
			global $elemento;
			global $cantidad;
			global $counter;
			global $tamaño;
			global $x;
			$tamaño = 'chico';
			$x = 0;
			$counter = 1;
			$args = array(
				'post_type'              => array( 'archivos' ),
				'post_status'            => array( 'publish' ),
				'posts_per_page'         => '-1',
				'meta_query' => array(
				  'validez' => array(
				    'key' => 'fecha',
				    'compare' => '<=',
				    'type' => 'DATE',
				    'value' => date('Y-m-d')
				  )
				),
				'orderby' => array( 
				  'fecha' => 'DESC',
				),
			);
			$query = new WP_Query( $args );
			$cantidad = $query->post_count;
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) { 
				  	$query->the_post(); 	
					$elemento = $post;
					get_template_part( 'parts/cuadricula' );
				}
			}
		?>
	</div>
</div>
<?php get_footer(); ?>