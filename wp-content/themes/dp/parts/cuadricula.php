<?php 
	global $elemento; 
	global $cantidad;
	global $x;
	$diapositivas = floor($cantidad / 5);
	$resto = $cantidad % 5;
	$posicion_maxima = $diapositivas * 5;
	$clase = $cantidad <= 5 ? 'es_simple' : 'es_multiple';
	if($x < $posicion_maxima) {
		switch ($x % 5) {
			case '0':
				$tamaño = "grande";
				break;
			default:
				$tamaño = "chico";
				break;
		}
	} else {
		switch ($resto) {
			case '1':
			case '2':
				$tamaño = "grande";
				break;
			case '3':
				$tamaño = "mediano";
				break;
			case '4':
				$tamaño = "chico";
				break;
		}
	}
	if($x == 0) 
		echo '<div class="col-12 flex justify-content-between w-100 slider_multiple '.$clase.'">';
	$validez = get_field('validez', $elemento->ID) ? get_field('validez', $elemento->ID) : get_field('fin', $elemento->ID);
	require('box.php');
	if($x + 1 == $cantidad) 
		echo '</div>';
	$x++;
?>