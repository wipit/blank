<?php 
	global $elemento; 
	global $cantidad;
	global $counter;
	switch ($cantidad) {
		case '1':
			$filas = 'simple';
			$tamaño = 'grande';
			break;
		case '2':
			$filas = 'simple';
			$tamaño = 'grande';
			break;
		case '3':
			$filas = 'simple';
			$tamaño = 'chico';
			break;
		case '4':
			$filas = 'simple';
			$tamaño = 'chico';
			break;
		case '5':
			$filas = 'd-block';
			$tamaño = 'chico';
			break;
		case '6':
			$filas = 'multiple';
			$tamaño = 'chico';
			break;
		default:
			$filas = 'simple';
			$tamaño = 'chico';
			break;
	}
	if($counter == 1) 
		echo '<div id="slider_beneficios" class="row flex justify-content-between w-100 '.$filas.'">';
	$validez = get_field('validez', $elemento->ID) ? get_field('validez', $elemento->ID) : get_field('fin', $elemento->ID);
	if($validez >= date('d-m-Y H:i:s'))
		require('box.php');
	if($counter == $cantidad) 
		echo '</div>';
	$counter++;
?>