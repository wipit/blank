<?php
	global $elemento;
	global $tamaño;
	global $cantidad;
	global $counter;
	if($counter < 3) 
		echo '<div class="col-6">';
	if($counter == 2)
		echo '<div class="row flex justify-content-between">';
	$categoria = get_post_type($elemento->ID);
	echo $tamaño == 'grande' ? '' : '<div class="half">';
	$imagen = get_field('thumbnail', $elemento->ID) ? get_field('thumbnail', $elemento->ID) : get_the_post_thumbnail_url($elemento->ID);
?>
	<a href="<?php echo get_permalink($elemento->ID); ?>" class="mini d-block <?php echo $tamaño; ?>" style="background-image: url('<?php echo $imagen; ?>');">
		<div class="dummy"></div>
		<h6><?php echo $elemento->post_title; ?></h6>
<?php 
	if($categoria == 'beneficios') {
		$color = '#001CB4';
		$beneficio = '<p><strong>'.get_post_meta($elemento->ID, 'descuento_porcentaje', true).'%</strong> Descuento</p>';
	} else {
		$color = '#4D35B0';
		$beneficio = get_field('beneficio', $elemento->ID);
		echo '<p class="fecha"><i class="fa fa-lg fa-calendar"></i> '.get_field('fecha', $elemento->ID).'</p>';
	}
	echo $tamaño == 'grande' ? '<span class="d-block" style="color: '.$color.'">'.$beneficio.'</span>' : '<span class="d-block" style="background: '.$color.'; color: white;">'.$beneficio.'</span>';
?>
	</a>
	</div>
<?php
	if($counter == $cantidad) 
		echo '</div></div>';
	$counter++;
?>