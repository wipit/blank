<?php
	global $elemento;
	global $cantidad;
	global $counter;
	switch ($cantidad) {
		case '1':
			$filas = 'simple';
			$tamaño = 'grande';
			break;
		case '2':
			$filas = 'simple';
			$tamaño = 'grande';
			break;
		case '3':
			$filas = 'simple';
			$tamaño = 'chico';
			break;
		case '4':
			$filas = 'simple';
			$tamaño = 'chico';
			break;
		case '5':
			$filas = 'd-block';
			$tamaño = 'chico';
			break;
		case '6':
			$filas = 'multiple';
			$tamaño = 'chico';
			break;
		default:
			$filas = 'simple';
			$tamaño = 'chico';
			break;
	}
	if($counter == 1) 
		echo '<div class="row flex justify-content-between w-100 '.$filas.'">';
	$categoria = get_post_type($elemento->ID);
	$imagen = get_field('thumbnail', $elemento->ID) ? get_field('thumbnail', $elemento->ID) : get_the_post_thumbnail_url($elemento->ID);
?>
	<a href="<?php echo get_permalink($elemento->ID); ?>" class="mini d-inline-block <?php echo $cantidad == 5 && $counter == 1 ? 'grande' : $tamaño; ?>">
		<div class="position-relative" style="background-image: url('<?php echo $imagen; ?>');">
			<div class="dummy"></div>
			<h6><?php echo $elemento->post_title; ?></h6>
<?php 
	if($categoria == 'beneficios') {
		$term = get_the_category($elemento->ID);
		$categoria = $term[0] && $term[0]->parent == '0' ? $term[0]->term_id : $term[0]->parent;
		$term = get_term($categoria, 'category');
		$color = get_field('color', $term) ? get_field('color', $term) : '#001CB4';
		$beneficio = '<p><strong>'.get_post_meta($elemento->ID, 'descuento_porcentaje', true).'%</strong> Descuento</p>';
	} else {
		$color = 'rgba(64, 83, 108, 0.9)';
		$beneficio = get_field('beneficio', $elemento->ID);
		echo '<p class="fecha"><i class="fa fa-lg fa-calendar"></i> '.get_field('fecha', $elemento->ID).'</p>';
	}
	if($tamaño == 'grande' || ($cantidad == 5 && $counter == 1))
		echo '<span class="d-block" style="color: '.$color.';">'.$beneficio.'</span>';
	else
		echo '<span class="d-block" style="background: '.$color.'; color: white;">'.$beneficio.'</span>';
?>
		</div>
	</a>
<?php
	if($counter == $cantidad) 
		echo '</div>';
	$counter++;
?>