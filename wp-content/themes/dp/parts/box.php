<?php
	$categoria = get_post_type($elemento->ID);
	$imagen = get_field('thumbnail', $elemento->ID) ? get_field('thumbnail', $elemento->ID) : get_the_post_thumbnail_url($elemento->ID, 'thumb');
	$counter = $x + 1;
	$parametro = isset($get) ? '?nuevo-socio' : '';
?>
<a href="<?php echo get_permalink($elemento->ID).$parametro; ?>" class="mini d-inline-block <?php echo $cantidad == 5 && $counter == 1 ? 'grande' : $tamaño; ?>">
	<div class="position-relative" style="background-image: url('<?php echo $imagen; ?>');">
		<div class="dummy"></div>
		<div class="ubicar">
<?php 
$clase = '';
if($categoria == 'beneficios') {
	$term = get_the_category($elemento->ID);
	$categoria = $term[0] && $term[0]->parent == '0' ? $term[0]->term_id : $term[0]->parent;
	$term = get_term($categoria, 'category');
	$color = get_field('color', $term) ? get_field('color', $term) : '#001CB4';
	$beneficio = get_descuento($elemento->ID, 'box');
	$merchant = get_field('merchant', $elemento->ID);
	$ademas = $merchant ? $merchant->post_title : '';
	$titulo = $ademas ? $ademas : $elemento->post_title;
	echo '<h6 data-ademas="'.$ademas.'">'.$titulo.'</h6>';
	$tipo = get_post_meta($elemento->ID, 'descuento_tipo_de_beneficio', true);
	if($tipo == 'regalo' || $tipo == 'promo')
		$clase = 'fixs';
} else {
	$color = '#4F97C7';
	$beneficio = get_field('beneficio', $elemento->ID);
	$fecha = explode(' ', get_field('fecha', $elemento->ID));
	$term = get_the_category($elemento->ID);
	$ademas = $term[0] ? $term[0]->name : '';
	$merchant = get_field('merchant', $elemento->ID);
	$merchante = $merchant ? $merchant->post_title : '';
	$titulo = $merchante ? $merchante : $elemento->post_title;
	echo '<h6 data-ademas="'.$ademas.'">'.$titulo.'</h6>';
	echo '<p class="fecha"><i class="fa fa-lg fa-calendar"></i> '.formatearFecha($fecha[0], "fix").'</p>';
}
if($tamaño == 'grande' || ($cantidad == 5 && $counter == 1)) {
	echo '<span class="d-block fondo '.$clase.'" style="color: '.$color.';">'.$beneficio.' <i class="fa fa-lg fa-chevron-right"></i></span>';
} else {
	$clase_responsive = "";
	if($counter == 2)
		$clase_responsive = "fix2";

	echo '<span class="d-block fondo '.$clase_responsive.' " style="background: '.$color.'; color: white;">'.$beneficio.' <i class="fa fa-lg fa-chevron-right"></i></span>';
}
?>
	</div></div>
</a>