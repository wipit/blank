<?php 
  /* Template Name: Beneficios Exclusivos */
  // echo '<h1>LOGUEADO : '.$_COOKIE['logged'].'</h1>';
  // echo '<h1>NUEVO : '.$_COOKIE['nuevo'].'</h1>';
  if(!isset($_COOKIE['logged']) || ($_COOKIE['logged'] == 1 && $_COOKIE['nuevo'] == 1 )) {
  // if(isset($_COOKIE['logged']) && ($_COOKIE['logged'] == 1 && $_COOKIE['nuevo'] == 1 )) {
    // echo 'suscriptor nuevo';
  } else {
    header("Location: http://".$_SERVER['HTTP_HOST']);
    die();
    // echo 'suscriptor viejo';
  }
  get_header();  
  echo '<script>patear = true;</script>';
  global $elemento;
  global $cantidad;
  global $counter;
  global $tamaño;
  $tamaño = 'chico';
  $cantidad = 0;
  $x = 0;
  $counter = 1;
  $args = array(
    'post_type'              => array( 'beneficios' ),
    'post_status'            => array( 'publish' ),
    'posts_per_page'         => '-1',
    'meta_query' => array(
      'relation' => 'AND',
      'exclusivo' => array(
        'key' => 'exclusivo',
        'compare' => '!=',
        'value' => 'no'
      ),
      'validez' => array(
        'key' => 'validez',
        'compare' => '>=',
        'type' => 'DATE',
        'value' => date('Y-m-d')
      )
    ),
    'orderby' => array( 
      'prioridad' => 'DESC',
      'validez' => 'ASC',
    ),
  );
  $query = new WP_Query( $args );
  $post_ids = wp_list_pluck( $query->posts, 'ID' );
?>
<div id="cuerpo">
  <div class="row">
    <div class="container-fluid">
      <div class="row">
        <div id="top" class="w-100">
          <?php 
            $header_desktop = get_field('header_desktop', $post->ID);
            $header_mobile = get_field('header_mobile', $post->ID) ? get_field('header_mobile', $post->ID) : $select_desktop;
          ?>
            <img class="w-100 d-none d-sm-block" style="object-fit: cover;" src="<?php echo $header_desktop; ?>" />
            <img class="w-100 d-block d-sm-none" style="object-fit: cover;" src="<?php echo $header_mobile; ?>" />
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row" id="resultados">
        <div class="col-12">
          <div class="row">
            <div class="col-12 col-md-4">
              <button onclick="abrirFiltros();" class="btn btn-filtro d-block d-lg-none">Filtrar por</button>
              <?php while ( have_posts() ) : the_post(); ?>
                <h3 class="archive-title"><?php the_title(); ?></h3>
                <?php the_content(); ?>
              <?php endwhile; ?>
              <div class="seleccionados d-block d-md-none"></div>
              <div id="filtros">
                <div class="d-flex justify-content-between"><h5>Filtrar por:</h5><button type="button" class="hamburguesa d-block d-lg-none is-open" onclick="javascript:cerrarFiltros();"><span class="hamb-top"></span> <span class="hamb-middle"></span> <span class="hamb-bottom"></span></button></div>
                <div class="seleccionados d-none d-lg-block"></div>
                <?php 
                  $color = '#009FFC';
                ?>
                <style type="text/css">
                  .seleccionados .badge, [type=checkbox]:checked + span,
                  body #cuerpo #beneficios .mini.chico span.fondo, body #cuerpo #proximos .mini.chico span.fondo, body #cuerpo #resultados .mini.chico span.fondo, body #cuerpo #beneficio .mini.chico span.fondo, body #cuerpo #lista .mini.chico span.fondo, body #cuerpo #archivo .mini.chico span.fondo, body #cuerpo #evento .mini.chico span.fondo, body #cuerpo #archivos .mini.chico span.fondo, body #cuerpo #sugeridos .mini.chico span.fondo {
                    background: <?php echo $color; ?>!important;
                  }
                  [type=checkbox]:checked + span {
                    border-color: <?php echo $color; ?>!important;
                  }
                </style>
                <div id="subcategorias">
                  <div class="dropdown">
                    <a class="d-flex justify-content-between w-100 collapsed" data-toggle="collapse" data-target="#collapseSubcategorias" aria-expanded="false" aria-controls="collapseSubcategorias" href="#">Subcategoría<i class="fa fa-lg fa-chevron-up"></i><i class="fa fa-lg fa-chevron-down"></i></a>
                    <div class="collapse" id="collapseSubcategorias">
                      <?php
                        $tax = $wp_query->get_queried_object();
                        $sql_query = "SELECT distinct(tt.term_taxonomy_id) as term_id FROM wp_term_taxonomy tt INNER JOIN wp_term_relationships tr ON tt.term_taxonomy_id=tr.term_taxonomy_id INNER JOIN wp_posts wp ON tr.object_id=wp.ID INNER JOIN wp_postmeta wpp ON wpp.post_id=wp.ID WHERE wpp.post_id IN (".implode(',', $post_ids).")";
                        $results = $wpdb->get_results($sql_query, ARRAY_A);
                        $obj=[];
                        foreach($results as $k=>$v){  $obj[] = $v['term_id']; }
                        if($obj) {
                          $terms = get_terms( array(
                              'include' => $obj
                          ) );
                          foreach($terms as $term) {
                            if ($term->parent != 0) { 
                              echo '<label><input type="checkbox" value="'.$term->slug.'" data-name="'.$term->name.'" class="filter-item categorias" /><span></span> '.$term->name.'</label><br/>';
                            }
                          }
                        }
                      ?>
                    </div>
                  </div>
                </div>
                <div id="localidades">
                  <div class="dropdown">
                    <a class="d-flex justify-content-between w-100 collapsed" data-toggle="collapse" data-target="#collapseLocalidades" aria-expanded="false" aria-controls="collapseLocalidades" href="#">Localidad<i class="fa fa-lg fa-chevron-up"></i><i class="fa fa-lg fa-chevron-down"></i></a>
                    <div class="collapse" id="collapseLocalidades">
                      <?php
                        $tax = $wp_query->get_queried_object();
                        $sql_query = 'SELECT DISTINCT(tt.term_taxonomy_id) as term_taxonomy_id FROM wp_term_taxonomy tt INNER JOIN wp_term_relationships tr ON tt.term_taxonomy_id=tr.term_taxonomy_id INNER JOIN wp_posts wp ON tr.object_id=wp.ID INNER JOIN wp_postmeta wpp ON wpp.post_id=wp.ID INNER JOIN wp_terms wpt ON wpt.term_id = tt.term_id WHERE tt.taxonomy="localidad" AND wpp.meta_key="validez" AND wpp.post_id IN ('.implode(",", $post_ids).') AND EXISTS(SELECT DISTINCT(wp1.id) as term_taxonomy_id FROM wp_term_taxonomy tt1  INNER JOIN wp_term_relationships tr1 ON tt1.term_taxonomy_id=tr1.term_taxonomy_id INNER JOIN wp_posts wp1 ON tr1.object_id=wp1.ID INNER JOIN wp_postmeta wpp1 ON wpp1.post_id=wp1.ID WHERE tt1.parent="'.$tax->term_id.'" AND wpp1.meta_key="validez" AND date(wpp1.meta_value) >= DATE(NOW()) AND wp1.ID=wp.ID)';
                        $results = $wpdb->get_results($sql_query, ARRAY_A);
                        $obj=[];
                        foreach($results as $k=>$v){  $obj[] = $v['term_taxonomy_id']; }
                        if($obj) {
                          $terms = get_terms( array(
                              'taxonomy' => 'localidad',
                              'hide_empty' => true,
                              'include' => $obj
                          ) );
                          foreach($terms as $term) {
                            echo '<label><input type="checkbox" value="'.$term->slug.'" data-name="'.$term->name.'" class="filter-item localidades" /><span></span> '.$term->name.'</label><br/>';
                          }
                        }
                      ?>
                    </div>
                  </div>
                </div>
                <button class="btn btn-primary d-block d-md-none" onclick="javascript:cerrarFiltros();">Filtrar</button>
              </div>
            </div>
            <div id="grilla" class="col-12 col-md-8">
              <div id="noResultsContainer">
                <p>Pendientes. Próximamente tendremos nuevos cupones.</p>
              </div>
              <div class="row padd-left">
                <div id="total" class="w-100">
                  <?php 
                    if ( $query->have_posts() ) {
                      while ( $query->have_posts() ) { 
                        $query->the_post(); 
                        $elemento = $post; 
                        $subcategorias = get_the_terms( $elemento->ID, 'category' );
                        $localidades = get_the_terms( $elemento->ID, 'localidad' );
                        $get = true;
                        ?>
                        <div class="resultado col-12 col-lg-6<?php if($localidades) foreach( $localidades as $localidad ) echo ' ' . $localidad->slug; ?><?php foreach( $subcategorias as $subcategoria ) echo ' ' . $subcategoria->slug; ?>">
                          <?php require('parts/box.php'); ?>
                        </div>
                      <?php }  
                    } ?>
                  </div>
              </div>
              <div id="paginado" class="d-flex justify-content-between"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>