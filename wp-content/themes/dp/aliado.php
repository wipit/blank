<?php 
	/* Template Name: Aliado */
?>
	<?php get_header(); ?>
	<div class="container-fluid" id="cuerpo">
		<div class="row">
			<div id="acerca" class="col-12 aliado">
				<div class="row">
					<div id="top" class="col-12">
						<?php
							$imagen = get_the_post_thumbnail_url() ? get_the_post_thumbnail_url() : get_stylesheet_directory_uri().'/images/aliado.png';
						?>
						<div class="slide d-block" style="background-image: url('<?php echo $imagen; ?>');">
							<div class="dummy"></div>

							<?php echo '<h4 class="d-md-none">'.get_the_title().'</h4>'; ?>
							
							<?php
								$pie = get_post_meta($post->ID, 'pie_de_imagen_texto_pie', true);
						    	if($pie)
						    		$color = get_post_meta($post->ID, 'pie_de_imagen_color_pie', true) ? get_post_meta($post->ID, 'pie_de_imagen_color_pie', true) : '#fff';
									echo '<p class="pie" style="color: '.$color.';">'.$pie.'</p>';
							?>
						</div>
					</div>
					<div id="informacion" class="col-12">
						<div class="container">
							<div class="row">
								<div id="menu" class="col-12 col-lg-4 order-12 order-lg-1">
									<?php
										$telefono = get_field('telefono', $post->ID);
						            	if($telefono)
						            		echo '<p>Teléfono</p><p class="bigger"><strong><a class="telefono" style="color: initial;" href="tel:'.$telefono.'">'.$telefono.'</a></strong></p>';
						            	$email = get_field('email', $post->ID);
						            	if($email && $telefono)
						            		echo '<hr />';
						            	if($email)
						            		echo '<p>Correo electrónico</p><p class="margined"><strong><a class="correo" style="color: initial;" href="mailto:'.$email.'">'.$email.'</a></strong></p>';
									?>
								</div>
								<div id="texto" class="col-12 col-lg-8 order-1 order-lg-12">
							        <?php
								        while ( have_posts() ) : the_post();
								            echo '<h1 class="d-none d-md-block">'.get_the_title().'</h1>';
								            the_content();
								        endwhile;
							        ?>
							    </div>
						    </div>
						</div>
					</div>
				</div>
				<div id="form" class="row">
			    	<div class="col-12">
			    		<div class="container">
			    			<div class="row">
			    				<div class="col-12">
			    					<h1 class="text-center">Déjanos tu mensaje para ser aliado del Club El Nuevo Día</h1>
			    					<?php echo do_shortcode('[contact-form-7 id="318" title="Contacto"]'); ?>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
	    	</div>
	    </div>
	</div>
<?php get_footer(); ?>