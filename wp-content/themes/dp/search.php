<?php 
	get_header();
	global $wp_query;
	$total_results = $wp_query->posts;
  function myfunction($v) {
    $validez = get_field('validez', $v->ID); 
    if(strpos($validez, '/')) {
      $fecha = date_create_from_format('d/m/Y g:i a', $validez);
      $validez = date_format($fecha, 'Y-m-d');
    }
    $v->validez = date('Y-m-d', strtotime($validez));
    return $v;
  }
  $total_results = array_map("myfunction", $total_results);
  usort($total_results, function($a, $b) {
    return strtotime($a->validez) - strtotime($b->validez);
  });
  $cat_ids = [];
  foreach($total_results as $resultado) {
    $temp_cat = get_the_category($resultado->ID);
    $temp_cat = $temp_cat[0];
    if($resultado->post_type == 'beneficios')
      $temp_cat = $temp_cat->category_parent;
    else
      $temp_cat = $temp_cat->term_id;
    if(!in_array($temp_cat, $cat_ids))
      array_push($cat_ids, $temp_cat);
  }
?>
<div class="container" id="cuerpo">
  <div class="row" id="resultados">
    <div class="col-12">
      <div class="row">
        <div class="col-12 col-md-4">
          <button onclick="abrirFiltros();" class="btn btn-filtro d-block d-md-none">Filtrar por</button>
          <h3><?php echo $_GET['s'] ?></h3>
          <p id="total_elementos"></p>
          <div class="seleccionados d-block d-md-none"></div>
          <div id="filtros">
            <div class="d-flex justify-content-between"><h5>Filtrar por:</h5><i class="fa fa-lg fa-times d-block d-md-none" onclick="javascript:cerrarFiltros();"></i></div>
            <div class="seleccionados d-none d-lg-block"></div>
            <style type="text/css">
              #seleccionados .badge, [type=checkbox]:checked + span {
                background: #4F97C7;
              }
              [type=checkbox]:checked + span {
                border-color: #4F97C7!important;
              }
            </style>
            <div id="subcategorias">
              <div class="dropdown">
                <a class="d-flex justify-content-between w-100 collapsed" data-toggle="collapse" data-target="#collapseSubcategorias" aria-expanded="false" aria-controls="collapseSubcategorias" href="#">Categoría<i class="fa fa-lg fa-chevron-up"></i><i class="fa fa-lg fa-chevron-down"></i></a>
                <div class="collapse" id="collapseSubcategorias">
                  <?php
                    // $post_ids = wp_list_pluck( $total_results, 'ID' );
                    $terms = traerDeTodo($cat_ids);
                    foreach($terms as $term) {
                     	echo '<label><input type="checkbox" value="'.$term['slug'].'" data-name="'.$term['nombre'].'" class="filter-item categorias" /><span></span> '.$term['nombre'].'</label><br/>';
                    }
                  ?>
                </div>
              </div>
            </div>
            <div id="localidades">
              <div class="dropdown">
                <a class="d-flex justify-content-between w-100 collapsed" data-toggle="collapse" data-target="#collapseLocalidades" aria-expanded="false" aria-controls="collapseLocalidades" href="#">Localidad<i class="fa fa-lg fa-chevron-up"></i><i class="fa fa-lg fa-chevron-down"></i></a>
                <div class="collapse" id="collapseLocalidades">
                  <?php
                    $tax = $wp_query->get_queried_object();
                    $terms = get_terms( array(
                        'taxonomy' => 'localidad',
                        'hide_empty' => true
                    ) );
                    foreach($terms as $term) {
                      echo '<label><input type="checkbox" value="'.$term->slug.'" data-name="'.$term->name.'" class="filter-item localidades" /><span></span> '.$term->name.'</label><br/>';
                    }
                  ?>
                </div>
              </div>
            </div>
            <button class="btn btn-primary d-block d-md-none" onclick="javascript:cerrarFiltros();">Filtrar</button>
          </div>
        </div>
        <div id="grilla" class="col-12 col-md-8">
          <div id="noResultsContainer">
            <p>Pendientes. Próximamente tendremos nuevos cupones.</p>
          </div>
          <div class="row padd-left">
            <div id="total" class="w-100">
              <?php 
              	global $elemento;
                global $cantidad;
                global $counter;
                global $tamaño;
                $tamaño = 'chico';
                $cantidad = 0;
                $x = 0;
                $counter = 1;
                foreach($total_results as $elemento) {
                    $subcategorias = get_the_terms( $elemento->ID, 'category' );
                    $localidades = get_the_terms( $elemento->ID, 'localidad' );
                    $clases_categorias = '';
                    if($subcategorias) { 
                      foreach( $subcategorias as $subcategoria ) {
                        $padre = ( isset( $subcategoria->parent ) ) ? get_term_by( 'id', $subcategoria->parent, 'category' ) : false;
                        $clases_categorias .= $padre && get_post_type($elemento->ID) == 'beneficios' ? ' '.$padre->slug : ' '.$subcategoria->slug;
                        // if($subcategoria)
                        //   $clases_categorias .= ' ' . $subcategoria->slug; 
                        // else
                        //   $clases_categorias .= ' padre'; 
                      }
                    }
                    if($elemento->validez >= date('Y-m-d')) {
                      ?>
                      <div class="resultado col-12 col-lg-6<?php if($localidades) { foreach( $localidades as $localidad ) echo ' ' . $localidad->slug; } ?><?php echo $clases_categorias; ?>">
                        <?php require('parts/box.php'); ?>
                      </div>
                    <?php 
                    }
                  $x++;
                } ?>
              </div>
          </div>
          <div id="paginado" class="d-flex justify-content-between"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>