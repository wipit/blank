<?php 
	/* Template Name: Acerca */
?>
	<?php get_header(); ?>
	<div class="container-fluid" id="cuerpo">
		<div class="row">
			<div id="acerca" class="col-12">
				<div class="row">
					<div id="top" class="col-12">
						<?php
							$imagen = get_the_post_thumbnail_url() ? get_the_post_thumbnail_url() : get_stylesheet_directory_uri().'/images/acerca.png';
						?>
						<div class="slide d-block" style="background-image: url('<?php echo $imagen; ?>');">
							<div class="dummy"></div>
							<?php echo '<h4 class="d-md-none">'.get_the_title().'</h4>'; ?>
							<?php
								$pie = get_post_meta($post->ID, 'pie_de_imagen_texto_pie', true);
						    	if($pie)
						    		$color = get_post_meta($post->ID, 'pie_de_imagen_color_pie', true) ? get_post_meta($post->ID, 'pie_de_imagen_color_pie', true) : '#fff';
									echo '<p class="pie" style="color: '.$color.';">'.$pie.'</p>';
							?>
						</div>
					</div>
					<div id="informacion" class="col-12">
						<div class="container">
							<div class="row">
								<div class="col-12 col-lg-4 order-12 order-lg-1">
									<div id="menu_lateral">
										<?php
											wp_nav_menu( array( 'theme_location' => 'acerca-menu', 'container_class' => 'acerca-navigation', 'menu_class' => 'col-12 p-0 m-0', 'before' => '<i class="fa fa-lg fa-chevron-right"></i>' ) ); 
										?>
									</div>
								</div>
								<div id="texto" class="col-12 col-lg-8 order-1 order-lg-12">
							        <?php
								        while ( have_posts() ) : the_post();
								            echo '<h4 class="d-none d-md-block">'.get_the_title().'</h4>';
								            the_content();
								            $pagename = get_query_var('pagename');  
								            if($pagename == 'contacto') {
								            	$contacto = get_field('contacto', $post->ID);
								            	if($contacto['telefono'])
								            		echo '<hr class="mb-0" /><p class="marginado">Teléfono</p><h6 class="bigger"><a href="tel:'.preg_replace("/[^0-9]/", "", $contacto['telefono']).'" class="telefono">'.$contacto['telefono'].'</a></h6>';
								            	if($contacto['e-mail'])
								            		echo '<hr class="mb-0" /><p class="marginado">Correo electrónico</p><h6><a class="correo" href="mailto:'.$contacto['e-mail'].'">'.$contacto['e-mail'].'</a></h6>';
								            	echo '<div class="espacio"></div>';
								            } else if($pagename == 'preguntas-frecuentes') {
								            	$preguntas = get_field('preguntas_frecuentes', $post->ID); 
								            	$x = 1;
								            	echo '<div id="accordion">';
								            	foreach($preguntas as $pregunta) { ?>
								            		<div class="pregunta_respuesta">
													    <div class="pregunta" id="heading_<?php echo $x; ?>">
													      <h5 class="mb-0">
													        <a href="#" class="<?php echo $x != 1 ? 'collapsed' : ''; ?> d-flex justify-content-between" data-toggle="collapse" data-target="#collapse_<?php echo $x; ?>" aria-expanded="true" aria-controls="collapse_<?php echo $x; ?>">
													          <?php echo $pregunta['pregunta']; ?><i class="fa fa-lg fa-chevron-up"></i><i class="fa fa-lg fa-chevron-down"></i>
													        </a>
													      </h5>
													    </div>
													    <div id="collapse_<?php echo $x; ?>" class="collapse <?php echo $x == 1 ? 'show' : ''; ?>" aria-labelledby="heading_<?php echo $x; ?>" data-parent="#accordion">
													      <div class="respuesta">
													        <?php echo str_replace('#FIN', '</a>', str_replace('#INICIO', '<a href="#" onclick="javascript:abrirLB();" style="text-decoration: underline;">', $pregunta['respuesta'])); ?>
													      </div>
													    </div>
													  </div>
								            	<?php $x++; }
								            	echo '</div>';
								            }
								        endwhile;
							        ?>
							    </div>
						    </div>
						</div>
					</div>
				</div>
	    	</div>
	    </div>
	</div>
<?php get_footer(); ?>
<?php
	$home = get_option( 'page_on_front' );
	$mp4 = get_field('video', $home) ? '<source src="'.get_field('video', $home).'" type="video/mp4" />' : '';
	$ogg = get_field('video_onboarding_ogg', $home) ? '<source src="'.get_field('video_onboarding_ogg', $home).'" type="video/ogg" />' : '';
	$webm = get_field('video_onboarding_webm', $home) ? '<source src="'.get_field('video_onboarding_webm', $home).'" type="video/webm" />' : '';
	if($mp4|| $ogg || $webm) {
?>
	<script type="text/javascript">
		function abrirLB(){
			jQuery.fancybox.defaults.baseClass = 'centrado';
			jQuery.fancybox.open('<div id="contenedor"><div class="dummy"></div><video id="video" width="560" height="320" preload="auto" autoplay="autoplay" class="video-js vjs-default-skin"><?php echo $mp4.$ogg.$webm; ?><div>Your browser does not support the HTML5 video tag.</div></video></div>', {
				    touch: false
				});
			jQuery(document).ready(function(){
				document.getElementById('video').addEventListener('ended',cerrarVideo,false);
				var promise = jQuery('video').prop('muted',true)[0].play();
				"use strict";
				if (promise !== undefined) {
				  promise.then(function (_) {}).catch(function (error) {
				    jQuery('#contenedor').prepend('<a href="#" id="play"><img src="wp-content/themes/dp/images/play.png" /></a>');
				  });
				}
			});
		}

		jQuery(document).on('click', '#play', function(){
			jQuery('video').prop('muted',true)[0].play();
			jQuery('#play').remove();
		});
	</script>
<?php
	}
?>