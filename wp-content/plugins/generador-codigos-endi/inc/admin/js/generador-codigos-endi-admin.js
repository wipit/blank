(function( $ ) {
	'use strict';
	$(function() {
		$(".btn.generar").click(function(e){
			e.preventDefault();
			let beneficio = $(this).attr("data-beneficio");
			let cantidad = $(this).parent().find(".generar").val();
			let usos = $(this).parent().find(".usos").val();
			$(this).parent().find(".btn").text('Generando...');
			$.ajax({
				url: ajax_generar.ajaxurl, 
				method: 'POST',
				data: {
					beneficio : beneficio,
					cantidad : cantidad,
					usos: usos
				},
				success: function(r){
					if(r == 'ok') {
						location.reload();
					} else {
						$(this).parent().find(".btn").text('ERROR');
					}
			    }
			});
		});
	});
})( jQuery );
