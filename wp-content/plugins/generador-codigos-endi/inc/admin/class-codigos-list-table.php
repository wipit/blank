<?php
namespace Generador_Codigos_ENDI\Inc\Admin;
use Generador_Codigos_ENDI\Inc\Libraries;

/**
 * @link       http://www.dinardi.com.ar
 * @since      1.0.0
 * @author     Jazmín Nasta
 */
class Codigos_List_Table extends Libraries\WP_List_Table  {

	protected $plugin_text_domain;
	protected $actual_link;
	
	public function __construct( $plugin_text_domain ) {
		
		$this->plugin_text_domain = $plugin_text_domain;

		$link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$partes = explode('?', $link);
		$this->actual_link = $partes[0].'?page='.$_GET['page'];

		parent::__construct( array( 
			'plural'	=>	'beneficios',
			'singular'	=>	'beneficio',
			'ajax'		=>	false,
		) );
	}

		public function display() {
		$singular = $this->_args['singular'];

		$this->display_tablenav( 'top' );

		$this->screen->render_screen_reader_content( 'heading_list' );
?>
<div style="text-align: right;"><a href="<?php echo get_template_directory_uri(); ?>/../../uploads/simple.csv" target="_blank">Bajar archivo de muestra</a></div>
<table class="wp-list-table <?php echo implode( ' ', $this->get_table_classes() ); ?>">
	<thead>
	<tr>
		<?php $this->print_column_headers(); ?>
	</tr>
	</thead>

	<tbody id="the-list"<?php
		if ( $singular ) {
			echo " data-wp-lists='list:$singular'";
		} ?>>
		<?php $this->display_rows_or_placeholder(); ?>
	</tbody>

	<tfoot>
	<tr>
		<?php $this->print_column_headers( false ); ?>
	</tr>
	</tfoot>

</table>
<?php
		$this->display_tablenav( 'bottom' );
	}
	
	public function prepare_items() {
		$user_search_key = isset( $_REQUEST['s'] ) ? wp_unslash( trim( $_REQUEST['s'] ) ) : '';
		
		$this->_column_headers = $this->get_column_info();
		
		$this->handle_table_actions();
		
		$table_data = $this->fetch_table_data();

		if( $user_search_key ) {
			$table_data = $this->filter_table_data( $table_data, $user_search_key );
		}		
		
		$codigos_per_page = $this->get_items_per_page( 'codigos_per_page' );
		$table_page = $this->get_pagenum();		
		
		$this->items = array_slice( $table_data, ( ( $table_page - 1 ) * $codigos_per_page ), $codigos_per_page );

		$total_codigos = count( $table_data );
		$this->set_pagination_args( array (
			'total_items' => $total_codigos,
			'per_page'    => $codigos_per_page,
			'total_pages' => ceil( $total_codigos/$codigos_per_page )
		) );
	}
	
	public function get_columns() {
		
		$table_columns = array(
			'cb'		=> '<input type="checkbox" />',
			// 'ID' => _x( 'ID', 'column name', $this->plugin_text_domain ),
			'post_title' => 'Beneficio',
			'cantidad' => 'Códigos disponibles',
			'canjeados' => 'Códigos canjeados',
			'generar' => 'Generar códigos',	
			'subir' => 'Subir códigos de CSV'	 		
		);
		return $table_columns;
	}

	protected function column_cb( $item ) {
		return sprintf(		
		'<label class="screen-reader-text" for="beneficio_' . $item['ID'] . '">' . sprintf( __( 'Seleccionar %s' ), $item['post_title'] ) . '</label>'
		. "<input type='checkbox' name='beneficios[]' id='beneficio_{$item['ID']}' value='{$item['ID']}' />"					
		);
	}
	
	protected function get_sortable_columns() {
		$sortable_columns = array (
			'ID' => array( 'ID', true ),
			'post_title'=>'post_title'
		);
		return $sortable_columns;
	}	
	
	public function no_items() {
		_e( 'No hay códigos.', $this->plugin_text_domain );
	}	
	
	public function fetch_table_data() {
		global $wpdb;
		$orderby = ( isset( $_GET['orderby'] ) ) ? esc_sql( $_GET['orderby'] ) : 'post_title';
		$order = ( isset( $_GET['order'] ) ) ? esc_sql( $_GET['order'] ) : 'ASC';
		
		$user_query = "SELECT p.ID, p.post_title, SUM(c.usos) as cantidad, SUM(c.canjeados) as canjeados FROM wp_posts p LEFT JOIN wp_codigos c ON p.ID = c.beneficio_id WHERE p.post_status = 'publish' AND p.post_type = 'beneficios' GROUP BY p.ID ORDER BY $orderby $order";

		$query_results = $wpdb->get_results( $user_query, ARRAY_A  );
		return $query_results;		
	}
	
	public function filter_table_data( $table_data, $search_key ) {
		$filtered_table_data = array_values( array_filter( $table_data, function( $row ) use( $search_key ) {
			foreach( $row as $row_val ) {
				if( stripos( $row_val, $search_key ) !== false ) {
					return true;
				}				
			}			
		} ) );
		return $filtered_table_data;
	}
	
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {			
			case 'post_title':
			case 'ID':
				return $item[$column_name];
			default:
			  return $item[$column_name];
		}
	}
	
	protected function column_generar( $item ) {
		return sprintf(		
			"<div class='form'><div class='form-group'><label for='generar'>Cantidad</label><input class='generar form-control' type='number' min='1' name='generar[]' id='codigo_{$item['ID']}' value='' /></div><div class='form-group'><label for='usos'>Usos</label><input class='usos form-control' type='number' min='1' name='usos[]' id='uso_{$item['ID']}' value='' /></div><button data-beneficio='".$item['ID']."' class='btn generar'>Generar ahora</button></div>"					
		);
	}

	protected function column_subir( $item ) {
		return sprintf(		
			"<form></form><div class='form'><form action='admin-post.php' method='post' enctype='multipart/form-data'><input type='hidden' name='back' value='".$this->actual_link."' /><input type='hidden' name='action' value='subir' /><input type='hidden' name='beneficio' value='".$item['ID']."' /><input type='file' name='subir' accept='.csv' /><input type='submit' value='Subir' /></form></div>"			
		);
	}
	
	public function get_bulk_actions() {
		$actions = array(
			'bulk-download' => 'Bajar listado de códigos'
		);

		 return $actions;
	}

	public function handle_table_actions() {
		if ( ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] === 'bulk-download' ) || ( isset( $_REQUEST['action2'] ) && $_REQUEST['action2'] === 'bulk-download' ) ) {	
			$this->page_bulk_download($_REQUEST['beneficios']);
			$this->graceful_exit();
		}
		if ( ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] === 'subir' ) || ( isset( $_REQUEST['action2'] ) && $_REQUEST['action2'] === 'subir' ) ) {
			$this->graceful_exit();
		}
	}
	
	public function page_bulk_download( $bulk_codigos_ids ) {		
		require_once( 'views/partials-wp-list-table-demo-bulk-download.php' );
	}  

	public function graceful_exit() {
		exit;
	}
	  	 
	public function invalid_nonce_redirect() {
		wp_die( __( 'Invalid Nonce', $this->plugin_text_domain ),
				__( 'Error', $this->plugin_text_domain ),
				array( 
						'response' 	=> 403, 
						'back_link' =>  esc_url( add_query_arg( array( 'page' => wp_unslash( $_REQUEST['page'] ) ) , admin_url( 'admin.php?page=generador-codigos-endi' ) ) ),
					)
		);
	 }
}