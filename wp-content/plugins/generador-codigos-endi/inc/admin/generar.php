<?php
	define( 'BLOCK_LOAD', true );
	$WP_PATH = str_replace('/wp-content/plugins', '', implode("/", (explode("/", $_SERVER["PHP_SELF"], -4))));
	require_once( $_SERVER['DOCUMENT_ROOT'] . $WP_PATH . '/wp-config.php' );
	require_once( $_SERVER['DOCUMENT_ROOT'] . $WP_PATH . '/wp-includes/wp-db.php' );
	$wpdb = new wpdb(DB_USER, DB_PASSWORD, DB_NAME, DB_HOST);

	$values = '';
	$length = 4;
	for($i = 0; $i < $_POST['cantidad']; $i++) {
		$random = $_POST['beneficio'].substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, $length).$i;
		$values .= "('".$_POST['beneficio']."', '".$random."', '".$_POST['usos']."', 0),";
	}
	$values = rtrim($values, ',');
	$query = "INSERT INTO wp_codigos (beneficio_id, codigo, usos, canjeados) VALUES ".$values;
	$result = $wpdb->get_results($query);
	echo 'ok';
?>