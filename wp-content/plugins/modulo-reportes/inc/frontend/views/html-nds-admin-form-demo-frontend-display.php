<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.dinardi.com.ar
 * @since      1.0.0
 *
 * @package    Modulo_Reportes_ENDI
 * @subpackage Modulo_Reportes_ENDI/inc/frontend/views
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
