<div class="wrap">
    <h2><?php _e( 'Reportes por Beneficios', $this->plugin_text_domain); ?></h2>
        <div id="modulo-reportes-endi">			
            <div id="nds-post-body">		
				<form id="nds-user-list-form" method="get">
					<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
					<?php 
						$this->Codigos_List_Table->search_box( __( 'Buscar', $this->plugin_text_domain ), 'codigos-find');
					?>
					<div id="filtros" style="display: flex;">
						<div style="margin-right: 15px;">
							<label>Tipo</label>
							<select name="tipo">
								<option value="todos">Todos</option>
								<option value="beneficios" <?php echo isset($_GET['tipo']) && $_GET['tipo'] == 'beneficios' ? 'selected="selected"' : ''; ?>>Beneficios</option>
								<option value="eventos" <?php echo isset($_GET['tipo']) && $_GET['tipo'] == 'eventos' ? 'selected="selected"' : ''; ?>>Eventos</option>
							</select>
						</div>
						<div style="margin-right: 15px;">
							<label>Estado</label>
							<select name="estado">
								<option value="todos">Todos</option>
								<option value="activos" <?php echo isset($_GET['estado']) && $_GET['estado'] == 'activos' ? 'selected="selected"' : ''; ?>>Activos</option>
								<option value="vencidos" <?php echo isset($_GET['estado']) && $_GET['estado'] == 'vencidos' ? 'selected="selected"' : ''; ?>>Vencidos</option>
							</select>
						</div>
						<div style="margin-right: 15px;">
							<label>De</label>
							<input class="datepicker" type="text" name="desde" style="width: 90px;" value="<?php echo isset($_GET['desde']) ? $_GET['desde'] : ''; ?>" />
							<label>A</label>
							<input class="datepicker" type="text" name="hasta" style="width: 90px;" value="<?php echo isset($_GET['hasta']) ? $_GET['hasta'] : ''; ?>" />
						</div>
						<button>Filtrar</button>
					</div>
					<?php
						$this->Codigos_List_Table->display(); 
					?>					
				</form>
            </div>			
        </div>
</div>
<script type="text/javascript">
	jQuery('[name="mostrar"]').change(function(){
		var url = window.location.href;
		var direccion = replaceUrlParam(url, 'mostrar', jQuery(this).val());
		window.location.href = direccion;
	});
	function replaceUrlParam(url, paramName, paramValue) {
	    if (paramValue == null) {
	        paramValue = '';
	    }
	    var pattern = new RegExp('\\b('+paramName+'=).*?(&|#|$)');
	    if (url.search(pattern)>=0) {
	        return url.replace(pattern,'$1' + paramValue + '$2');
	    }
	    url = url.replace(/[?#]$/,'');
	    return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue;
	}
</script>