<?php
namespace Modulo_Reportes_ENDI\Inc\Admin;
use Modulo_Reportes_ENDI\Inc\Libraries;

/**
 * @link       http://www.dinardi.com.ar
 * @since      1.0.0
 * @author     Jazmín Nasta
 */
class Codigos_List_Table extends Libraries\WP_List_Table  {

	protected $plugin_text_domain;
	protected $actual_link;
	protected $usuario_logueado;
	
	public function __construct( $plugin_text_domain ) {
		
		$this->plugin_text_domain = $plugin_text_domain;

		$link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$partes = explode('?', $link);
		$this->actual_link = $partes[0].'?page='.$_GET['page'];

		$this->usuario_logueado = get_current_user_id();
		
		parent::__construct( array( 
			'plural'	=>	'beneficios',
			'singular'	=>	'beneficio',
			'ajax'		=>	false,
		) );
	}

	public function display() {
		$singular = $this->_args['singular'];

		$this->display_tablenav( 'top' );

		$this->screen->render_screen_reader_content( 'heading_list' );
?>
<table class="wp-list-table <?php echo implode( ' ', $this->get_table_classes() ); ?>">
	<thead>
	<tr>
		<?php $this->print_column_headers(); ?>
	</tr>
	</thead>

	<tbody id="the-list"<?php
		if ( $singular ) {
			echo " data-wp-lists='list:$singular'";
		} ?>>
		<?php $this->display_rows_or_placeholder(); ?>
	</tbody>

	<tfoot>
	<tr>
		<?php $this->print_column_headers( false ); ?>
	</tr>
	</tfoot>

</table>
<div class="tablenav bottom">

		<?php if ( $this->has_items() ) : ?>
		<div class="alignleft actions bulkactions">
			<?php $this->bulk_actions( 'bottom' ); ?>
		</div>
			<?php
		endif;
		$this->extra_tablenav( 'bottom' );
		$this->pagination( 'bottom' );
		$options = '';
		$cantidades = [15, 25, 50, 100, 500];
		foreach ($cantidades as $cantidad) {
			$selected = isset($_GET['mostrar']) && $cantidad == $_GET['mostrar'] ? 'selected' : '';
			$options .= '<option value="'.$cantidad.'" '.$selected.'>'.$cantidad.'</option>';
		}
		echo '<div class="alignright" style="margin: 3px 20px 9px;"><span class="displaying-num">Mostrar</span><select name="mostrar">'.$options.'</select></div>';
		?>

		<br class="clear" />
	</div>
<?php
		// $this->display_tablenav( 'bottom' );
	}

	function display_tablenav($which) {
	    ?>
	    <div class="tablenav <?php echo esc_attr( $which ); ?>">
	        <div class="alignleft actions">
	            <?php $this->bulk_actions(); ?>
	        </div>
	        <?php
	        $this->extra_tablenav( $which );
	        $this->pagination( $which );
	        ?>
	        <br class="clear" />
	    </div>
	    <?php
	}
	
	public function prepare_items() {
		$user_search_key = isset( $_REQUEST['s'] ) ? wp_unslash( trim( $_REQUEST['s'] ) ) : '';
		
		$this->_column_headers = $this->get_column_info();
		
		$this->handle_table_actions();
		
		$table_data = $this->fetch_table_data();

		if( $user_search_key ) {
			$table_data = $this->filter_table_data( $table_data, $user_search_key );
		}		
		
		$codigos_per_page = $_GET['mostrar'] ? $_GET['mostrar'] : 15;
		$table_page = $this->get_pagenum();		
		
		$this->items = array_slice( $table_data, ( ( $table_page - 1 ) * $codigos_per_page ), $codigos_per_page );

		$total_codigos = count( $table_data );
		$this->set_pagination_args( array (
			'total_items' => $total_codigos,
			'per_page'    => $codigos_per_page,
			'total_pages' => ceil( $total_codigos/$codigos_per_page )
		) );
	}
	
	public function get_columns() {
		
		$table_columns = array(
			'cb'		=> '<input type="checkbox" />',
			// 'ID' => _x( 'ID', 'column name', $this->plugin_text_domain ),
			'tipo' => 'Tipo',
			'post_title' => 'Beneficio/Evento',
			'estado' => 'Estado',
			'visitas' => 'Visitas',
			'canjes' => 'Descargas',
			'inicio' => 'Inicio',
			'fin' => 'Fin',
		);
		return $table_columns;
	}

	public function clean($string) {
	   $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
	   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}

	protected function column_cb( $item ) {
		return sprintf(		
		'<label class="screen-reader-text" for="beneficio_' . $item['ID'] . '">' . sprintf( __( 'Seleccionar %s' ), $this->clean($item['post_title']) ) . '</label>'
		. "<input type='checkbox' name='beneficios[]' id='beneficio_{$item['ID']}' value='{$item['ID']}' />"					
		);
	}

	protected function column_estado($item) {
		return ucfirst($item['estado']);
	}

	protected function column_inicio($item) {
		return ucfirst($item['inicio']);
	}

	protected function column_fin($item) {
		return ucfirst($item['fin']);
	}

	protected function column_tipo($item) {
		return ucfirst($item['tipo']);
	}

	protected function column_usuarios( $item ) {
		if(!$item['usuarios'])
			return '';
		$usuarios = '| ';
		foreach(unserialize($item['usuarios']) as $usuario) {
			$usuarios .= $usuario.' | ';
		}
		return $usuarios;
	}

	protected function column_visitas( $item ) {
		return $item['visitas'];
	}

	protected function column_canjes( $item ) {
		return $item['canjes'];
	}
	
	protected function get_sortable_columns() {
		$sortable_columns = array (
			'ID' => array( 'ID', true ),
			'post_title'=> 'post_title',
			'tipo' => 'post_type',
			'inicio' => 'inicio',
			'fin' => 'fin',
			'estado' => 'estado',
			'visitas' => 'visitas',
			'canjes' => 'canjes',
		);
		return $sortable_columns;
	}	
	
	public function no_items() {
		_e( 'No hay códigos.', $this->plugin_text_domain );
	}	
	
	public function fetch_table_data() {
		global $wpdb;
		$orderby = ( isset( $_GET['orderby'] ) ) ? esc_sql( $_GET['orderby'] ) : 'inicio';
		$order = ( isset( $_GET['order'] ) ) ? esc_sql( $_GET['order'] ) : 'DESC';
		$having = 'HAVING 1';
		if($_GET['tipo'] && $_GET['tipo'] != 'todos')
			$having .= " AND tipo='".$_GET['tipo']."'";
		if($_GET['estado'] && $_GET['estado'] != 'todos')
			$having .= " AND estado='".$_GET['estado']."'";
		$where = '';

		$desde = (isset($_GET['desde'])) ? date_format(date_create_from_format('Y-m-d', $_GET['desde']), 'Y-m-d') : '2001-01-01';
		$hasta = (isset($_GET['hasta'])) ? date_format(date_create_from_format('Y-m-d', $_GET['hasta']), 'Y-m-d') : '2050-01-01';

		if($_GET['desde'] || $_GET['hasta'])
			$having .= ' AND ((CAST("'.$desde.'" as DATE) <= cast(inicio as DATE) AND CAST("'.$hasta.'" as DATE) >= cast(inicio as DATE)) OR (CAST("'.$desde.'" as DATE) <= cast(fin as DATE) AND CAST("'.$desde.'" as DATE) >= cast(inicio as DATE))) ';

		// $user_query = "SELECT p.id as ID, p.post_title, IF(p.post_type = 'beneficios', (SELECT count(1) FROM wp_cupones u WHERE p.ID = u.cupon_id), (SELECT count(1) FROM wp_eventos e WHERE p.ID = e.evento_id)) as canjes, (SELECT count(1) FROM wp_visitas c WHERE p.ID = c.beneficio_id) as visitas, (SELECT m.meta_value FROM wp_postmeta m WHERE p.ID = m.post_id AND m.meta_key = 'reportes') as usuarios FROM wp_posts p WHERE p.post_status = 'publish' AND (p.post_type = 'beneficios' OR p.post_type = 'eventos') ORDER BY $orderby $order";
		$user_query = "SELECT p.id as ID, p.post_title, p.post_type as tipo, 
		IF(p.post_type = 'beneficios', (SELECT count(1) FROM wp_cupones u WHERE p.ID = u.cupon_id), (SELECT count(1) FROM wp_eventos e WHERE p.ID = e.evento_id)) as canjes, 
		(SELECT count(1) FROM wp_visitas c WHERE p.ID = c.beneficio_id) as visitas, 
		(SELECT m.meta_value FROM wp_postmeta m WHERE p.ID = m.post_id AND m.meta_key = 'merchant') as merchant, 
		(SELECT m.meta_value FROM wp_postmeta m WHERE m.post_ID = merchant AND m.meta_key = 'reportes') as usuarios,
		CAST(p.post_date as DATE) as inicio, 
		CAST((SELECT m.meta_value FROM wp_postmeta m WHERE p.ID = m.post_id AND m.meta_key = 'validez') as DATE) as fin,
		if((CAST(now() as DATE) BETWEEN p.post_date AND (SELECT m.meta_value FROM wp_postmeta m WHERE p.ID = m.post_id AND m.meta_key = 'validez')), 'Activos', 'Vencidos') as estado
		FROM wp_posts p WHERE p.post_status = 'publish' AND (p.post_type = 'beneficios' OR p.post_type = 'eventos') $having ORDER BY $orderby $order";
		// echo $user_query;
		$query_results = $wpdb->get_results( $user_query, ARRAY_A  );
		if (current_user_can('administrator')) {
			return $query_results;
		} else {
			return array_filter($query_results, array($this, "sacarAjenos"));
		}
	}

	public function sacarAjenos($item) {
		$array = $item['usuarios'] ? unserialize($item['usuarios']) : [];
		return in_array($this->usuario_logueado, $array);
	}
	
	public function filter_table_data( $table_data, $search_key ) {
		$filtered_table_data = array_values( array_filter( $table_data, function( $row ) use( $search_key ) {
			foreach( $row as $row_val ) {
				if( stripos( $row_val, $search_key ) !== false ) {
					return true;
				}				
			}			
		} ) );
		return $filtered_table_data;
	}
	
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {			
			case 'post_title':
			case 'ID':
				return $item[$column_name];
			default:
			  return $item[$column_name];
		}
	}
	
	public function get_bulk_actions() {
		$actions = array(
			'bulk-download' => 'Bajar estadísticas'
		);

		 return $actions;
	}

	public function handle_table_actions() {
		// wp_die($this->current_action());
		// if ( ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] === 'bulk-download' ) || ( isset( $_REQUEST['action2'] ) && $_REQUEST['action2'] === 'bulk-download' ) ) {	
		// 	$this->page_bulk_download($_REQUEST['beneficios']);
		// 	$this->graceful_exit();
		// }
	}
	
	public function page_bulk_download( $bulk_codigos_ids ) {	
	    $this->graceful_exit();	
	} 

	public function graceful_exit() {
		exit;
	}
	  	 
	public function invalid_nonce_redirect() {
		wp_die( __( 'Invalid Nonce', $this->plugin_text_domain ),
				__( 'Error', $this->plugin_text_domain ),
				array( 
						'response' 	=> 403, 
						'back_link' =>  esc_url( add_query_arg( array( 'page' => wp_unslash( $_REQUEST['page'] ) ) , admin_url( 'admin.php?page=modulo-reportes-endi' ) ) ),
					)
		);
	 }
}