<?php

namespace Modulo_Reportes_ENDI\Inc\Admin;

/**
 * @link       http://www.dinardi.com.ar
 * @since      1.0.0
 * @author    Jazmín Nasta
 */
class Admin {
	private $plugin_name;
	private $version;
	private $plugin_text_domain;
	private $Codigos_List_Table;	

	public function __construct( $plugin_name, $version, $plugin_text_domain ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->plugin_text_domain = $plugin_text_domain;
	}

	public function enqueue_styles() {
		wp_enqueue_style('datepicker', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/modulo-reportes-endi-admin.css', array(), null, 'all' );
	}

	public function enqueue_scripts() {
		wp_enqueue_script('jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js', array('jquery'), '', false);
		wp_enqueue_script('modulo-reportes', plugin_dir_url( __FILE__ ) . 'js/modulo-reportes-endi-admin.js', array('jquery', 'jquery-ui'), '', false);
		// $params = array ( 'ajaxurl' => plugin_dir_url( __FILE__ ).'generar.php' );
		// wp_enqueue_script( 'codigos_ajax_handle', plugin_dir_url( __FILE__ ) . 'js/modulo-reportes-endi-admin.js', array( 'jquery' ), $this->version, false );				
		// wp_localize_script( 'codigos_ajax_handle', 'ajax_codigos', $params );		
	}
	
	public function add_plugin_admin_menu() {
		$page_hook = add_menu_page(
			__( 'Reportes por Beneficio/Evento', $this->plugin_text_domain ),
			__( 'Reportes', $this->plugin_text_domain ),
			'manage_reportes',
			$this->plugin_name,
			array( $this, 'load_Codigos_List_Table' ),
			'dashicons-format-aside', 4
		);
		add_action( 'load-'.$page_hook, array( $this, 'load_Codigos_List_Table_screen_options' ) );
	}
	
	/**
	* Screen options for the List Table
	*
	* Callback for the load-($page_hook_suffix)
	* Called when the plugin page is loaded
	* 
	* @since    1.0.0
	*/
	public function load_Codigos_List_Table_screen_options() {
				
		$arguments	=	array(
						'label'		=>	__( 'Beneficios/Eventos por página', $this->plugin_text_domain ),
						'default'	=>	5,
						'option'	=>	'beneficios_por_pagina'
					);
		
		add_screen_option( 'per_page', $arguments );
		
		// instantiate the User List Table
		$this->Codigos_List_Table = new Codigos_List_Table( $this->plugin_text_domain );		
		
	}
	
	/*
	 * Display the User List Table
	 * 
	 * Callback for the add_users_page() in the add_plugin_admin_menu() method of this class.
	 * 
	 * @since	1.0.0
	 */
	public function load_Codigos_List_Table(){
		
		// query, filter, and sort the data
		$this->Codigos_List_Table->prepare_items();
		
		// render the List Table
		include_once( 'views/partials-wp-list-table-demo-display.php' );
	}

}