<?php

namespace Modulo_Reportes_ENDI\Inc\Core;

/**
 * Fired during plugin deactivation
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @link       http://www.dinardi.com.ar
 * @since      1.0.0
 *
 * @author     Jazmín Nasta
 */

class Deactivator {

	/**
	 * Short Description.
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
