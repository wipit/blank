<?php
/**
 *
 * @link              http://www.dinardi.com.ar
 * @since             1.0.0
 * @package           Modulo_Reportes_ENDI
 *
 * @wordpress-plugin
 * Plugin Name:       Módulo de reportes
 * Plugin URI:        https://www.dinardi.com.ar/
 * Description:       Plugin para mostrar reportes
 * Version:           1.0.0
 * Author:            Jazmín Nasta
 * Author URI:        https://www.dinardi.com.ar/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       modulo-reportes-endi
 * Domain Path:       /languages
 */

namespace Modulo_Reportes_ENDI;

if ( ! defined( 'WPINC' ) ) {
	die;
}

define( __NAMESPACE__ . '\NS', __NAMESPACE__ . '\\' );
define( NS . 'PLUGIN_NAME', 'modulo-reportes-endi' );
define( NS . 'PLUGIN_VERSION', '1.0.0' );
define( NS . 'PLUGIN_NAME_DIR', plugin_dir_path( __FILE__ ) );
define( NS . 'PLUGIN_NAME_URL', plugin_dir_url( __FILE__ ) );
define( NS . 'PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
define( NS . 'PLUGIN_TEXT_DOMAIN', 'modulo-reportes-endi' );

require_once( PLUGIN_NAME_DIR . 'inc/libraries/autoloader.php' );
register_activation_hook( __FILE__, array( NS . 'Inc\Core\Activator', 'activate' ) );
register_deactivation_hook( __FILE__, array( NS . 'Inc\Core\Deactivator', 'deactivate' ) );

class Modulo_Reportes_ENDI {

	static $init;
	public static function init() {
		if ( null == self::$init ) {
			self::$init = new Inc\Core\Init();
			self::$init->run();
		}
		return self::$init;
	}
}

function Modulo_Reportes_ENDI_init() {
	return Modulo_Reportes_ENDI::init();
}

$min_php = '5.6.0';
if ( version_compare( PHP_VERSION, $min_php, '>=' ) ) {
	Modulo_Reportes_ENDI_init();
}
