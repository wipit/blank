<?php
   /*
   Plugin Name: Plugin para Club ENDI
   Plugin URI: http://www.clubendi.com
   description: Plugin para inclusión de Gigya en sitio del Club El Nuevo Día
   Version: 1
   Author: Jazmín Nasta
   Author URI: http://www.dinardi.com.ar
   License: GPL2
   */

   function club_scripts() {
      // wp_enqueue_style('gigya', get_template_directory_uri() . '/css/gigya.css');
      wp_enqueue_script('axios', '//unpkg.com/axios/dist/axios.min.js', array(), '', true);
      wp_enqueue_script('vue', SCRIPT_VUE, array(), '', true);
      wp_register_script( 'socialize', 'https://cdns.gigya.com/JS/socialize.js?apiKey='.GIGYA_KEY, array('vue'), null, false);
      wp_enqueue_script( 'socialize' );
      wp_enqueue_script('gigya', get_template_directory_uri() . '/js/gigya.js?v3', array(), null, true);
   }
   add_action('wp_enqueue_scripts', 'club_scripts');

   add_filter('script_loader_tag', 'add_attributes_to_script', 10, 3); 
   function add_attributes_to_script( $tag, $handle, $src ) {
       if ( 'socialize' === $handle ) {
           $tag = '<script type="text/javascript" src="' . esc_url( $src ) . '">{ 
              "regSource": "club.elnuevodia.com"
              , "lang": "es"
              , "siteName": "club.elnuevodia.com"
              , "enabledProviders": "facebook,twitter,whatsapp,googleplus,email,pinterest"
              , "connectWithoutLoginBehavior": "loginExistingUser"
              , "autoLogin": true
              , "sessionExpiration": "-2" 
            }</script>';
       } 
       return $tag;
   }

   add_action('wp_ajax_confirmar_suscripcion', 'confirmar_suscripcion');
   add_action('wp_ajax_nopriv_confirmar_suscripcion', 'confirmar_suscripcion');
   function confirmar_suscripcion() {
      $respuesta['CustID'] = 123;
      $respuesta['respuesta'] = 'ok';
      echo json_encode($respuesta);
      die();
      $respuesta = array();
      if(!isset($_COOKIE['jwt'])) {
         $respuesta['CustID'] = null;
         $respuesta['respuesta'] = 'no';
         echo json_encode($respuesta);
         die();
      } else {
         $jwt = $_COOKIE['jwt'];
      }
      $method = 'GET';
      $headers = array(
         'Authorization' => $jwt
      );
      $request = array(
         'headers' => $headers,
         'method'  => $method,
      );

      // NO
      // $user = '4d6ff7922e544c8c901f2fbf8f8902e2';
      // $email = 'clubendi.gfrm1@gmail.com';
      // SI
      $user = '5db0345690e54925ac66cb79be076d44';
      $email = 'cc.gfrm5@outlook.com';

      $user = json_decode(html_entity_decode(stripslashes($_COOKIE['user'])));
      $url = GFRM_URL.'?GigyaUserID='.$user->UID.'&Email='.$user->email;
      $request = wp_remote_get($url, $request);
      $response = wp_remote_retrieve_body($request);
      $array = json_decode($response, true);
      if(isset($array['CustID']) && $array['CustID']) {
         $respuesta['CustID'] = $array['CustID'];
         $respuesta['respuesta'] = 'ok';
      } else {
         $respuesta['CustID'] = null;
         $respuesta['respuesta'] = 'no';
      }
      echo json_encode($respuesta);
      die();
   }

   add_action('wp_ajax_chequear_suscripcion', 'chequear_suscripcion');
   add_action('wp_ajax_nopriv_chequear_suscripcion', 'chequear_suscripcion');
   function chequear_suscripcion($jwt, $uid, $email) {
      $respuesta['CustID'] = 123;
      $respuesta['respuesta'] = 'ok';
      $respuesta['nuevo'] = false;
      return $respuesta;
      die();
      if(!isset($jwt))
         return 'no';
      $method = 'GET';
      $headers = array(
         'Authorization' => $jwt
      );
      $request = array(
         'headers' => $headers,
         'method'  => $method,
      );

      $uid = '5db0345690e54925ac66cb79be076d44';
      $email = 'cc.gfrm5@outlook.com';
      // $uid = $_COOKIE['user']->UID;
      // $email = $_COOKIE['user']->email;

      $url = GFRM_URL.'?GigyaUserID='.$uid.'&Email='.$email."&t=".time();
      $request = wp_remote_get($url, $request);
      $response = wp_remote_retrieve_body($request);
      $array = json_decode($response, true);
      $respuesta = array();
      $nuevo = false;

      //$respuesta['debug_url'] =  $url;      

      if(isset($array['StartDate'])) {
         $ahora = new DateTime(date("Y-m-d"));
         $fecha = explode(' ', $array['StartDate']);
         $desde = new DateTime($fecha[0]);
         $diferencia = $desde->diff($ahora);
         $nuevo = $diferencia->days < 90 ? true : false;
      }
      if(isset($array['CustID']) && $array['CustID']) {
         $respuesta['CustID'] = $array['CustID'];
         $respuesta['nuevo'] = $nuevo;
         $respuesta['respuesta'] = 'ok';
         setcookie('CustID', $array['CustID'], time() + (86400 * 365), '/' ); 
         return $respuesta;
      } else {
         $respuesta['CustID'] = null;
         $respuesta['nuevo'] = $nuevo;
         $respuesta['respuesta'] = 'no';
         setcookie('CustID', null, time() + (86400 * 365), '/' ); 
         return $respuesta;
      }
      die();
   }

   add_action('wp_ajax_chequear_existencia', 'chequear_existencia');
   add_action('wp_ajax_nopriv_chequear_existencia', 'chequear_existencia');
   function chequear_existencia() {
      global $wpdb;
      $user = json_decode(html_entity_decode(stripslashes($_POST['user'])));
      $user->UID = $_POST['uid'];
      unset($user->capabilities);
      unset($user->identities);
      unset($user->loginProviderUID);
      unset($user->oldestDataUpdatedTimestamp);
      unset($user->signatureTimestamp);
      unset($user->UIDSig);
      setcookie('logged', true, time() + (86400 * 365), '/' ); 
      setcookie('user', json_encode($user), time() + (86400 * 365), '/' ); 
      setcookie('cupones', [], time() + (86400 * 365), '/' ); 
      setcookie('eventos', [], time() + (86400 * 365), '/' ); 
      setcookie('jwt', $_POST['jwt'], time() + (86400 * 365), '/' );
      if(!isset($_COOKIE['nuevas'])) {
         setcookie('nuevas', 0, time() + (86400 * 365), '/' );
         $nuevas = 0;
      } else {
         $nuevas = $_COOKIE['nuevas'];
      }
      $respuesta = chequear_suscripcion($_POST['jwt'], $user->UID, $user->email);

      if($respuesta['respuesta'] == 'ok') {
         $wpdb->update('wp_usuarios', array('CustID' => $respuesta['CustID']), array('UID' => $user->UID, 'email' => $user->email));
         $suscripto = true;
      } else {
         $wpdb->update('wp_usuarios', array('CustID' => null), array('UID' => $user->UID, 'email' => $user->email));
         $suscripto = false;
      }
      setcookie('nuevo', $respuesta['nuevo'], time() + (86400 * 365), '/' ); 
      setcookie('suscripto', $suscripto, time() + (86400 * 365), '/' ); 
      $resultado = $wpdb->get_row("SELECT * FROM wp_usuarios WHERE uid = '".$_POST['uid']."'");
      if (null !== $resultado) {
         setcookie('id', $resultado->id, time() + (86400 * 365), '/' ); 
         $array_c = array();
         if($resultado->CustID) {
            //$sql = "SELECT id FROM wp_usuarios WHERE CustID = '".$resultado->CustID."'";
            //$usuarios = $wpdb->get_col($sql);
            //$cupones = $wpdb->get_results("SELECT * FROM wp_cupones WHERE usuario_id IN (".implode(',', $usuarios).")");
            $cupones = $wpdb->get_results("SELECT * FROM wp_cupones WHERE CustID = '".$resultado->CustID."'");
            
            foreach ($cupones as $cupon) {
               $array_c[] = $cupon->cupon_id;
            }
         }
         setcookie('cupones', json_encode($array_c), time() + (86400 * 365), '/' );
         $array_e = array();
         if($resultado->CustID) {
            //$participaciones = $wpdb->get_results("SELECT * FROM wp_eventos WHERE usuario_id IN (".implode(',', $usuarios).")");
            $participaciones = $wpdb->get_results("SELECT * FROM wp_eventos WHERE CustID = '".$resultado->CustID."'");
            
            foreach ($participaciones as $participacion) {
               $array_e[] = $participacion->evento_id;
            }
         }
         setcookie('eventos', json_encode($array_e), time() + (86400 * 365), '/' ); 
         echo json_encode(['respuesta' => 'existe', 'cupones' => $array_c, 'eventos' => $array_e, 'id' => $resultado->id, 'suscripto' => $suscripto, 'nuevas' => $nuevas, 'nuevo' => $respuesta['nuevo']]);
      } else {
         $wpdb->insert('wp_usuarios', array('nombre' => $user->firstName, 'apellido' => $user->lastName, 'email' => $user->email, 'UID' => $_POST['uid'], 'created_at' => date('Y-m-d H:i:s')));
         $last_id = $wpdb->insert_id;
         setcookie('id', $last_id, time() + (86400 * 365), '/' ); 
         echo json_encode(['respuesta' => 'nuevo', 'id' => $last_id, 'suscripto' => $suscripto, 'nuevas' => $nuevas, 'nuevo' => $respuesta['nuevo']]);
      }
      die();
   }

   add_action('wp_ajax_calcular_ahorro', 'calcular_ahorro');
   add_action('wp_ajax_nopriv_calcular_ahorro', 'calcular_ahorro');
   function calcular_ahorro() {
      global $wpdb;
      $total = 0;
      $usuario = $wpdb->get_col("SELECT CustID FROM wp_usuarios WHERE id = '".$_POST['id']."'");
      $CustID = $usuario[0];
      //$usuarios = $wpdb->get_col("SELECT id FROM wp_usuarios WHERE CustID = '".$CustID."'");
      //$resultado = $wpdb->get_results("SELECT cupon_id FROM wp_cupones c INNER JOIN wp_posts p ON c.cupon_id = p.ID WHERE usuario_id IN (".implode(',', $usuarios).") AND p.post_status = 'publish'");
      $resultado = $wpdb->get_results("SELECT cupon_id FROM wp_cupones c INNER JOIN wp_posts p ON c.cupon_id = p.ID WHERE CustID='".$CustID."' AND p.post_status = 'publish'");
      foreach($resultado as $cupon) {
         $datos = $wpdb->get_results("SELECT p.post_title, m1.meta_value as 'descuento_tipo_de_beneficio', m3.meta_value as 'descuento_porcentaje',
            IF(m2.meta_value <> '', m2.meta_value, m4.meta_value) as 'descuento_precio_original'  
            FROM wp_posts p LEFT JOIN wp_postmeta m1 ON p.id = m1.post_id 
            AND m1.meta_key = 'descuento_tipo_de_beneficio' 
            LEFT JOIN wp_postmeta m2 ON p.id = m2.post_id AND m2.meta_key = 'descuento_precio_original' 
            LEFT JOIN wp_postmeta m3 ON p.id = m3.post_id AND m3.meta_key = 'descuento_porcentaje' 
            LEFT JOIN wp_postmeta m4 ON p.id = m4.post_id AND m4.meta_key = 'descuento_valor_promedio'
            WHERE p.id = ".$cupon->cupon_id);
         foreach($datos as $row) {
            if($row->descuento_tipo_de_beneficio == 'fijo') {
               $sub = $row->descuento_porcentaje;
            } else if($row->descuento_tipo_de_beneficio == 'dos') {
               $sub = $row->descuento_precio_original * 0.5;
            } else if($row->descuento_tipo_de_beneficio == 'regalo' || $row->descuento_tipo_de_beneficio == 'promo') {
               $sub = $row->descuento_precio_original;
            } else {
               $sub = $row->descuento_precio_original * $row->descuento_porcentaje / 100;
            }
            $total += $sub;
         }
      }
      $resultado = $wpdb->get_results("SELECT e.*, w.post_title, m.* FROM wp_posts w, wp_eventos e, wp_postmeta m WHERE w.post_type = 'eventos' AND e.CustID='".$CustID."' AND w.id = e.evento_id AND w.id = m.post_id AND m.meta_key = 'precio_original' AND ganador = 'Y'");
      foreach($resultado as $evento) {
         $total += $evento->meta_value;
      }
      echo formatoPrecio($total);
      die();
   }

   add_action('wp_ajax_actualizar_datos', 'actualizar_datos' );
   add_action('wp_ajax_nopriv_actualizar_datos', 'actualizar_datos' );
   function actualizar_datos() {
      $user = json_decode(html_entity_decode(stripslashes($_POST['datos'])));
      setcookie('user', $user, time() + (86400 * 365), '/' ); 
      global $wpdb;
      $wpdb->update('wp_usuarios', array('nombre' => $user->firstName, 'apellido' => $user->lastName, 'email' => $user->email), array('UID' => $user->UID));
      // echo $wpdb->last_query;
      die();
   }

   add_action('wp_ajax_chequear_status', 'chequear_status' );
   add_action('wp_ajax_nopriv_chequear_status', 'chequear_status' );
   function chequear_status() {
      $logged = isset($_COOKIE['logged']) && $_COOKIE['logged'] == true ? true : false;
      $user = isset($_COOKIE['user']) ? json_decode(stripslashes($_COOKIE['user'])) : [];
      $cupones = isset($_COOKIE['cupones']) ? json_decode(stripslashes($_COOKIE['cupones'])) : [];
      $eventos = isset($_COOKIE['eventos']) ? json_decode(stripslashes($_COOKIE['eventos'])) : [];
      //$suscripto = isset($_COOKIE['suscripto']) ? $_COOKIE['suscripto'] : false;
      $id = isset($_COOKIE['id']) ? $_COOKIE['id'] : false;
      $nuevas = isset($_COOKIE['nuevas']) ? $_COOKIE['nuevas'] : 0;

      if($logged) {
         global $wpdb;
         $jwt = isset($_COOKIE['jwt']) ? $_COOKIE['jwt'] : false;
         $respuesta = chequear_suscripcion($jwt, $user->UID, $user->email);
         //print_r($respuesta); die("FIN");
         if($respuesta['respuesta'] == 'ok') {
            $wpdb->update('wp_usuarios', array('CustID' => $respuesta['CustID']), array('UID' => $user->UID, 'email' => $user->email));
            $suscripto = true;
         } else {
            $wpdb->update('wp_usuarios', array('CustID' => null), array('UID' => $user->UID, 'email' => $user->email));
            $suscripto = false;
         }
         setcookie('suscripto', $suscripto, time() + (86400 * 365), '/' ); 
      }
      //die("FIN2");

      echo json_encode(['logged' => $logged, 'user' => $user, 'cupones' => $cupones, 'eventos' => $eventos, 'id' => $id, 'suscripto' => $suscripto, 'nuevas' => $nuevas, 'UID' => $user->UID]);
      //echo json_encode(['url_debug' => $respuesta['debug_url'], 'url_CustID' => $respuesta['CustID'], 'logged' => $logged, 'user' => $user, 'cupones' => $cupones, 'eventos' => $eventos, 'id' => $id, 'suscripto' => $suscripto, 'nuevas' => $nuevas]);
      die();
   }

   add_action('wp_ajax_borrar_nuevas', 'borrar_nuevas' );
   add_action('wp_ajax_nopriv_borrar_nuevas', 'borrar_nuevas' );
   function borrar_nuevas() {
      setcookie('nuevas', 0, time() + (86400 * 365), '/' ); 
      die();
   }

   add_action('wp_ajax_consulta_usuario', 'consulta_usuario' );
   add_action('wp_ajax_nopriv_consulta_usuario', 'consulta_usuario' );
   function consulta_usuario() {
      global $wpdb;
      if(!$_POST['token'] || $_POST['token'] != ']pYKAntZ0<%H9{U#5a+s~1]>6T9NU516K20Nw9]vh;_B.tR{)OW!3m8Pj3J@K<A7}p/HZd§Dv%~!/5!1vI4cR}fX+Wp(Cs.3tXq6§(%^$nl|V2wkH§XZe$o4+D},°.S8l4IjYaM_,JaIPP1G4{°EZy,T&TRv=s?P_oS9%Txu]Mme_x%Eq7V4:8W3iZIuh*$0DzuHI.D]°]M(Dh2tswQoc:aoGGdf§8q3yd|+L&vU<H39L61°%;VGGX=+!4I9-*i°/PP{29$6!KhFd~*j83-E|5mO§oAh8o_q=QNf(IcGBm,>vEP{v39I9nSKc7zs>1i<2fu:m9?A5]§d|d<n-^c+tY0bmaa!0[38E7Cl{wvY|5PF*H°09YEAr*Ose7U4a°zdC2P1O(>*c}A:9vK$sa2bY4xNN~^5VtCnw°{y°[°]^7H5°URf,95ei7~JHgPvNC?W(2+Ph3p{s0V[?9:vl%T8nP&y;°+95m,=qp6@;kb9>Xix*nUIuSl{/Ow(Y}tsalvN^x6VivgYS-*YV@!+};>TICrP^QdlClzP|jO]8|}!V)gRy,.a1C5y={e.%CnSz?k~xyyHL!O&![n?}9-UHHZhnD2nRC_4X8$3Dn)},uE:ik~)w25f<I6^QzK&L)3>/KP-') {
         die();
      }
      $usuario_id = isset($_POST['CustID']) ? esc_sql($_POST['CustID']) : null;
      $inicio = isset($_POST['inicio']) ? esc_sql($_POST['inicio']).' 00:00:00' : '2018-01-01 00:00:00';
      $fin = isset($_POST['fin']) ? esc_sql($_POST['fin']).' 23:59:59' : date('Y-m-d').' 23:59:59';
      // $usuarios = $wpdb->get_col("SELECT id FROM wp_usuarios WHERE CustID = '".$usuario_id."'");
      $respuesta = [
         'status' => 200,
         'data' => []
      ];

      /* MATS ADDED ******************
      if(!$usuario_id) {
      // if(count($usuarios) < 1) {
         $respuesta['status'] = 400;
         echo json_encode($respuesta);
         die();
      } 
      */

      // $where = $usuario_id ? "AND usuario_id IN (".implode(',', $usuarios).")" : "";
      $where_cupones = "";
      $where_eventos = "";
         
      if($usuario_id != null) { /* MATS ADDED ******************/
         $where_cupones = "AND c.custID = ".$usuario_id;
         $where_eventos = "AND e.custID = ".$usuario_id;
         // $groupby = $usuario_id ? "GROUP BY Numero_cupon" : "";
      } /* MATS ADDED ******************/

      switch(get_field('exclusivo', $_POST['post'])) {
         case 'si':
            $exclusivo = 'Exclusivo';
            break;
         case 'ambos':
            $exclusivo = 'Libre';
            break;
         default:
            $exclusivo = 'Regular';
            break;
      }

      $resultados = $wpdb->get_results("(SELECT c.CustID as Customer_ID, d.codigo as Numero_cupon, p.post_title as Nombre_cupon, c.created_at as Fecha_agregado, 'Beneficio' as Tipo, IF(m1.meta_value = 'dos', '2x1', m1.meta_value) as Tipo_de_descuento, IF(m1.meta_value = 'regalo' OR m1.meta_value = 'promo', IF(m2.meta_value <> '', m2.meta_value * 1, m4.meta_value), IF(m1.meta_value = 'dos', 0.5 * IF(m2.meta_value <> '', m2.meta_value * 1, m4.meta_value), m3.meta_value * 1)) as Valor_del_descuento,
         IF(m2.meta_value <> '', m2.meta_value * 1, m4.meta_value) as Valor_promedio_de_compra, DATE(m5.meta_value) as Fecha_de_vencimiento, if(t.parent=0, s.name, t2.name) AS Categoria, p2.post_title as Merchant, IF(m7.meta_value = 'si', 'Exclusivo', IF(m7.meta_value = 'ambos', 'Libre', 'Regular')) as Tipo_iniciativa,
         (CASE
            WHEN m1.meta_value = 'fijo' THEN m3.meta_value
            WHEN m1.meta_value = 'dos' THEN 0.5 * IF(m2.meta_value <> '', m2.meta_value, m4.meta_value)
            WHEN m1.meta_value = 'regalo' OR m1.meta_value = 'promo' THEN IF(m2.meta_value <> '', m2.meta_value, m4.meta_value)
            ELSE IF(m2.meta_value <> '', m2.meta_value, m4.meta_value) * m3.meta_value / 100
         END) as Total_ahorro
         FROM wp_cupones c 
         INNER JOIN wp_codigos d ON c.codigo_id = d.id
         LEFT JOIN wp_posts p ON c.cupon_id = p.id 
         LEFT JOIN wp_postmeta m1 ON p.id = m1.post_id AND m1.meta_key = 'descuento_tipo_de_beneficio' 
         LEFT JOIN wp_postmeta m2 ON p.id = m2.post_id AND m2.meta_key = 'descuento_precio_original' 
         LEFT JOIN wp_postmeta m3 ON p.id = m3.post_id AND m3.meta_key = 'descuento_porcentaje' 
         LEFT JOIN wp_postmeta m4 ON p.id = m4.post_id AND m4.meta_key = 'descuento_valor_promedio'
         LEFT JOIN wp_postmeta m5 ON p.id = m5.post_id AND m5.meta_key = 'validez'
         LEFT JOIN wp_postmeta m7 ON p.id = m7.post_id AND m7.meta_key = 'exclusivo'
         LEFT JOIN wp_term_relationships r ON (p.id = r.object_id)
         LEFT JOIN wp_term_taxonomy t ON (r.term_taxonomy_id = t.term_taxonomy_id)
         LEFT JOIN wp_terms t2 ON (t.parent = t2.term_id)
         LEFT JOIN wp_terms s ON t.term_id = s.term_id
         LEFT JOIN wp_postmeta m6 ON p.id = m6.post_id AND m6.meta_key = 'merchant'
         LEFT JOIN wp_posts p2 ON p2.id = m6.meta_value
         LEFT JOIN wp_usuarios u ON u.id = c.usuario_id
         WHERE c.created_at BETWEEN '".$inicio."' AND '".$fin."' ".$where_cupones." AND t.taxonomy='category' ".$groupby.") 
         UNION
         (SELECT e.CustID as Customer_ID, '' as Numero_cupon, p.post_title as Nombre_cupon, e.created_at as Fecha_agregado, 'Evento' as Tipo, 'Evento' as Tipo_de_descuento, 0 as Valor_del_descuento, 
         m1.meta_value * 1 as Valor_promedio_de_compra, DATE(m2.meta_value) as Fecha_de_vencimiento, 'Eventos' as Categoria, p2.post_title as Merchant, 'Regular' as Tipo_iniciativa, 0 as Total_ahorro FROM wp_eventos e 
         LEFT JOIN wp_posts p ON e.evento_id = p.id 
         LEFT JOIN wp_postmeta m1 ON p.id = m1.post_id AND m1.meta_key = 'precio_original' 
         LEFT JOIN wp_postmeta m2 ON p.id = m2.post_id AND m2.meta_key = 'validez'
         LEFT JOIN wp_postmeta m3 ON p.id = m3.post_id AND m3.meta_key = 'merchant'
         LEFT JOIN wp_posts p2 ON p2.id = m3.meta_value
         LEFT JOIN wp_usuarios u ON u.id = e.usuario_id
         WHERE e.created_at BETWEEN '".$inicio."' AND '".$fin."' ".$where_eventos.")
         UNION
         (SELECT e.CustID as Customer_ID, '' as Numero_cupon, p.post_title as Nombre_cupon, e.created_at as Fecha_agregado, 'Evento' as Tipo, 'Evento' as Tipo_de_descuento, m1.meta_value * 1 as Valor_del_descuento,
         m1.meta_value * 1 as Valor_promedio_de_compra, DATE(m2.meta_value) as Fecha_de_vencimiento, 'Eventos' as Categoria, p2.post_title as Merchant, 'Regular' as Tipo_iniciativa, m1.meta_value as Total_ahorro FROM wp_eventos e 
         LEFT JOIN wp_posts p ON e.evento_id = p.id 
         LEFT JOIN wp_postmeta m1 ON p.id = m1.post_id AND m1.meta_key = 'precio_original' 
         LEFT JOIN wp_postmeta m2 ON p.id = m2.post_id AND m2.meta_key = 'validez'
         LEFT JOIN wp_postmeta m3 ON p.id = m3.post_id AND m3.meta_key = 'merchant'
         LEFT JOIN wp_posts p2 ON p2.id = m3.meta_value
         LEFT JOIN wp_usuarios u ON u.id = e.usuario_id
         WHERE ganador='Y' AND e.created_at BETWEEN '".$inicio."' AND '".$fin."' ".$where_eventos.")", ARRAY_A);
      
      $respuesta['data'] = $resultados;
      // echo var_dump($respuesta);
      // IF(e.ganador = 'Y', m1.meta_value, 0)
      echo json_encode($respuesta);
      die();
   }

   function enviar_AWS($custID, $tipo, $cupon, $referencia, $valor, $promedio, $categoria, $fecha, $vencimiento, $localidad, $merchant, $iniciativa, $total) {
      if(ENTORNO == 'QA') {
         return;
      }
      require $_SERVER['DOCUMENT_ROOT'] .'/vendor/autoload.php';

      $queueUrl = AMAZON_URL;
      $client = new Aws\Sqs\SqsClient([
         'http' => [ 'verify' => false ],
         'version'     => 'latest',
         'region'      => 'us-east-1',
         'credentials' => [
            'key'    => 'AKIAIYN2NNQEBT67BBZQ',
            'secret' => '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',
         ]
      ]);

/* DP 
 $queueUrl = "https://sqs.us-west-2.amazonaws.com/478880111351/TestENDI";
 $client = new Aws\Sqs\SqsClient([
 'http' => [ 'verify' => false ],
     'version'     => 'latest',
     'region'      => 'us-west-2',
     'credentials' => [
         'key'    => 'AKIAJSISZDJEYTLPO2FA',
         'secret' => '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',
     ],
     // 'debug' => true
 ]);
*/
      $custID = $custID ? $custID : 0;
      $info = [
         "Customer_ID" =>  $custID,
         "Nombre_cupon" => $referencia,
         "Numero_cupon" => $cupon,
         "Tipo_de_descuento" => $tipo == 'dos' ? '2x1' : $tipo,
         "Tipo" => $tipo == 'Evento' ? 'Evento' : 'Beneficio',
         "Valor_del_descuento" => $valor,
         "Valor_promedio_de_compra" => $promedio,
         "Categoria" => $categoria,
         "Fecha_agregado" => $fecha,
         "Fecha_de_vencimiento" => $vencimiento,
         "Merchant" => $merchant,
         "Tipo_iniciativa" => $iniciativa,
         "Total_ahorro" => $total
      ];
      $params = [
          'DelaySeconds' => 10,
          'MessageBody' => json_encode($info),
          'QueueUrl' => $queueUrl
      ];
      $result = $client->sendMessage($params);
   }

   add_action('wp_ajax_agregar_cupon', 'agregar_cupon' );
   add_action('wp_ajax_nopriv_agregar_cupon', 'agregar_cupon' );
   function agregar_cupon() {
      date_default_timezone_set("America/Puerto_Rico"); 
      global $wpdb;
      $user = json_decode(stripslashes($_COOKIE['user']));
      $respuesta = chequear_suscripcion($_COOKIE['jwt'], $user->UID, $user->email);
      if($respuesta['respuesta'] == 'ok') {
         $wpdb->update('wp_usuarios', array('CustID' => $respuesta['CustID']), array('UID' => $user->UID, 'email' => $user->email));
         $suscripto = true;
      } else {
         $wpdb->update('wp_usuarios', array('CustID' => null), array('UID' => $user->UID, 'email' => $user->email));
         $suscripto = false;
      }
      setcookie('suscripto', $suscripto, time() + (86400 * 365), '/' ); 
      if(!$suscripto) {
         echo 'no_suscripto';
         die();
      }
      if($respuesta['nuevo'] == false && get_field('exclusivo', $_POST['post']) == 'si') {
         echo 'no_nuevo';
         die();
      }
      $post = $_POST['post'];
      $id = $_COOKIE['id'];
      $codigo = $wpdb->get_row("SELECT * FROM wp_codigos WHERE beneficio_id = '".$_POST['post']."' AND usos > 0");
      if(!$codigo) {
         echo 'no_codigo';
         die();
      }
      $canjeado = $wpdb->get_row("SELECT * FROM wp_cupones WHERE cupon_id = '".$_POST['post']."' AND custID='".$respuesta['CustID']."'");
      if($canjeado) {
         echo 'ya_agregado';
         die();
      }
      $fecha = date('Y-m-d H:i:s');
      // var_dump(array('cupon_id' => $post, 'custID' => $respuesta['CustID'], 'usuario_id' => $id, 'codigo_id' => $codigo->id, 'created_at' => $fecha));
      $wpdb->insert('wp_cupones', array('cupon_id' => $post, 'custID' => $respuesta['CustID'], 'usuario_id' => $id, 'codigo_id' => $codigo->id, 'created_at' => $fecha));
      $wpdb->update('wp_codigos', array('usos' => intval($codigo->usos - 1), 'canjeados' => intval($codigo->canjeados + 1)), array('id' => $codigo->id));
      $array = json_decode(stripslashes($_COOKIE['cupones']));
      $array[] = $_POST['post'];
      setcookie('cupones', json_encode($array), time() + (86400 * 365), '/' ); 
      // $referencia = get_the_title($_POST['post']);
      $referencia = get_post_field('post_title', $_POST['post'], 'raw');
      $tipo = get_post_meta($_POST['post'], 'descuento_tipo_de_beneficio', true);
      $promedio = get_post_meta($post, 'descuento_precio_original', true) ? get_post_meta($post, 'descuento_precio_original', true) : get_post_meta($post, 'descuento_valor_promedio', true);
      if($tipo == 'regalo' || $tipo == 'promo') {
         $valor = $promedio;
      } else if($tipo == 'dos') {
         $valor = $promedio * 0.5;
      } else {
         $valor = get_post_meta($_POST['post'], 'descuento_porcentaje', true);
      }
      // $valor = $tipo != 'regalo' ? get_post_meta($_POST['post'], 'descuento_porcentaje', true) : $promedio;
      $term = get_the_category($_POST['post']);
      $categoria_id = $term[0] && $term[0]->parent == '0' ? $term[0]->term_id : $term[0]->parent;
      $term = get_term($categoria_id, 'category');
      $categoria = $term->name;
      $vencimiento = get_field('validez', $_POST['post']);
      $localidad = 'A DEFINIR';
      $merchant = get_field('merchant', $_POST['post']);
      $merchant = $merchant->post_title;
      setcookie('nuevas', intval($_COOKIE['nuevas']) + 1, time() + (86400 * 365), '/' ); 
      switch(get_field('exclusivo', $_POST['post'])) {
         case 'si':
            $exclusivo = 'Exclusivo';
            break;
         case 'ambos':
            $exclusivo = 'Libre';
            break;
         default:
            $exclusivo = 'Regular';
            break;
      }
      if($tipo == 'fijo') {
         $total = $valor;
      } else if($tipo == 'dos') {
         $total = $promedio * 0.5;
      } else if($tipo == 'regalo' || $tipo == 'promo') {
         $total = $promedio;
      } else {
         $total = $promedio * $valor / 100;
      }
      enviar_AWS($respuesta['CustID'], $tipo, $codigo->codigo, $referencia, $valor, $promedio, $categoria, $fecha, $vencimiento, $localidad, $merchant, $exclusivo, $total);
      echo $wpdb->insert_id;
      die();
   }

   add_action('wp_ajax_agregar_evento', 'agregar_evento' );
   add_action('wp_ajax_nopriv_agregar_evento', 'agregar_evento' );
   function agregar_evento() {
      // echo 'FIN';
      // die();
      date_default_timezone_set("America/Puerto_Rico"); 
      $post = $_POST['post'];
      $id = $_COOKIE['id'];
      $array = json_decode(stripslashes($_COOKIE['eventos']));
      $array[] = $post;
      setcookie('eventos', json_encode($array), time() + (86400 * 365), '/' ); 
      $user = json_decode(stripslashes($_COOKIE['user']));
      $respuesta = chequear_suscripcion($_COOKIE['jwt'], $user->UID, $user->email);
      global $wpdb;
      if($respuesta['respuesta'] == 'ok') {
         $wpdb->update('wp_usuarios', array('CustID' => $respuesta['CustID']), array('UID' => $user->UID, 'email' => $user->email));
         $suscripto = true;
      } else {
         $wpdb->update('wp_usuarios', array('CustID' => null), array('UID' => $user->UID, 'email' => $user->email));
         $suscripto = false;
      }
      setcookie('suscripto', $suscripto, time() + (86400 * 365), '/' ); 
      if(!$suscripto) {
         echo 'no_suscripto';
         die();
      }

      $wpdb->insert('wp_eventos', array('evento_id' => $post, 'usuario_id' => $id, 'custID' => $respuesta['CustID'], 'created_at' => date('Y-m-d H:i:s')));
      echo $wpdb->insert_id;
      // $referencia = get_the_title($_POST['post']);
      $referencia = get_post_field('post_title', $_POST['post'], 'raw');
      $tipo = 'Evento';
      $valor = get_field('precio_original', $_POST['post']);
      // $promedio = get_field('precio_original', $_POST['post']);
      $term = get_the_category($_POST['post']);
      // $categoria_id = $term[0] && $term[0]->parent == '0' ? $term[0]->term_id : $term[0]->parent;
      // $term = get_term($categoria_id, 'category');
      // $categoria = $term->name;
      $fecha = date('Y-m-d H:i:s');
      // $vencimiento = get_field('sorteo_fecha_limite', $_POST['post']);
      $vencimiento = get_field('validez', $_POST['post']);
      $dia = substr($vencimiento,0,2);
      $mes = substr($vencimiento,3,2);
      $ano = substr($vencimiento,6,4);
      $vencimiento = $ano.'-'.$mes.'-'.$dia;
      $localidad = 'A DEFINIR';
      $merchant = get_field('merchant', $_POST['post']);
      $merchant = $merchant->post_title;
      enviar_AWS($respuesta['CustID'], $tipo, '', $referencia, 0, $valor, 'Eventos', $fecha, $vencimiento, $localidad, $merchant, 'Regular', 0);
      die();
   }

   add_action('wp_ajax_destruir_sesion', 'destruir_sesion' );
   add_action('wp_ajax_nopriv_destruir_sesion', 'destruir_sesion' );
   function destruir_sesion() {
      session_destroy();
      die();
   }

   function status_online() {
      setcookie('logged', true, time() + (86400 * 365), '/' ); 
   }

   add_action('init', 'start_session', 1);
   function start_session() {
      if(!session_id()) {
         session_start();
      }
   }

   add_action('wp_logout', 'end_session');
   add_action('wp_login', 'end_session');
   function end_session() {
      session_destroy();
   }

   // BAJAR USUARIOS ANOTADOS A EVENTOS
   function add_sorteo_admin_menu() {
      // $page_hook = 
      add_menu_page(
         __( 'Usuarios', 'sorteo-usuarios' ),
         __( 'Sorteos', 'sorteo-usuarios' ),
         'manage_options',
         'sorteo-usuarios',
         'load_Sorteo_List_Table',
         'dashicons-tickets-alt', 7
      );
   }
   add_action( 'admin_menu', 'add_sorteo_admin_menu' );

   function load_Sorteo_List_Table(){
      include_once('Classes/class-sorteos-list-table.php');
      $tabla = new Sorteos_List_Table('sorteo-usuarios');
      $tabla->prepare_items(); 
      echo '<div class="wrap">
          <h2>Usuarios anotados por evento</h2>
              <div id="sorteo-usuarios">       
                  <div id="nds-post-body">      
                  <form id="nds-user-list-form" method="get">
                     <input type="hidden" name="page" value="'.$_REQUEST['page'].'" />';
                     $tabla->display();
      echo '</form>
                  </div>         
              </div>
      </div>';
      die();
   }

   add_action( 'admin_post_print', 'print_csv' );
   function print_csv() {
      if ( ! current_user_can( 'manage_options' ) )
         return;
      
      if(!$_REQUEST['evento'])
         return;

      $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
         'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
         'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
         'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
         'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y');
      $titulo = get_the_title($_REQUEST['evento']);
      $slug = strtolower(preg_replace('/[^A-Za-z0-9-]+/', '-', strtr($titulo, $unwanted_array)));
      // $filename = $slug.'_usuarios_'.date('d-m-Y').'.csv';
      $filename = $slug.'_usuarios_'.date('d-m-Y').'.xls';

      header('Pragma: no-cache');
      header("Content-Disposition: attachment; filename=\"$filename\"");
      // header("Content-Type: text/csv: charset=UTF-8");
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

      header('Content-Description: File Transfer');
      header('Content-Transfer-Encoding: binary');
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');

      global $wpdb;
      $user_query = "SELECT u.nombre, u.apellido, u.email, u.UID FROM wp_eventos e INNER JOIN wp_usuarios u ON e.usuario_id = u.id WHERE evento_id = '".$_REQUEST['evento']."'";
      $data = $wpdb->get_results( $user_query, ARRAY_A);
      $string = "Nombre\tApellido\tE-mail\tUID\n";
      foreach ($data as $usuario) {
         $string .= $usuario['nombre']."\t".$usuario['apellido']."\t".$usuario['email']."\t".$usuario['UID']."\t\n";
      }
      print mb_convert_encoding($string, 'UTF-16LE', 'UTF-8');
      exit; 
   }

   add_action( 'admin_post_subir', 'subir_csv' );
   function subir_csv() {
      $csv = array_map('str_getcsv', file($_FILES['subir']['tmp_name']));
      $string = '';
      foreach($csv as $row) {
         $partes = explode(';', $row[0]);
         $string .= "(".$_POST['beneficio'].", ";
         $string .= "'".$partes[0]."', ".$partes[1].", ";
         $string .= "0), ";
      }
      $string = rtrim(rtrim($string), ',');
      $query = "INSERT INTO wp_codigos (beneficio_id, codigo, usos, canjeados) VALUES ".$string;
      global $wpdb;
      $wpdb->query($query);
      header("Location: ".$_REQUEST['back']);
      die();
      exit;
   }

   add_action('wp_ajax_guardar_ganadores', 'guardar_ganadores' );
   add_action('wp_ajax_nopriv_guardar_ganadores', 'guardar_ganadores' );
   function guardar_ganadores() {
      date_default_timezone_set("America/Puerto_Rico"); 
      if ( ! current_user_can( 'manage_options' ) )
         return;
      global $wpdb;
      $wpdb->query("UPDATE wp_eventos SET ganador = 'N' WHERE evento_id = ".$_POST['evento']);
      foreach($_POST['ganadores'] as $ganador) {
         $wpdb->update('wp_eventos', array('ganador' => 'Y'), array('usuario_id' => $ganador, 'evento_id' => $_POST['evento']));
         $usuario = $wpdb->get_results('SELECT * FROM wp_usuarios WHERE id="'.$ganador.'" LIMIT 1');
         $custID = $usuario[0]->CustID;
         $tipo = 'Evento';
         $referencia = get_post_field('post_title', $_POST['evento'], 'raw');
         $valor = get_field('precio_original', $_POST['evento']);
         $categoria = 'Eventos';
         //////////////////////////$fecha = date('Y-m-d H:i:s');
         $aux_evento = $wpdb->get_results('SELECT * FROM wp_eventos WHERE evento_id="'.$_POST['evento'].'" AND usuario_id="'.$usuario[0]->id.'" LIMIT 1');
         $fecha = $aux_evento[0]->created_at;

         $vencimiento = get_field('validez', $_POST['evento']);
         $dia = substr($vencimiento,0,2);
         $mes = substr($vencimiento,3,2);
         $ano = substr($vencimiento,6,4);
         $vencimiento = $ano.'-'.$mes.'-'.$dia;
         $localidad = 'A DEFINIR';
         $merchant = get_field('merchant', $_POST['evento']);
         $merchant = $merchant ? $merchant->post_title : 'No definido';
         enviar_AWS($custID, $tipo, '', $referencia, $valor, $valor, $categoria, $fecha, $vencimiento, $localidad, $merchant, 'Regular', $valor);
      }
      echo 'ok';
      exit; 
   }

   function get_client_ip() {
       $ipaddress = '';
       if (getenv('HTTP_CLIENT_IP'))
           $ipaddress = getenv('HTTP_CLIENT_IP');
       else if(getenv('HTTP_X_FORWARDED_FOR'))
           $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
       else if(getenv('HTTP_X_FORWARDED'))
           $ipaddress = getenv('HTTP_X_FORWARDED');
       else if(getenv('HTTP_FORWARDED_FOR'))
           $ipaddress = getenv('HTTP_FORWARDED_FOR');
       else if(getenv('HTTP_FORWARDED'))
          $ipaddress = getenv('HTTP_FORWARDED');
       else if(getenv('REMOTE_ADDR'))
           $ipaddress = getenv('REMOTE_ADDR');
       else
           $ipaddress = 'UNKNOWN';
       return $ipaddress;
   }

   add_action('wp_ajax_guardar_visita', 'guardar_visita' );
   add_action('wp_ajax_nopriv_guardar_visita', 'guardar_visita' );
   function guardar_visita() {
      $ip = get_client_ip();
      global $wpdb;
      $resultado = $wpdb->get_row("SELECT * FROM wp_visitas WHERE ip = '".$ip."' AND tipo = '".$_POST['tipo']."' AND beneficio_id = '".$_POST['id']."' ORDER BY created_at DESC LIMIT 1");
      if ($wpdb->num_rows) {
         $now = new \DateTime();
         $now->setTimezone(new DateTimeZone('America/Argentina/Buenos_Aires'));
         $hora = new DateTime($resultado->created_at);
         date_add($hora, date_interval_create_from_date_string('6 hours'));
         if($hora > $now) {
            exit;
         }
      } 
      $wpdb->insert('wp_visitas', array('ip' => $ip, 'tipo' => $_POST['tipo'], 'beneficio_id' => $_POST['id']));
      exit; 
   }

   add_action('wp_ajax_bajarCSV', 'bajarCSV' );
   add_action('wp_ajax_nopriv_bajarCSV', 'bajarCSV' );
   function bajarCSV() {
      echo "HOLA";
   }

   add_action( 'plugins_loaded', function() {
      if ( isset($_GET['page']) && $_GET['page'] == 'modulo-reportes-endi' && isset($_GET['beneficios'])) {
         $filename = 'reporte_'.date('d-m-Y').'.xls';
         global $wpdb;
         $query_results = "SELECT p.id as ID, p.post_title, p.post_type as tipo, IF(p.post_type = 'beneficios', (SELECT count(1) FROM wp_cupones u WHERE p.ID = u.cupon_id), (SELECT count(1) FROM wp_eventos e WHERE p.ID = e.evento_id)) as canjes, 
         (SELECT count(1) FROM wp_visitas c WHERE p.ID = c.beneficio_id) as visitas, 
         CAST(p.post_date as DATE) as inicio, 
         CAST((SELECT m.meta_value FROM wp_postmeta m WHERE p.ID = m.post_id AND m.meta_key = 'validez') as DATE) as fin,
         if((CAST(now() as DATE) BETWEEN p.post_date AND (SELECT m.meta_value FROM wp_postmeta m WHERE p.ID = m.post_id AND m.meta_key = 'validez')), 'Activos', 'Vencidos') as estado
         FROM wp_posts p WHERE p.post_status = 'publish' AND (p.post_type = 'beneficios' OR p.post_type = 'eventos') AND p.id IN (".implode(',', $_GET['beneficios']).") ORDER BY p.post_title ASC";
         $data = $wpdb->get_results( $query_results, ARRAY_A );
         $string = "Tipo\tBeneficio/Evento\tEstado\tVisitas\tDescargas\tInicio\tFin\n";
         foreach ($data as $beneficio) {
            $string .= ucfirst($beneficio['tipo'])."\t".$beneficio['post_title']."\t".$beneficio['estado']."\t".$beneficio['visitas']."\t".$beneficio['canjes']."\t".$beneficio['inicio']."\t".$beneficio['fin']."\t\n";
         }
         $output = mb_convert_encoding($string, 'UTF-16LE', 'UTF-8');
         header('Pragma: no-cache');
         header("Content-Disposition: attachment; filename=\"$filename\"");
         header("Content-type: application/vnd.ms-excel");
         print $output;
         exit;    
      }

      if ( isset($_GET['page']) && $_GET['page'] == 'generador-codigos-endi' && isset($_GET['beneficios'])) {
         $filename = 'codigos_generados_al_'.date('d-m-Y').'.csv';
         global $wpdb;
         $query_results = "SELECT p.post_title, c.* FROM wp_codigos c INNER JOIN wp_posts p ON c.beneficio_id = p.id WHERE p.post_status = 'publish' AND (p.post_type = 'beneficios' OR p.post_type = 'eventos') AND p.id IN (".implode(',', $_GET['beneficios']).") ORDER BY p.post_title ASC";
         $data = $wpdb->get_results( $query_results, ARRAY_A );
         $string = "Beneficio; Código\n";
         foreach ($data as $beneficio) {
            $string .= $beneficio['post_title'].";".$beneficio['codigo']."\t\n";
         }

         $output = mb_convert_encoding($string, 'UTF-16LE', 'UTF-8');
         header('Pragma: no-cache');
         header("Content-Disposition: attachment; filename=\"$filename\"");
         header("Content-Type: text/csv: charset=UTF-8");
         print $output;
         exit;    
      }
   });
?>