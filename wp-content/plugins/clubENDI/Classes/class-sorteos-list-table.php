<?php
// namespace clubENDI;
// include_once('class-wp-list-table.php');

/**
 * @link       http://www.dinardi.com.ar
 * @since      1.0.0
 * @author     Jazmín Nasta
 */
class Sorteos_List_Table extends WP_List_Table  {

	protected $plugin_text_domain;
	
	public function __construct( $plugin_text_domain ) {
		$this->plugin_text_domain = $plugin_text_domain;
		
		parent::__construct( array( 
			'plural' => 'Usuarios',
			'singular' => 'Usuario',
			'ajax' => false,
			'screen' => 'sorteos',
		) );
	}	
	
	public function prepare_items() {
		$user_search_key = isset( $_REQUEST['s'] ) ? wp_unslash( trim( $_REQUEST['s'] ) ) : '';
		$this->_column_headers = $this->get_column_info();
		$this->handle_table_actions();
		$table_data = $this->fetch_table_data();

		if( $user_search_key ) {
			$table_data = $this->filter_table_data( $table_data, $user_search_key );
		}		
		
		$codigos_per_page = 10;
		$table_page = $this->get_pagenum();		
		$this->items = array_slice( $table_data, ( ( $table_page - 1 ) * $codigos_per_page ), $codigos_per_page );

		$total_codigos = count( $table_data );
		$this->set_pagination_args( array (
			'total_items' => $total_codigos,
			'per_page'    => $codigos_per_page,
			'total_pages' => ceil( $total_codigos/$codigos_per_page )
		) );
	}
	
	public function get_columns() {
		$table_columns = array(
			// 'ID' => _x( 'ID', 'column name', $this->plugin_text_domain ),
			'post_title' => 'Beneficio',
			'estado' => 'Estado',
			'cantidad' => 'Usuarios',
			'bajar' => 'Participantes',
			'ganadores' => 'Ganadores' 		
		);
		return $table_columns;
	}

	protected function column_bajar( $item ) {
		return sprintf('<a class="btn btn-default" href="admin-post.php?action=print&evento='.$item['ID'].'">Bajar listado</a>');
	}

	protected function column_ganadores( $item ) {
		global $wpdb;
        $participantes = $wpdb->get_results("SELECT * FROM wp_eventos w INNER JOIN wp_usuarios u ON w.usuario_id = u.id WHERE evento_id = ".$item['ID']);
		$string = '<select data-evento="'.$item['ID'].'" class="tokenize" multiple>';
		foreach($participantes as $participante) {
			$selected = $participante->ganador == 'Y' ? 'selected="selected"' : '';
		  	$string .= '<option value="'.$participante->usuario_id.'" '.$selected.'>'.$participante->nombre.' '.$participante->apellido.'</option>';
		}
		$string .= '</select>';
		$string .= '<button type="button" class="guardar">Guardar</button>';
		return $string;
	}
	
	protected function get_sortable_columns() {
		$sortable_columns = array (
			'ID' => array( 'ID', true ),
			'post_title'=>'post_title'
		);
		return $sortable_columns;
	}	
	
	public function no_items() {
		_e( 'No hay usuarios.', $this->plugin_text_domain );
	}	
	
	public function fetch_table_data() {
		global $wpdb;
		$orderby = ( isset( $_GET['orderby'] ) ) ? esc_sql( $_GET['orderby'] ) : 'id';
		$order = ( isset( $_GET['order'] ) ) ? esc_sql( $_GET['order'] ) : 'ASC';
		
		$user_query = "SELECT p.ID, p.post_title, m.meta_value as limite, COUNT(e.ID) as cantidad FROM wp_posts p LEFT JOIN wp_eventos e ON p.ID = e.evento_id LEFT JOIN wp_postmeta m ON m.post_id = p.ID WHERE p.post_status = 'publish' AND p.post_type = 'eventos' AND m.meta_key = 'sorteo_fecha_limite' GROUP BY p.ID ORDER BY $orderby $order";

		$query_results = $wpdb->get_results( $user_query, ARRAY_A  );
		$resultado = array_map("mapearArray", $query_results);
		return $resultado;		
	}
	
	public function filter_table_data( $table_data, $search_key ) {
		$filtered_table_data = array_values( array_filter( $table_data, function( $row ) use( $search_key ) {
			foreach( $row as $row_val ) {
				if( stripos( $row_val, $search_key ) !== false ) {
					return true;
				}				
			}			
		} ) );
		return $filtered_table_data;
	}
	
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {			
			case 'post_title':
			case 'ID':
				return $item[$column_name];
			default:
			  return $item[$column_name];
		}
	}

	public function graceful_exit() {
		exit;
	}
	  	 
	public function invalid_nonce_redirect() {
		wp_die( __( 'Invalid Nonce', $this->plugin_text_domain ),
				__( 'Error', $this->plugin_text_domain ),
				array( 
						'response' 	=> 403, 
						'back_link' =>  esc_url( add_query_arg( array( 'page' => wp_unslash( $_REQUEST['page'] ) ) , admin_url( 'admin.php?page=generador-codigos-endi' ) ) ),
					)
		);
	}

	public function handle_table_actions() {
		if (isset( $_REQUEST['action'] ) && $_REQUEST['action'] === 'download') {	
			$this->page_bulk_download($_REQUEST['evento']);
			$this->graceful_exit();
		}
	}	
	
	public function page_bulk_download( $id ) {
		// require_once( 'list-download.php' );
	}
}

function mapearArray($e) {
	if($e['limite']) {
		if($e['limite'] < date('Y-m-d H:i:s')) {
			$limite = 'Cerrado';
		} else {
			$parse = date_parse($e['limite']);
			$limite = 'Abierto hasta el '.$parse['day'].'/'.$parse['month'];
		}
	} else {
		$limite = 'Sin fecha de cierre';
	}
	$e['estado'] = $limite;
	return $e;
}