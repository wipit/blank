<?php
	$filename = $slug.'_usuarios.xls';
	$headers = [
        'Content-Encoding' => 'UTF-8',
        'Content-Type' => 'application/vnd.ms-excel; charset=UTF-8',
        'Content-Disposition' => "attachment; filename=\"$filename\"",
    ];

	global $wpdb;
	if($id) {
		$titulo = get_the_title($id);
		$slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $titulo);
		$string = 'Usuarios anotados para '.$titulo.'/r/n';
		$user_query = "SELECT u.nombre, u.apellido, u.email, u.UID FROM wp_eventos e INNER JOIN wp_usuarios u ON e.usuario_id = u.id WHERE evento_id = '".$id."'";
		$data = $wpdb->get_results( $user_query, ARRAY_A);
		$string .= 'Nombre\t, apellido\t,email\t,UID\t\n';
		foreach ($data as $usuario) {
			$string .= $usuario['nombre'].','.$usuario['apellido'].','.$usuario['email'].','.$usuario['UID'].'\t\n';
		}
	}

    $output = mb_convert_encoding($string, 'UTF-16LE', 'UTF-8');
    response(rtrim($output, "\n"), 200, $headers)->send();
    exit;
?>